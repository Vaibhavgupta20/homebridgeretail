package com.homebridge.retail.test;

import java.text.ParseException;
import java.util.Hashtable;

import org.openqa.selenium.support.ui.Select;
import org.testng.SkipException;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.homebridge.retail.base.BaseTest;
import com.homebridge.retail.util.DataUtil;
import com.homebridge.retail.util.Xls_Reader;
import com.relevantcodes.extentreports.LogStatus;

public class TwoBorrowerPurchaseTest extends BaseTest {
	String testName = "TwoBorrowerPurchaseTest";
	Xls_Reader xls;

	// 90% application gets submitted
	@Test(dataProvider = "getData")
	public void SubmitProfile(Hashtable<String, String> data) throws ParseException, InterruptedException {
		// reporting
		test = rep.startTest(testName);
		xlsResults.setCellData("Results", "Test Case Name", 2, testName);
		// Generating the log in the report
		test.log(LogStatus.INFO, data.toString());

		// Validating the Run mode N or Y if N test will skip
		if (DataUtil.isSkip(xls, testName) || data.get("Runmode").equals("N")) {
			test.log(LogStatus.SKIP, "Skipping the test as runmode is N");
			throw new SkipException("Skipping the test as runmode is N");
		}
		test.log(LogStatus.INFO, "Starting " + testName);

		openBrowser(data.get("Browser"));
		navigate("url_AWilson");

		click("applyNow_xpath");

		// switchToPopUp();

		// filling pre app information
		wait("continueBtn_xpath", 5);
		click("continueBtn_xpath");

		Thread.sleep(3000);
		click("genericcontainsValue", data.get("LoanTypeB"));
		click("otherAppAppliying", data.get("OtherApplicant"));
		click("isApplicantSpouseBegining", data.get("IsApplicantSpouse"));
		click("isCoApplicantIncludedMortgage", data.get("CoApplicantMortgage"));

		type_actions("zipcodeBegining_xpath", data.get("ZipCodeB"));
		type_actions("purchasePriceBegining_xpath", data.get("PurchasePriceB"));

		type_actions("downpaymentBegining_xpath", data.get("DownpaymentB"));
		click("nextBegining_xpath");

		type_actions("firstNameBegining_xpath", data.get("FirstNameB"));
		type_actions("middleNameBegining_xpath", data.get("MiddleNameB"));
		type_actions("lastNameBegining_xpath", data.get("LastNameB"));

		// scrolling
		scrollIntoView("nextCoAppBegining_xpath");
		String email = "alicex" + getSaltString() + "@m.tst";
		System.out.println(email);
		type_actions("emailBegining_xpath", email);
		type_actions("phoneBegining_xpath", data.get("PhoneNumberB"));
		type_actions("dobBegining_xpath", data.get("DateOfBirthB"));
		click_actions("vetranBegining_xpath");
		click("generictext", data.get("MarriedB"));
		click("nextCoAppBegining_xpath");

		type_actions("firstNameSpouseBegining_xpath", data.get("FirstNameSpouseB"));
		type_actions("middleNameSpouseBegining_xpath", data.get("MiddleNameSpouseB"));
		type_actions("lastNameSpouseBegining_xpath", data.get("LastNameSpouseB"));

		scrollIntoView("submit2Begining_xpath");
		type_actions("emailSpouseBegining_xpath", data.get("EmailSpouseB"));
		type_actions("phoneSpouseBegining_xpath", data.get("PhoneNumberSpouseB"));
		type_actions("dobSpouseBegining_xpath", data.get("DateOfBirthSpouseB"));
		click_actions("vetranSpouseBegining_xpath");

		click("submit2Begining_xpath");

		// creating account
		type_actions("passwordSignup_xpath", data.get("Password"));
		type_actions("password2Signup_xpath", data.get("Password"));
		click("createProfile_xpath");

		// signing in
		Thread.sleep(11000);
		click("signInSignUpBtn_xpath");

		wait("email_xpath", 5);
		type("email_xpath", email);
		type("password_xpath", data.get("Password"));
		wait("signIn_xpath", 5);
		/** clicking on sign in button */
		click_actions("signIn_xpath");

		/** loan purpose option */
		Thread.sleep(2000);
		click("generictext", data.get("SMSOptIn"));
		type_actions("phoneNumber_xpath", data.get("PhoneNumber"));
		click("loanType", data.get("LoanType"));
		// excel reporting
		String fln[] = getElement("firstLastName_xpath").getText().trim().split(" ");
		xlsResults.setCellData("Results", "PrimaryFirstName", 2, fln[0]);
		xlsResults.setCellData("Results", "PrimaryLastName", 2, fln[1]);
		click("saveContinue_xpath");
		Thread.sleep(2000);

		/** purchase loan options */
		click("generictext", data.get("PropertyUsage"));
		click("propertyType_xpath");
		Thread.sleep(500);
		click("genericcontains", data.get("PropertyType"));
		Thread.sleep(500);
		// type_actions("purchasePrice_xpath", data.get("PurchasePrice"));
		// type_actions("downPaymentAmount_xpath",
		// data.get("DownPaymentAmount"));

		click("saveContinue2_xpath");
		Thread.sleep(2000);

		/** filling primary aplicant */
		click_actions("primaryApplicant_xpath");
		Thread.sleep(1000);

		/** reporting excel email */
		xlsResults.setCellData("Results", "Email", 2, getElement("email_xpath").getAttribute("value"));

		click("genericcontains", data.get("CurrentOccupancyPriApp"));
		click("genericcontains", data.get("MaritalStatusPriApp"));
		click("phoneType", data.get("PhoneTypePriApp"));
		click("emailType", data.get("EmailTypePriApp"));
		Thread.sleep(500);
		// TODO: need a solutuon for this
		// type_actions("firstName_xpath", data.get("FirstNamePriApp"));
		// type_actions("middleInitial_xpath", data.get("MiddleNamePriApp"));
		// type_actions("lastName_xpath", data.get("LastNamePriApp"));
		// type_actions("birthDate_xpath", data.get("BirthDatePriApp"));
		type_actions("numberDependents_xpath", data.get("NoDependentsPriApp"));
		Thread.sleep(500);
		click("homeAddress_xpath");
		Thread.sleep(500);
		type_actions("addressAddress_xpath", data.get("AddressPriApp"));
		type_actions("cityAddress_xpath", data.get("CityPriApp"));
		type_actions("zipcodeAddress_xpath", data.get("ZipcodePriApp"));
		// type_actions("stateAddress_xpath", data.get("StatePriApp"));

		click("stateAddress_xpath");
		Thread.sleep(500);
		click("genericcontains", data.get("StatePriApp"));

		click_actions("okayAddress_xpath");
		Thread.sleep(500);
		type_actions("rentAmt_xpath", data.get("RentAmountPriApp"));
		// type_actions("otherExpensesAmt_xpath",
		// data.get("OtherExpensesPriApp"));
		type_actions("timeAtAddressYears_xpath", data.get("TimeYearsPriApp"));
		type_actions("timeAtAddressMonths_xpath", data.get("TimeMonthsPriApp"));
		// TODO:check whether PhoneNumberPriApp is needed //
		type_actions("phoneNumber_xpath", data.get("PhoneNumberPriApp"));
		// email is already filled in application
		// type_actions("email_xpath", data.get("EmailPriApp"));
		click_actions("save_xpath");
		Thread.sleep(3500);

		/** filling Co appilicant details */
		click_actions("primaryApplicant_xpath");
		Thread.sleep(1000);
		click("genericcontains", data.get("CoApplicantSpose"));
		click("genericcontains", data.get("CurrentOccupancyCo"));
		click("genericcontains", data.get("MaritalStatusCo"));
		Thread.sleep(500);
		type_actions("firstName_xpath", data.get("FirstNameCo"));
		type_actions("middleInitial_xpath", data.get("MiddleNameCo"));
		type_actions("lastName_xpath", data.get("LastNameCo"));
		// type_actions("birthDate_xpath", data.get("BirthDateCo"));
		type_actions("numberDependents_xpath", data.get("NoDependentsCo"));
		click("homeAddress_xpath");
		Thread.sleep(500);
		type_actions("addressAddress_xpath", data.get("AddressCo"));
		type_actions("cityAddress_xpath", data.get("CityCo"));
		type_actions("zipcodeAddress_xpath", data.get("ZipcodeCo"));
		// type_actions("stateAddress_xpath", data.get("StateCo"));

		click("stateAddress_xpath");
		Thread.sleep(500);
		click("genericcontains", data.get("StateCo"));

		click_actions("okayAddress_xpath");
		Thread.sleep(500);
		type_actions("rentAmt_xpath", data.get("RentAmountCo"));
		// type_actions("otherExpensesAmt_xpath", data.get("OtherExpensesCo"));
		type_actions("timeAtAddressYears_xpath", data.get("TimeYearsCo"));
		type_actions("timeAtAddressMonths_xpath", data.get("TimeMonthsCo"));
		type_actions("phoneNumber_xpath", data.get("PhoneNumberCo"));
		// type_actions("email_xpath", data.get("EmailCo"));
		click("emailType", data.get("EmailTypeCo"));
		click("phoneType", data.get("PhoneTypeCo"));
		click_actions("save_xpath");
		Thread.sleep(3500);

		// saving applicants
		click("saveContinue2_xpath");
		Thread.sleep(1500);

		/** filling income primary applicant */
		type_actions("baseSalaryAnnual_xpath", data.get("BaseSalaryAnnualPriApp"));
		type_actions("overtimeAnnual_xpath", data.get("OvertimeAnnualPriApp"));
		type_actions("bonusAnnual_xpath", data.get("BonusAnnualPriApp"));
		type_actions("commissionsAnnual_xpath", data.get("CommisionsAnnualPriApp"));

		click("saveContinue2_xpath");
		Thread.sleep(500);

		/** filling income co-applicant */
		type_actions("baseSalaryAnnual_xpath", data.get("BaseSalaryAnnualCo"));
		type_actions("overtimeAnnual_xpath", data.get("OvertimeAnnualCo"));
		type_actions("bonusAnnual_xpath", data.get("BonusAnnualCo"));
		type_actions("commissionsAnnual_xpath", data.get("CommisionsAnnualCo"));

		click("saveContinue2_xpath");
		Thread.sleep(1500);
		/** consent */

		click("creditreportConsent", data.get("CreditReportConsentPriApp"));
		click("creditreportConsentCoApp", data.get("CreditReportConsentCo"));

		click("receiveSmsConsent", data.get("SMSConsentPriApp"));
		click("receiveSmsConsentCoApp", data.get("SMSConsentCo"));

		// click("edocsConsent", data.get("EDocsConsentPriApp"));
		// click("edocsConsentCoApp", data.get("EDocsConsentCo"));

		type_actions("ssnConsent_xpath", data.get("SSNConsentPriApp"));
		type_actions("ssnConsentCoApp_xpath", data.get("SSNConsentCo"));

		click("jointApplicant", data.get("JointApplicant"));

		click("saveContinue2_xpath");
		Thread.sleep(1500);

		/** continue to full application */
		Thread.sleep(500);
		click_actions("continueFullApplication_xpath");
		Thread.sleep(2000);

		/** filing primary Employment */
		click_actions("addemployment_xpath");
		Thread.sleep(500);
		click_actions("employmentType_xpath");
		Thread.sleep(500);
		click("genericcontains", data.get("EmploymentTypeEmpPri"));
		Thread.sleep(500);
		type_actions("employerName_xpath", data.get("EmployerNameEmpPri"));
		click_actions("employmentAddress_xpath");
		Thread.sleep(500);
		type_actions("addressEmpAddress_xpath", data.get("AddressEmpPri"));
		type_actions("cityEmpAddress_xpath", data.get("CityEmpPri"));
		type_actions("zipcodeEmpAddress_xpath", data.get("ZipcodeEmpPri"));
		// type_actions("stateEmpAddress_xpath", data.get("StateEmpPri"));

		click("stateEmpAddress_xpath");
		Thread.sleep(500);
		click("genericcontains", data.get("StateEmpPri"));

		click_actions("okayEmpAddress_xpath");
		Thread.sleep(500);
		click("genericcontains", data.get("SelfEmployedEmpPri"));
		type_actions("positionEmployment_xpath", data.get("PositionHeldEmpPri"));
		type_actions("yearsWorked_xpath", data.get("YearsWorkedEmpPri"));
		type_actions("monthsWorked_xpath", data.get("MonthsWorkedEmpPri"));
		click_actions("save_xpath");
		Thread.sleep(1000);

		/** filing co applicant Employment */
		click_actions("addemploymentco_xpath");
		Thread.sleep(500);
		click_actions("employmentType_xpath");
		Thread.sleep(500);

		click("genericcontains", data.get("EmploymentTypeEmpCo"));
		Thread.sleep(500);
		type_actions("employerName_xpath", data.get("EmployerNameEmpCo"));
		click_actions("employmentAddress_xpath");
		Thread.sleep(500);
		type_actions("addressEmpAddress_xpath", data.get("AddressEmpCo"));
		type_actions("cityEmpAddress_xpath", data.get("CityEmpCo"));
		type_actions("zipcodeEmpAddress_xpath", data.get("ZipcodeEmpCo"));

		// type_actions("stateEmpAddress_xpath", data.get("StateEmpCo"));

		click("stateEmpAddress_xpath");
		Thread.sleep(500);
		click("genericcontains", data.get("StateEmpCo"));

		click_actions("okayEmpAddress_xpath");
		Thread.sleep(500);
		click("genericcontains", data.get("SelfEmployedEmpCo"));
		type_actions("positionEmployment_xpath", data.get("PositionHeldEmpCo"));
		type_actions("yearsWorked_xpath", data.get("YearsWorkedEmpCo"));
		type_actions("phoneEmp_xpath", data.get("PhoneNumberB"));
		type_actions("monthsWorked_xpath", data.get("MonthsWorkedEmpCo"));
		click_actions("save_xpath");
		Thread.sleep(1000);

		click("saveContinue2_xpath");
		Thread.sleep(1500);

		/** filling Assets */
		click_actions("addManualAssetItems_xpath");
		Thread.sleep(500);
		type_actions("AssetInstitutionName_xpath", data.get("AssetInstitutionName"));
		click("genericcontains", data.get("AssetAppliesT0"));
		click("AssetAppliesT0Co", data.get("AssetAppliesT0Co"));
		click_actions("Assetaddress_xpath");
		Thread.sleep(500);
		type_actions("AseetLineAddress_xpath", data.get("Assetaddress"));
		type_actions("AseetcityAddress_xpath", data.get("AseetcityAddress"));
		type_actions("AssetZipcodeAddress_xpath", data.get("AssetZipcodeAddress"));
		// type_actions("AssetstateAddress_xpath",
		// data.get("AssetstateAddress"));

		click("AssetstateAddress_xpath");
		Thread.sleep(500);
		click("genericcontains", data.get("AssetstateAddress"));

		click_actions("AssetokayAddress_xpath");
		Thread.sleep(500);
		type_actions("AssetaccountNumber_xpath", data.get("AssetaccountNumber"));
		type_actions("AssetBalanceCurrentValue_xpath", data.get("AssetBalanceCurrentValue"));
		click_actions("AssetAccountType_xpath");
		Thread.sleep(900);
		click("genericcontains", data.get("AssetAccountType"));
		Thread.sleep(500);
		click_actions("save_xpath");
		Thread.sleep(1500);

		click("saveContinue2_xpath");
		Thread.sleep(1500);

		/** real estate */

		click("saveContinue2_xpath");
		Thread.sleep(1500);

		/** declarations page primary applicant */

		click("outstandigjudgement", data.get("OutstandingJudgementPriApp"));
		click("declaredbankruptcy", data.get("DeclaredBankruptcyPriApp"));
		click("propertyforeclosed", data.get("PropertyForeclosedPriApp"));
		click("currentlylawsuit", data.get("CurrentlyLawsuitPriApp"));
		click("foreclosureinvolved", data.get("ForeclosureInvolvedPriApp"));
		click("currentlydefaultor", data.get("CurrentlyDefaultorPriApp"));
		click("childsupport", data.get("ChildSupportPriApp"));
		click("paymentborrowed", data.get("PaymentBorrowedPriApp"));
		click("endorsercomaker", data.get("EndorserComakerPriApp"));
		click("UScitizen", data.get("UscitizenPriApp"));
		click("permanentresidentalien", data.get("PermanentresidentAlienPriApp"));
		click("primaryresidence", data.get("PrimaryResidencePriApp"));
		click("ownershipIntrest", data.get("OwnershipIntrestPriApp"));

		click("saveContinue2_xpath");
		Thread.sleep(500);
		/** declarations page co applicant */
		click("outstandigjudgement", data.get("OutstandingJudgementCo"));
		click("declaredbankruptcy", data.get("DeclaredBankruptcyCo"));
		click("propertyforeclosed", data.get("PropertyForeclosedCo"));
		click("currentlylawsuit", data.get("CurrentlyLawsuitCo"));
		click("foreclosureinvolved", data.get("ForeclosureInvolvedCo"));
		click("currentlydefaultor", data.get("CurrentlyDefaultorCo"));
		click("childsupport", data.get("ChildSupportCo"));
		click("paymentborrowed", data.get("PaymentBorrowedCo"));
		click("endorsercomaker", data.get("EndorserComakerCo"));
		click("UScitizen", data.get("UscitizenCo"));
		click("permanentresidentalien", data.get("PermanentresidentAlienCo"));
		click("primaryresidence", data.get("PrimaryResidenceCo"));
		click("ownershipIntrest", data.get("OwnershipIntrestPriApp"));
		click("saveContinue2_xpath");
		Thread.sleep(1500);

		/** govt. monitoring primary */
		click_actions("otherThanHispanic_xpath");
		click_actions("femaleGovt_xpath");
		click_actions("asianGovt_xpath");
		click_actions("filipinoGovt_xpath");

		click("saveContinue2_xpath");
		Thread.sleep(1500);

		/** govt. monitoring coapplicant */
		click_actions("otherThanHispanic_xpath");
		click_actions("femaleGovt_xpath");
		click_actions("asianGovt_xpath");
		click_actions("filipinoGovt_xpath");

		click("saveContinue2_xpath");
		Thread.sleep(1500);

		click_actions("submitNow_xpath");
		Thread.sleep(2500);
		isSuccess();
	}

	@BeforeTest
	public void xlsResult() {
		init();
		initXlsResult(testName);
	}

	@DataProvider()
	public Object[][] getData() {
		super.init();
		xls = new Xls_Reader(System.getProperty("user.dir") + prop.getProperty("xlspath"));
		return DataUtil.getTestData(xls, testName);

	}

}