package com.homebridge.retail.test;

import java.text.ParseException;
import java.util.Hashtable;

import org.openqa.selenium.support.ui.Select;
import org.testng.SkipException;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.homebridge.retail.base.BaseTest;
import com.homebridge.retail.util.DataUtil;
import com.homebridge.retail.util.Xls_Reader;
import com.relevantcodes.extentreports.LogStatus;

public class TwoBorrowerRefinanceTest extends BaseTest {
	String testName = "TwoBorrowerRefinanceTest";
	Xls_Reader xls;

	private void fillingPurchaseLoneOption(Hashtable<String, String> data) throws InterruptedException {
		/** purchase loan options */
		click("homeAddress_xpath");
		Thread.sleep(500);
//		type_actions("addressAddress_xpath", data.get("AddressPurchaseLoan"));
//		type_actions("cityAddress_xpath", data.get("CityPurchaseLoan"));
//		type_actions("stateAddress_xpath", data.get("StatePurchaseLoan"));
//		type_actions("zipcodeAddress_xpath", data.get("ZipcodePurchaseLoan"));
		click_actions("okayAddress_xpath");
		Thread.sleep(500);
		click("generictext", data.get("PropertyUsage"));
		click("propertyType_xpath");
		Thread.sleep(500);
		click("genericcontains", data.get("PropertyType"));
		Thread.sleep(500);
		type_actions("propertyValue_xpath", data.get("PropertyValue"));

		click("saveContinue2_xpath");
		Thread.sleep(2000);

		click_actions("addOutstandingLoan_xpath");
		Thread.sleep(500);
		type_actions("currentBalance_xpath", data.get("Current Balance"));
		type_actions("intrestRate_xpath", data.get("Interest Rate"));
		click("genericcontains", data.get("Payoff Loan"));
		click("loanmortgageType_xpath");
		Thread.sleep(500);
		click("genericcontains", data.get("Loan Type"));
		Thread.sleep(500);
		click_actions("save_xpath");
		Thread.sleep(1500);

		click("refiPurposeType_xpath");
		Thread.sleep(500);
		click("genericcontains", data.get("RefinancePurpose"));
		Thread.sleep(500);

		click("saveContinue2_xpath");
		Thread.sleep(2000);

	}

	@Test(dataProvider = "getData")
	public void SubmitProfile(Hashtable<String, String> data) throws ParseException, InterruptedException {
		// reporting
		test = rep.startTest(testName);
		xlsResults.setCellData("Results", "Test Case Name", 2, testName);
		// Generating the log in the report
		test.log(LogStatus.INFO, data.toString());

		// Validating the Run mode N or Y if N test will skip
		if (DataUtil.isSkip(xls, testName) || data.get("Runmode").equals("N")) {
			test.log(LogStatus.SKIP, "Skipping the test as runmode is N");
			throw new SkipException("Skipping the test as runmode is N");
		}
		test.log(LogStatus.INFO, "Starting " + testName);

		openBrowser(data.get("Browser"));
		navigate("url_AWilson");

		click("applyNow_xpath");

		// switchToPopUp();

		// filling pre app information
		wait("continueBtn_xpath", 5);
		click("continueBtn_xpath");

		Thread.sleep(3000);
		click("genericcontainsValue", data.get("LoanTypeB"));
		click("otherAppAppliying", data.get("OtherApplicant"));
		click("isApplicantSpouseBegining", data.get("IsApplicantSpouse"));

//		type_actions("zipcodeBegining_xpath", data.get("ZipCodeB"));
		
		
		Thread.sleep(500);
		click("homeAddress_xpath");
		Thread.sleep(500);
		type_actions("addressAddress_xpath", data.get("AddressPriApp"));
		type_actions("cityAddress_xpath", data.get("CityPriApp"));
		type_actions("stateAddress_xpath", data.get("StatePriApp"));
		type_actions("zipcodeAddress_xpath", data.get("ZipcodePriApp"));
		click_actions("okayAddress_xpath");
		Thread.sleep(500);
		
		type_actions("outstandingBalanceBegining_xpath", data.get("OutstandingBalanceB"));
		type_actions("cashOutAmountBegining_xpath", data.get("CashoutAmountB"));
		click("nextBegining_xpath");

		type_actions("firstNameBegining_xpath", data.get("FirstNameB"));
		type_actions("middleNameBegining_xpath", data.get("MiddleNameB"));
		type_actions("lastNameBegining_xpath", data.get("LastNameB"));

		// scrolling
		scrollIntoView("nextCoAppBegining_xpath");
		type_actions("emailBegining_xpath", data.get("EmailB"));
		type_actions("phoneBegining_xpath", data.get("PhoneNumberB"));
		type_actions("dobBegining_xpath", data.get("DateOfBirthB"));
		click_actions("vetranBegining_xpath");
		click("generictext", data.get("MarriedB"));
		click("nextCoAppBegining_xpath");

		type_actions("firstNameSpouseBegining_xpath", data.get("FirstNameSpouseB"));
		type_actions("middleNameSpouseBegining_xpath", data.get("MiddleNameSpouseB"));
		type_actions("lastNameSpouseBegining_xpath", data.get("LastNameSpouseB"));

		scrollIntoView("submit2Begining_xpath");
		type_actions("emailSpouseBegining_xpath", data.get("EmailSpouseB"));
		type_actions("phoneSpouseBegining_xpath", data.get("PhoneNumberSpouseB"));
		type_actions("dobSpouseBegining_xpath", data.get("DateOfBirthSpouseB"));
		click_actions("vetranSpouseBegining_xpath");

		click("submit2Begining_xpath");

		// creating account
		type_actions("passwordSignup_xpath", data.get("Password"));
		type_actions("password2Signup_xpath", data.get("Password"));
		click("createProfile_xpath");

		// signing in
		Thread.sleep(11000);
		click("signInSignUpBtn_xpath");

		wait("email_xpath", 5);
		type("email_xpath", data.get("EmailB"));
		type("password_xpath", data.get("Password"));
		wait("signIn_xpath", 5);
		/** clicking on sign in button */
		click_actions("signIn_xpath");

		/** loan purpose option */
		click("generictext", data.get("SMSOptIn"));
		type_actions("phoneNumber_xpath", data.get("PhoneNumber"));
		click("loanType", data.get("LoanType"));
		// excel reporting
		String fln[] = getElement("firstLastName_xpath").getText().trim().split(" ");
		xlsResults.setCellData("Results", "PrimaryFirstName", 2, fln[0]);
		xlsResults.setCellData("Results", "PrimaryLastName", 2, fln[1]);
		click("saveContinue_xpath");
		Thread.sleep(2000);

		fillingPurchaseLoneOption(data);
		fillingApplicantDetail(data);
		fillingIncome(data);
		fillingConsent(data);

		/** continue to full application */
		Thread.sleep(500);
		click_actions("continueFullApplication_xpath");
		Thread.sleep(2000);

		fillingEmployment(data);

		fillingAssests(data);

		fillingRealEstate(data);

		fillingDeclarations(data);

		fillingGovtMonitring();

		click_actions("submitNow_xpath");
		Thread.sleep(2500);
		isSuccess();
	}

	private void fillingAssests(Hashtable<String, String> data) throws InterruptedException {
		/** filling Assets */
		click_actions("addManualAssetItems_xpath");
		Thread.sleep(500);
		type_actions("AssetInstitutionName_xpath", data.get("AssetInstitutionName"));
		click("genericcontains", data.get("AssetAppliesT0"));
		click("AssetAppliesT0Co", data.get("AssetAppliesT0Co"));
		click_actions("Assetaddress_xpath");
		Thread.sleep(500);
		type_actions("AseetLineAddress_xpath", data.get("Assetaddress"));
		type_actions("AseetcityAddress_xpath", data.get("AseetcityAddress"));
		type_actions("AssetstateAddress_xpath", data.get("AssetstateAddress"));
		type_actions("AssetZipcodeAddress_xpath", data.get("AssetZipcodeAddress"));
		click_actions("AssetokayAddress_xpath");
		Thread.sleep(500);
		type_actions("AssetaccountNumber_xpath", data.get("AssetaccountNumber"));
		type_actions("AssetBalanceCurrentValue_xpath", data.get("AssetBalanceCurrentValue"));
		click_actions("AssetAccountType_xpath");
		Thread.sleep(900);
		click("genericcontains", data.get("AssetAccountType"));
		Thread.sleep(500);
		click_actions("save_xpath");
		Thread.sleep(1500);

		click("saveContinue2_xpath");
		Thread.sleep(1500);

	}

	private void fillingRealEstate(Hashtable<String, String> data) throws InterruptedException {
		/** real estate */

		click_actions("incompleteRealEstate_xpath");
		Thread.sleep(500);
		click("genericcontains", data.get("OccupancyTypeRE"));
		click("OccypancyTypeCoborrower", data.get("OccupancyTypeRECoApp"));
		click("genericcontains", data.get("SubjectPropertyRE"));
//		click_actions("propertyTypeRE_xpath");
//		Thread.sleep(500);
//		click("genericcontains", data.get("PropertyTypeRE"));
		Thread.sleep(500);
		click("genericcontains", data.get("PropertyStatusRE"));
		type_actions("curruntMarketValueRE_xpath", data.get("PresentValueRE"));
		type_actions("monthlyMortgageRE_xpath", data.get("MontlyMortgageRE"));
		type_actions("homeownerDuesRE_xpath", data.get("HomeownerDuesRE"));
		type_actions("otherExpensesRE_xpath", data.get("OtherExpensesRE"));
		click_actions("save_xpath");
		Thread.sleep(1500);

	
	
		
		click("saveContinue2_xpath");
		Thread.sleep(1500);

	}

	private void fillingDeclarations(Hashtable<String, String> data) throws InterruptedException {
		/** declarations page primary applicant */

		click("outstandigjudgement", data.get("OutstandingJudgementPriApp"));
		click("declaredbankruptcy", data.get("DeclaredBankruptcyPriApp"));
		click("propertyforeclosed", data.get("PropertyForeclosedPriApp"));
		click("currentlylawsuit", data.get("CurrentlyLawsuitPriApp"));
		click("foreclosureinvolved", data.get("ForeclosureInvolvedPriApp"));
		click("currentlydefaultor", data.get("CurrentlyDefaultorPriApp"));
		click("childsupport", data.get("ChildSupportPriApp"));
		click("paymentborrowed", data.get("PaymentBorrowedPriApp"));
		click("endorsercomaker", data.get("EndorserComakerPriApp"));
		click("UScitizen", data.get("UscitizenPriApp"));
		click("permanentresidentalien", data.get("PermanentresidentAlienPriApp"));
		click("primaryresidence", data.get("PrimaryResidencePriApp"));
		click("ownershipIntrest", data.get("OwnershipIntrest"));
		click("genericcontains", data.get("PropertyTypeDecPriApp"));
		click("genericcontains", data.get("OwnershipTitleDecPriApp"));

		click("saveContinue2_xpath");
		Thread.sleep(500);
		/** declarations page co applicant */
		click("outstandigjudgement", data.get("OutstandingJudgementCo"));
		click("declaredbankruptcy", data.get("DeclaredBankruptcyCo"));
		click("propertyforeclosed", data.get("PropertyForeclosedCo"));
		click("currentlylawsuit", data.get("CurrentlyLawsuitCo"));
		click("foreclosureinvolved", data.get("ForeclosureInvolvedCo"));
		click("currentlydefaultor", data.get("CurrentlyDefaultorCo"));
		click("childsupport", data.get("ChildSupportCo"));
		click("paymentborrowed", data.get("PaymentBorrowedCo"));
		click("endorsercomaker", data.get("EndorserComakerCo"));
		click("UScitizen", data.get("UscitizenCo"));
		click("permanentresidentalien", data.get("PermanentresidentAlienCo"));
		click("primaryresidence", data.get("PrimaryResidenceCo"));
		scrollIntoView("saveContinue2_xpath");
		click("ownershipIntrest", data.get("OwnershipIntrest"));
		click("genericcontains", data.get("PropertyTypeDecCo"));
		click("genericcontains", data.get("OwnershipTitleDecCo"));

		click("saveContinue2_xpath");
		Thread.sleep(1500);

	}

	private void fillingGovtMonitring() throws InterruptedException {

		/** govt. monitoring primary */
		click_actions("otherThanHispanic_xpath");
		click_actions("femaleGovt_xpath");
		click_actions("asianGovt_xpath");
		click_actions("filipinoGovt_xpath");

		click("saveContinue2_xpath");
		Thread.sleep(1500);

		/** govt. monitoring coapplicant */
		click_actions("otherThanHispanic_xpath");
		click_actions("femaleGovt_xpath");
		click_actions("asianGovt_xpath");
		click_actions("filipinoGovt_xpath");

		click("saveContinue2_xpath");
		Thread.sleep(1500);

	}

	private void fillingConsent(Hashtable<String, String> data) throws InterruptedException {
		/** consent */

		click("creditreportConsent", data.get("CreditReportConsentPriApp"));
		click("creditreportConsentCoApp", data.get("CreditReportConsentCo"));

		click("receiveSmsConsent", data.get("SMSConsentPriApp"));
		click("receiveSmsConsentCoApp", data.get("SMSConsentCo"));

		// click("edocsConsent", data.get("EDocsConsentPriApp"));
		// click("edocsConsentCoApp", data.get("EDocsConsentCo"));

		type_actions("ssnConsent_xpath", data.get("SSNConsentPriApp"));
		type_actions("ssnConsentCoApp_xpath", data.get("SSNConsentCo"));

		click("jointApplicant", data.get("JointApplicant"));

		click("saveContinue2_xpath");
		Thread.sleep(1500);

	}

	private void fillingEmployment(Hashtable<String, String> data) throws InterruptedException {
		/** filing primary Employment */
		click_actions("addemployment_xpath");
		Thread.sleep(500);
		click_actions("employmentType_xpath");
		Thread.sleep(500);
		click("genericcontains", data.get("EmploymentTypeEmpPri"));
		Thread.sleep(500);
		type_actions("employerName_xpath", data.get("EmployerNameEmpPri"));
		click_actions("employmentAddress_xpath");
		Thread.sleep(500);
		type_actions("addressEmpAddress_xpath", data.get("AddressEmpPri"));
		type_actions("cityEmpAddress_xpath", data.get("CityEmpPri"));
		type_actions("stateEmpAddress_xpath", data.get("StateEmpPri"));
		type_actions("zipcodeEmpAddress_xpath", data.get("ZipcodeEmpPri"));
		click_actions("okayEmpAddress_xpath");
		Thread.sleep(500);
		click("genericcontains", data.get("SelfEmployedEmpPri"));
		type_actions("positionEmployment_xpath", data.get("PositionHeldEmpPri"));
		type_actions("yearsWorked_xpath", data.get("YearsWorkedEmpPri"));
		type_actions("monthsWorked_xpath", data.get("MonthsWorkedEmpPri"));
		click_actions("save_xpath");
		Thread.sleep(1000);

		/** filing co applicant Employment */
		click_actions("addemploymentco_xpath");
		Thread.sleep(500);
		click_actions("employmentType_xpath");
		Thread.sleep(500);
		click("genericcontains", data.get("EmploymentTypeEmpCo"));
		Thread.sleep(500);
		type_actions("employerName_xpath", data.get("EmployerNameEmpCo"));
		click_actions("employmentAddress_xpath");
		Thread.sleep(500);
		type_actions("addressEmpAddress_xpath", data.get("AddressEmpCo"));
		type_actions("cityEmpAddress_xpath", data.get("CityEmpCo"));
		type_actions("stateEmpAddress_xpath", data.get("StateEmpCo"));
		type_actions("zipcodeEmpAddress_xpath", data.get("ZipcodeEmpCo"));
		click_actions("okayEmpAddress_xpath");
		Thread.sleep(500);
		click("genericcontains", data.get("SelfEmployedEmpCo"));
		type_actions("positionEmployment_xpath", data.get("PositionHeldEmpCo"));
		type_actions("yearsWorked_xpath", data.get("YearsWorkedEmpCo"));
		type_actions("monthsWorked_xpath", data.get("MonthsWorkedEmpCo"));
		click_actions("save_xpath");
		Thread.sleep(1000);

		click("saveContinue2_xpath");
		Thread.sleep(1500);

	}

	private void fillingIncome(Hashtable<String, String> data) throws InterruptedException {

		/** filling income primary applicant */
		type_actions("baseSalaryAnnual_xpath", data.get("BaseSalaryAnnualPriApp"));
		type_actions("overtimeAnnual_xpath", data.get("OvertimeAnnualPriApp"));
		type_actions("bonusAnnual_xpath", data.get("BonusAnnualPriApp"));
		type_actions("commissionsAnnual_xpath", data.get("CommisionsAnnualPriApp"));

		click("saveContinue2_xpath");
		Thread.sleep(500);

		/** filling income co-applicant */
		type_actions("baseSalaryAnnual_xpath", data.get("BaseSalaryAnnualCo"));
		type_actions("overtimeAnnual_xpath", data.get("OvertimeAnnualCo"));
		type_actions("bonusAnnual_xpath", data.get("BonusAnnualCo"));
		type_actions("commissionsAnnual_xpath", data.get("CommisionsAnnualCo"));

		click("saveContinue2_xpath");
		Thread.sleep(1500);

	}

	@BeforeTest
	public void xlsResult() {
		initXlsResult(testName);
	}

	@DataProvider()
	public Object[][] getData() {
		super.init();
		xls = new Xls_Reader(System.getProperty("user.dir") + prop.getProperty("xlspath"));
		return DataUtil.getTestData(xls, testName);

	}

	private void fillingApplicantDetail(Hashtable<String, String> data) throws InterruptedException {
		/** filling primary aplicant */
		click_actions("primaryApplicant_xpath");
		Thread.sleep(1000);
		/** reporting excel email */
		xlsResults.setCellData("Results", "Email", 2, getElement("email_xpath").getAttribute("value"));
		click("genericcontains", data.get("CurrentOccupancyPriApp"));
		click("genericcontains", data.get("MaritalStatusPriApp"));
		click("phoneType", data.get("PhoneTypePriApp"));
		click("emailType", data.get("EmailTypePriApp"));
		Thread.sleep(500);
		// TODO: need a solutuon for this
		// type_actions("firstName_xpath", data.get("FirstNamePriApp"));
		// type_actions("middleInitial_xpath", data.get("MiddleNamePriApp"));
		// type_actions("lastName_xpath", data.get("LastNamePriApp"));
		// type_actions("birthDate_xpath", data.get("BirthDatePriApp"));
		type_actions("numberDependents_xpath", data.get("NoDependentsPriApp"));
		Thread.sleep(500);
		click("homeAddress_xpath");
		Thread.sleep(500);
//		type_actions("addressAddress_xpath", data.get("AddressPriApp"));
//		type_actions("cityAddress_xpath", data.get("CityPriApp"));
//		type_actions("stateAddress_xpath", data.get("StatePriApp"));
//		type_actions("zipcodeAddress_xpath", data.get("ZipcodePriApp"));
		click_actions("okayAddress_xpath");
		Thread.sleep(500);
		type_actions("timeAtAddressYears_xpath", data.get("TimeYearsPriApp"));
		type_actions("timeAtAddressMonths_xpath", data.get("TimeMonthsPriApp"));
		// TODO:check whether PhoneNumberPriApp is needed //
		type_actions("phoneNumber_xpath", data.get("PhoneNumberPriApp"));
		// email is already filled in application
		// type_actions("email_xpath", data.get("EmailPriApp"));
		click_actions("save_xpath");
		Thread.sleep(3500);

		/** filling Co appilicant details */
		click_actions("primaryApplicant_xpath");
		Thread.sleep(1000);
		click("genericcontains", data.get("CoApplicantSpose"));
		click("genericcontains", data.get("CurrentOccupancyCo"));
		click("genericcontains", data.get("MaritalStatusCo"));
		click("phoneType", data.get("PhoneTypeCo"));
		click("emailType", data.get("EmailTypeCo"));
		Thread.sleep(500);
		type_actions("firstName_xpath", data.get("FirstNameCo"));
		type_actions("middleInitial_xpath", data.get("MiddleNameCo"));
		type_actions("lastName_xpath", data.get("LastNameCo"));
		// type_actions("birthDate_xpath", data.get("BirthDateCo"));
		type_actions("numberDependents_xpath", data.get("NoDependentsCo"));
		click("homeAddress_xpath");
		Thread.sleep(500);
//		type_actions("addressAddress_xpath", data.get("AddressCo"));
//		type_actions("cityAddress_xpath", data.get("CityCo"));
//		type_actions("stateAddress_xpath", data.get("StateCo"));
//		type_actions("zipcodeAddress_xpath", data.get("ZipcodeCo"));
		click_actions("okayAddress_xpath");
		Thread.sleep(500);
		type_actions("timeAtAddressYears_xpath", data.get("TimeYearsCo"));
		type_actions("timeAtAddressMonths_xpath", data.get("TimeMonthsCo"));
		type_actions("phoneNumber_xpath", data.get("PhoneNumberCo"));
		// type_actions("email_xpath", data.get("EmailCo"));
		click_actions("save_xpath");
		Thread.sleep(3500);

		wait("saveContinue2_xpath", 90);
		// saving applicants
		click("saveContinue2_xpath");
		Thread.sleep(1500);

	}

}
