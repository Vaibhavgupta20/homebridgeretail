package com.homebridge.retail.test;

import java.text.ParseException;
import java.util.Hashtable;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.SkipException;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.homebridge.retail.base.BaseTest;
import com.homebridge.retail.util.DataUtil;
import com.homebridge.retail.util.Xls_Reader;
import com.relevantcodes.extentreports.LogStatus;

public class SingleBorrowerRefinanceTest extends BaseTest {
	String testName = "SingleBorrowerRefinanceTest";
	Xls_Reader xls;

	@Test(dataProvider = "getData")
	public void SubmitProfile(Hashtable<String, String> data) throws ParseException, InterruptedException {
		// reporting
		test = rep.startTest(testName);
		xlsResults.setCellData("Results", "Test Case Name", 2, testName);

		// Generating the log in the report
		test.log(LogStatus.INFO, data.toString());

		// Validating the Run mode N or Y if N test will skip
		if (DataUtil.isSkip(xls, testName) || data.get("Runmode").equals("N")) {
			test.log(LogStatus.SKIP, "Skipping the test as runmode is N");
			throw new SkipException("Skipping the test as runmode is N");
		}
		test.log(LogStatus.INFO, "Starting " + testName);

		openBrowser(data.get("Browser"));
		navigate("url_AWilson");

		click("applyNow_xpath");

		// switchToPopUp();

		// filling pre app information
		wait("continueBtn_xpath", 5);
		click("continueBtn_xpath");

		Thread.sleep(3000);
		click("genericcontainsValue", data.get("LoanTypeB"));
		click("otherAppAppliying", data.get("OtherApplicant"));
		
		
		click("homeAddress_xpath");
		Thread.sleep(500);
		type_actions("addressAddress_xpath", data.get("AddressPurchaseLoan"));
		type_actions("cityAddress_xpath", data.get("CityPurchaseLoan"));
		type_actions("zipcodeAddress_xpath", data.get("ZipcodePurchaseLoan"));
		
		
		click("stateAddress_xpath");
		Thread.sleep(500);
		click("genericcontains", data.get("StatePurchaseLoan"));

		
		
		click_actions("okayAddress_xpath");
		Thread.sleep(500);
		
		
		
//		type_actions("zipcodeBegining_xpath", data.get("ZipCodeB"));
		type_actions("outstandingBalanceBegining_xpath", data.get("OutstandingBalanceB"));
		type_actions("cashOutAmountBegining_xpath", data.get("CashoutAmountB"));
		click("nextBegining_xpath");
		type_actions("firstNameBegining_xpath", data.get("FirstNameB"));
		type_actions("middleNameBegining_xpath", data.get("MiddleNameB"));
		type_actions("lastNameBegining_xpath", data.get("LastNameB"));

		// scrolling
		scrollIntoView("submitBegining_xpath");
		String email = "alicex" + getSaltString() + "@m.tst";
		System.out.println(email);
		
		
		type_actions("emailBegining_xpath", email);
		type_actions("phoneBegining_xpath", data.get("PhoneNumberB"));
		type_actions("dobBegining_xpath", data.get("DateOfBirthB"));
		click_actions("vetranBegining_xpath");
		click("generictext", data.get("MarriedB"));

		click("submitBegining_xpath");

		// creating account
		type_actions("passwordSignup_xpath", data.get("Password"));
		type_actions("password2Signup_xpath", data.get("Password"));
		click("createProfile_xpath");

		// signing in
		Thread.sleep(11000);
		click("signInSignUpBtn_xpath");

		wait("email_xpath", 5);
		type("email_xpath", email);
		type("password_xpath", data.get("Password"));
		wait("signIn_xpath", 5);
		/** clicking on sign in button */
		click_actions("signIn_xpath");

		/** loan purpose option */
		Thread.sleep(2000);
		click("generictext", data.get("SMSOptIn"));
		type_actions("phoneNumber_xpath", data.get("PhoneNumber"));
		click("loanType", data.get("LoanType"));
		// excel reporting
		String fln[] = getElement("firstLastName_xpath").getText().trim().split(" ");
		xlsResults.setCellData("Results", "PrimaryFirstName", 2, fln[0]);
		xlsResults.setCellData("Results", "PrimaryLastName", 2, fln[1]);

		click("saveContinue_xpath");
		Thread.sleep(2000);

		/** purchase loan options */
//		click("homeAddress_xpath");
//		Thread.sleep(500);
//		type_actions("addressAddress_xpath", data.get("AddressPurchaseLoan"));
//		type_actions("cityAddress_xpath", data.get("CityPurchaseLoan"));
//		type_actions("stateAddress_xpath", data.get("StatePurchaseLoan"));
//		type_actions("zipcodeAddress_xpath", data.get("ZipcodePurchaseLoan"));
//		click_actions("okayAddress_xpath");
		Thread.sleep(500);
		click("generictext", data.get("PropertyUsage"));
		click("propertyType_xpath");
		Thread.sleep(500);
		click("genericcontains", data.get("PropertyType"));
		Thread.sleep(500);
		type_actions("propertyValue_xpath", data.get("PropertyValue"));

		click("saveContinue2_xpath");
		Thread.sleep(2000);

		click_actions("addOutstandingLoan_xpath");
		Thread.sleep(500);

		type_actions("currentBalance_xpath", data.get("Current Balance"));
		type_actions("intrestRate_xpath", data.get("Interest Rate"));
		click("genericcontains", data.get("Payoff Loan"));
		click("loanmortgageType_xpath");
		Thread.sleep(500);
		click("genericcontains", data.get("Loan Type"));
		Thread.sleep(500);
		click_actions("save_xpath");
		Thread.sleep(1500);

		click("refiPurposeType_xpath");
		Thread.sleep(500);
		click("genericcontains", data.get("RefinancePurpose"));
		Thread.sleep(500);

		click("saveContinue2_xpath");
		Thread.sleep(2000);

		/** filling primary aplicant */
		click_actions("primaryApplicant_xpath");
		Thread.sleep(1000);

		/** reporting excel email */
		xlsResults.setCellData("Results", "Email", 2, getElement("email_xpath").getAttribute("value"));

		click("genericcontains", data.get("CurrentOccupancy"));
		click("genericcontains", data.get("MaritalStatus"));
		click("generictext", data.get("PhoneType"));
		click("emailType", data.get("EmailType"));
		Thread.sleep(500);

		// pre app is filling this
		// type_actions("middleInitial_xpath", data.get("MiddleNamePriApp"));
		// type_actions("birthDate_xpath", data.get("BirthDatePriApp"));
		type_actions("numberDependents_xpath", data.get("NoDependentsPriApp"));
		Thread.sleep(500);
		click("homeAddress_xpath");
		Thread.sleep(500);
//		type_actions("addressAddress_xpath", data.get("AddressPriApp"));
//		type_actions("cityAddress_xpath", data.get("CityPriApp"));
//		type_actions("stateAddress_xpath", data.get("StatePriApp"));
//		type_actions("zipcodeAddress_xpath", data.get("ZipcodePriApp"));
		click_actions("okayAddress_xpath");
		Thread.sleep(500);
		type_actions("timeAtAddressYears_xpath", data.get("TimeYearsPriApp"));
		type_actions("timeAtAddressMonths_xpath", data.get("TimeMonthsPriApp"));
		// TODO:check whether PhoneNumberPriApp is needed //
		type_actions("phoneNumber_xpath", data.get("PhoneNumberPriApp"));

		click_actions("save_xpath");
		Thread.sleep(1500);

		// saving applicants
		click("saveContinue2_xpath");
		Thread.sleep(3500);

		// filling income
		type_actions("baseSalaryAnnual_xpath", data.get("BaseSalaryAnnual"));
		type_actions("overtimeAnnual_xpath", data.get("OvertimeAnnual"));
		type_actions("bonusAnnual_xpath", data.get("BonusAnnual"));
		type_actions("commissionsAnnual_xpath", data.get("CommisionsAnnual"));

		click("saveContinue2_xpath");
		Thread.sleep(5000);
		// filling consent
		click("creditreportConsent", data.get("CreditReportConsent"));
		click("receiveSmsConsent", data.get("SMSConsent"));
		// click("edocsConsent", data.get("EDocsConsent"));
		type_actions("ssnConsent_xpath", data.get("SSNConsent"));

		click("saveContinue2_xpath");
		Thread.sleep(1500);
		// continue to full application
		click_actions("continueFullApplication_xpath");
		Thread.sleep(5000);

		// filing Employment
		click_actions("addemployment_xpath");
		Thread.sleep(500);
		click_actions("employmentType_xpath");
		Thread.sleep(500);
		click("genericcontains", data.get("EmploymentType"));
		Thread.sleep(500);
		type_actions("employerName_xpath", data.get("EmployerName"));
		click_actions("employmentAddress_xpath");
		Thread.sleep(500);
		type_actions("addressEmpAddress_xpath", data.get("AddressEmpPri"));
		type_actions("cityEmpAddress_xpath", data.get("CityEmpPri"));
		type_actions("zipcodeEmpAddress_xpath", data.get("ZipcodeEmpPri"));
		
		click("stateEmpAddress_xpath");
		Thread.sleep(500);
		click("genericcontains", data.get("StateEmpPri"));
		
		
		click_actions("okayEmpAddress_xpath");
		Thread.sleep(500);
		click("genericcontains", data.get("SelfEmployed"));
		type_actions("positionEmployment_xpath", data.get("PositionHeld"));
		type_actions("phoneEmp_xpath", data.get("PhoneNumberB"));
		type_actions("yearsWorked_xpath", data.get("YearsWorked"));
		type_actions("monthsWorked_xpath", data.get("MonthsWorked"));
		click_actions("save_xpath");
		Thread.sleep(1000);

		click("saveContinue2_xpath");
		Thread.sleep(1500);

		// filling Assets
		click_actions("addManualAssetItems_xpath");
		Thread.sleep(500);
		click_actions("AssetAccountType_xpath");
		Thread.sleep(500);
		click("genericcontains", data.get("AssetAccountType"));
		Thread.sleep(500);
		type_actions("AssetInstitutionName_xpath", data.get("AssetInstitutionName"));
		click_actions("Assetaddress_xpath");
		Thread.sleep(500);
		type_actions("AseetLineAddress_xpath", data.get("Assetaddress"));
		type_actions("AseetcityAddress_xpath", data.get("AseetcityAddress"));
		type_actions("AssetZipcodeAddress_xpath", data.get("AssetZipcodeAddress"));
//		type_actions("AssetstateAddress_xpath", data.get("AssetstateAddress"));
		
		click("AssetstateAddress_xpath");
		Thread.sleep(500);
		click("genericcontains", data.get("AssetstateAddress"));
		
		
		click_actions("AssetokayAddress_xpath");
		Thread.sleep(500);
		type_actions("AssetaccountNumber_xpath", data.get("AssetaccountNumber"));
		type_actions("AssetBalanceCurrentValue_xpath", data.get("AssetBalanceCurrentValue"));
		click_actions("save_xpath");
		Thread.sleep(1500);

		click("saveContinue2_xpath");
		Thread.sleep(1500);

		// check if address is needed

		click_actions("incompleteRealEstate_xpath");
		Thread.sleep(500);
		click("genericcontains", data.get("OccupancyTypeRE"));
//		click("genericcontains", data.get("SubjectPropertyRE"));
		click_actions("propertyTypeRE_xpath");
		Thread.sleep(500);
		click("genericcontains", data.get("PropertyTypeRE"));
		Thread.sleep(500);
		click("genericcontains", data.get("PropertyStatusRE"));
		type_actions("curruntMarketValueRE_xpath", data.get("PresentValueRE"));
		type_actions("monthlyMortgageRE_xpath", data.get("MontlyMortgageRE"));
		type_actions("homeownerDuesRE_xpath", data.get("HomeownerDuesRE"));
		type_actions("otherExpensesRE_xpath", data.get("OtherExpensesRE"));
		click_actions("save_xpath");
		Thread.sleep(1500);

		click("saveContinue2_xpath");
		Thread.sleep(1500);

		/** declarations page */

		click("outstandigjudgement", data.get("OutstandingJudgement"));
		click("declaredbankruptcy", data.get("DeclaredBankruptcy"));
		click("propertyforeclosed", data.get("PropertyForeclosed"));
		click("currentlylawsuit", data.get("CurrentlyLawsuit"));
		click("foreclosureinvolved", data.get("ForeclosureInvolved"));
		click("currentlydefaultor", data.get("CurrentlyDefaultor"));
		click("childsupport", data.get("ChildSupport"));
		click("paymentborrowed", data.get("PaymentBorrowed"));
		click("endorsercomaker", data.get("EndorserComaker"));
		click("UScitizen", data.get("Uscitizen"));
		click("permanentresidentalien", data.get("PermanentresidentAlien"));
		click("primaryresidence", data.get("PrimaryResidence"));
		click("ownershipIntrest", data.get("OwnershipIntrest"));
		click("genericcontains", data.get("PropertyTypeDec"));
		click("genericcontains", data.get("OwnershipTitleDec"));

		click("saveContinue2_xpath");
		Thread.sleep(1500);

		// govt. monitoring
		click_actions("otherThanHispanic_xpath");
		click_actions("femaleGovt_xpath");
		click_actions("asianGovt_xpath");
		click_actions("filipinoGovt_xpath");

		click("saveContinue2_xpath");
		Thread.sleep(1500);

		click_actions("submitNow_xpath");
		Thread.sleep(2500);
		isSuccess();

	}

	@BeforeTest
	public void xlsResult() {
		initXlsResult(testName);
	}

	@DataProvider()
	public Object[][] getData() {
		super.init();
		xls = new Xls_Reader(System.getProperty("user.dir") + prop.getProperty("xlspath"));
		return DataUtil.getTestData(xls, testName);
	}

}