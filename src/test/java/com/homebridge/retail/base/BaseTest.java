package com.homebridge.retail.base;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.joda.time.DateTime;
import org.joda.time.DateTimeComparator;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.DateTimeFormatterBuilder;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.asserts.SoftAssert;

import com.homebridge.retail.util.ExtentManager;
import com.homebridge.retail.util.Xls_Reader;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class BaseTest {
	public WebDriver driver;
	public Properties prop;
	public ExtentReports rep = ExtentManager.getInstance();
	public ExtentTest test;
	public SoftAssert Softassert;
	private WebDriverWait wait;
	public static Xls_Reader xlsResults;
	static Date d = new Date();
	public static String REPORT_PATH = "C:\\MylesReport\\" + d.toString().replace(":", "_").replace(" ", "_");
	public static String SCREENSHOT_PATH = REPORT_PATH + "\\screenshots\\";
	public static Hashtable<String, String> ht;

	public void init() {
		// init the prop file
		if (prop == null) {
			prop = new Properties();
			try {
				FileInputStream fs = new FileInputStream(
						System.getProperty("user.dir") + "//src//test//resources//projectconfig.properties");
				prop.load(fs);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public void initXlsResult(String testName) {
		String EXCELFILE_PATH = REPORT_PATH + "/" + testName + ".xlsx";
		String sheetName = "Results";// name of sheet
		XSSFWorkbook wb = new XSSFWorkbook();
		wb.createSheet(sheetName);
		// iterating r number of rows
		FileOutputStream fileOut;
		try {
			fileOut = new FileOutputStream(EXCELFILE_PATH);
			wb.write(fileOut);
			fileOut.flush();
			fileOut.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		// write this workbook to an Outputstream.
		catch (IOException e) {
			e.printStackTrace();
		}
		xlsResults = new Xls_Reader(EXCELFILE_PATH);
	}

	public void openBrowser(String browserType) {

		test.log(LogStatus.INFO, "Opening Browser");
		if (browserType.equals("Mozilla")) {
			System.setProperty("webdriver.gecko.driver", "C:\\drivers\\geckodriver.exe");
			driver = new FirefoxDriver();
		} else if (browserType.equals("Chrome")) {
			System.setProperty("webdriver.chrome.driver", "C:\\drivers\\chromedriver.exe");
			driver = new ChromeDriver();
		} else if (browserType.equals("ie")) {
			System.setProperty("webdriver.ie.driver", "C:\\drivers\\IEDriverServer_Win32_3.9.0\\IEDriverServer.exe");
			try {
				driver = new InternetExplorerDriver();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.manage().window().maximize();

	}

	/**
	 * explicit wait
	 * 
	 * @param locatorKey
	 * @param time
	 *            in seconds
	 */
	public void wait(String locatorKey, int time) {
		wait = new WebDriverWait(driver, time);
		wait.until(ExpectedConditions.elementToBeClickable(getElement(locatorKey)));
	}

	/**
	 * explicit wait
	 * 
	 * @param locatorKey
	 * @param macros
	 * @param time
	 *            in seconds
	 */
	public void wait(int time, String locatorKey, String... macros) {
		wait = new WebDriverWait(driver, time);
		wait.until(ExpectedConditions.elementToBeClickable(getElement(locatorKey, macros)));
	}

	/**
	 * scrolls to specific element
	 * 
	 * @param locatorKey
	 */
	public void scrollIntoView(String locatorKey) {
		WebElement element = getElement(locatorKey);
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
	}

	public void navigate(String urlKey) {
		test.log(LogStatus.INFO, "navigating to: " + prop.getProperty(urlKey));
		driver.get(prop.getProperty(urlKey));
	}

	public void click(String locatorKey) {
		test.log(LogStatus.INFO, "clicking on: " + locatorKey);
		getElement(locatorKey).click();
	}

	public void type(String locatorKey, String data) {
		test.log(LogStatus.INFO, "typing on: " + locatorKey);
		getElement(locatorKey).sendKeys(data);
	}

	// finding element and returning it
	public WebElement getElement(String locatorKey) {
		WebElement e = null;
		try {
			e = driver.findElement(getBy(locatorKey));
		} catch (Exception ex) {
			// fail the test and report the error
			reportFailure(ex.getMessage());
			ex.printStackTrace();
			Assert.fail("Failed the test - " + ex.getMessage());
		}
		return e;
	}

	/**
	 * Return the by. Mechanism is used for locating the element on page.
	 * 
	 * @param locatorKey
	 *            key should always end with _id, _name, _xpath
	 * @return {@link By} which is used in {@link findElement} for returning
	 *         {@link WebElement}
	 */
	public By getBy(String locatorKey) {
		By by = null;
		if (locatorKey.endsWith("_id"))
			by = By.id(prop.getProperty(locatorKey));
		else if (locatorKey.endsWith("_name"))
			by = By.name(prop.getProperty(locatorKey));
		else if (locatorKey.endsWith("_xpath"))
			by = By.xpath(prop.getProperty(locatorKey));
		else {
			reportFailure("Locator not correct - " + locatorKey);
			Assert.fail("Locator not correct - " + locatorKey);
		}

		return by;
	}

	public static double currecytoInt(String amount) {
		final NumberFormat format = NumberFormat.getNumberInstance();
		if (format instanceof DecimalFormat) {
			((DecimalFormat) format).setParseBigDecimal(true);
		}
		try {
			if (!amount.equals("")) {

				return format.parse(amount.replaceAll("[^\\d.,]", "")).doubleValue();
			}
		} catch (ParseException e) {

		} catch (Exception e) {

		}
		return (Double) null;

	}

	public static String currecytoInt2(String amount) {
		final NumberFormat format = NumberFormat.getNumberInstance();
		if (format instanceof DecimalFormat) {
			((DecimalFormat) format).setParseBigDecimal(true);
		}
		try {
			if (!amount.equals("")) {

				return Integer.toString(format.parse(amount.replaceAll("[^\\d.,]", "")).intValue());
			}
		} catch (ParseException e) {

		} catch (Exception e) {

		}
		return (String) null;

	}

	/************************ Actions ******************************/

	public void type_actions(String locatorKey, String data) {
		test.log(LogStatus.INFO, "typing on using actions class: " + locatorKey + " data:" + data);
		WebElement element = getElement(locatorKey);
		Actions ob = new Actions(driver);
		ob.moveToElement(element);
		ob.sendKeys(element, data).build().perform();

	}

	public void click_actions(String locatorKey) throws InterruptedException {
		test.log(LogStatus.INFO, "clicking on using actions class: " + locatorKey);
		WebElement element = getElement(locatorKey);
		Actions ob = new Actions(driver);
		ob.moveToElement(element).build().perform();
		;
		Thread.sleep(500);
		ob.click(element).build().perform();
	}

	public void doubleClick_actions(String locatorKey) throws InterruptedException {
		test.log(LogStatus.INFO, "clicking on using actions class: " + locatorKey);
		WebElement element = getElement(locatorKey);
		Actions ob = new Actions(driver);
		ob.moveToElement(element).perform();
		Thread.sleep(3000);

		ob.doubleClick(element).perform();
	}

	public void click_actions(WebElement element) {
		Actions ob = new Actions(driver);
		ob.moveToElement(element);
		ob.click(element).build().perform();
	}

	/*********************** Validations ***************************/
	public boolean verifyTitle() {
		return false;
	}

	public boolean isElementPresent(String locatorKey) {
		List<WebElement> elementList = null;
		if (locatorKey.endsWith("_id"))
			elementList = driver.findElements(By.id(prop.getProperty(locatorKey)));
		else if (locatorKey.endsWith("_name"))
			elementList = driver.findElements(By.name(prop.getProperty(locatorKey)));
		else if (locatorKey.endsWith("_xpath"))
			elementList = driver.findElements(By.xpath(prop.getProperty(locatorKey)));
		else {
			reportFailure("Locator not correct - " + locatorKey);
			Assert.fail("Locator not correct - " + locatorKey);
		}

		if (elementList.size() == 0)
			return false;
		else
			return true;
	}

	public boolean verifyText(String locatorKey, String expectedText, int rowNum, String colName)
			throws ParseException {
		locatorKey = getFullIdentifierName(locatorKey);
		String actualText = getElement(locatorKey).getText().trim();

		return compareActualExpected(actualText, expectedText, rowNum, colName);
	}

	public boolean verifyValueText(String locatorKey, String expectedText, int rowNum, String colName)
			throws ParseException {

		locatorKey = getFullIdentifierName(locatorKey);
		String actualText = getElement(locatorKey).getAttribute("value").trim();

		return compareActualExpected(actualText, expectedText, rowNum, colName);

	}

	public boolean compareActualExpected(String actualText, String expectedText, int rowNum, String colName)
			throws ParseException {

		xlsResults.setCellData("Results", "Actual " + colName, rowNum, actualText);
		xlsResults.setCellData("Results", "Expected " + colName, rowNum, expectedText);
		// System.out.println(actualText);
		// System.out.println(expectedText);
		if (actualText.equalsIgnoreCase(expectedText)) {
			xlsResults.setCellData("Results", colName + " Result", rowNum, "Pass");
			return true;
		} else if (actualText.endsWith("") && expectedText == (null)) {
			xlsResults.setCellData("Results", colName + " Result", rowNum, "Pass");
			return true;
		} else if ((actualText.equalsIgnoreCase("null") && expectedText.equalsIgnoreCase(""))
				|| (actualText.equalsIgnoreCase("") && expectedText.equalsIgnoreCase("null"))) {
			xlsResults.setCellData("Results", colName + " Result", rowNum, "Pass");
			return true;
		} else if (actualText.endsWith("") && expectedText.equals("0")) {
			xlsResults.setCellData("Results", colName + " Result", rowNum, "Pass");
			return true;
		} else if (actualText.startsWith("$")) {
			String act = Double.toString(currecytoInt(actualText));
			int actInt = (int) currecytoInt(actualText);
			String actI = Integer.toString(actInt);
			if (act.equals(expectedText) || (actI).equals(expectedText)) {
				xlsResults.setCellData("Results", colName + " Result", rowNum, "Pass");
				return true;
			}
		} else if (actualText.endsWith("%")) {
			NumberFormat defaultFormat = NumberFormat.getPercentInstance();
			Double percnt = ((defaultFormat.parse(actualText)).doubleValue() * 100);
			int perc = (int) ((defaultFormat.parse(actualText)).doubleValue() * 100);
			if (Double.toString(percnt).equals(expectedText) || Integer.toString(perc).equals(expectedText)) {
				xlsResults.setCellData("Results", colName + " Result", rowNum, "Pass");
				return true;
			}
		}
		if (colName.contains("date") || colName.contains("Date") || colName.contains("Status_130_Prospect")
				|| colName.contains("DOB")) {
			if (checkDates(actualText, expectedText)) {
				xlsResults.setCellData("Results", colName + " Result", rowNum, "Pass");
				return true;
			}
		}
		if (colName.contains("Phone")) {
			actualText = actualText.replaceAll("[-+.^:, \\[\\](){}]", "");
			if (actualText.equalsIgnoreCase(expectedText)) {
				xlsResults.setCellData("Results", colName + " Result", rowNum, "Pass");
				return true;
			}
		}
		if (ht.get(actualText) != null) {
			if (ht.get(actualText).equals(expectedText)) {
				xlsResults.setCellData("Results", colName + " Result", rowNum, "Pass");
				return true;
			}
		}
		if (ht.get(expectedText) != null) {
			if (ht.get(expectedText).equals(actualText)) {
				xlsResults.setCellData("Results", colName + " Result", rowNum, "Pass");
				return true;
			}
		}

		if (actualText.endsWith(expectedText)) {
			xlsResults.setCellData("Results", colName + " Result", rowNum, "Pass");
			return true;
		}
		// System.out.println(false);
		xlsResults.setCellData("Results", colName + " Result", rowNum, "Fail");
		return false;
	}

	public boolean checkDates(String actualText, String expectedText) {
		try {

			DateTime act = stringAsDate(actualText);
			DateTime exp = stringAsDate(expectedText);

			int i = DateTimeComparator.getDateOnlyInstance().compare(act, exp);
			if (i == 0) {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * Utility that can convert several different formatted strings into
	 * {@link Date} like
	 * <ul>
	 * <li>yyyy-MM-dd'T'HH:mm:ss
	 * <li>yyyy-MM-dd H:m:s.SSS
	 * <li>MM/d/yyyy h:m a
	 * <li>MM/d/yyyy
	 * <li>yyyy-MM-dd
	 * </ul>
	 * 
	 * @param strDate
	 * @return
	 */
	public static DateTime stringAsDate(String strDate) {
		DateTimeFormatter formatter = null;

		if (strDate == null || strDate.isEmpty()) {
			return null;
		} else {
			try {
				if (formatter == null) {
					formatter = new DateTimeFormatterBuilder()
							.appendOptional(DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss").getParser())
							.appendOptional(DateTimeFormat.forPattern("yyyy-MM-dd H:m:s.SSS").getParser())
							.appendOptional(DateTimeFormat.forPattern("MM/d/yyyy h:m a").getParser())
							.appendOptional(DateTimeFormat.forPattern("MM/d/yyyy").getParser())
							.appendOptional(DateTimeFormat.forPattern("yyyy-MM-dd").getParser()).toFormatter();
				}
				DateTime dateTime = DateTime.parse(strDate, formatter);
				return dateTime;
			} catch (Exception e) {
				return null;
			}
		}
	}

	public static void initCodes() {

		String[] value = { "Source", "AmortizationTypeID", "A", "F", "AUS_TypeId", "DU", "GUS", "LP",
				"DocumentationTypeID", "FULL", "STREAMLINE", "ALTERNAT", "REDUCED", "NODOC", "NORATIO", "LIMITEDDOC",
				"NOINCASSEM", "NOINCNOASS", "NOASSETS", "NOINCNOEMP", "NOINC", "NOVINEMAS", "NOVINAS", "NOVAS",
				"NOVINCEMP", "NOVINC", "VVEMP", "ONEPAY", "ONEPAYVVOE", "ONEPAYW2VO", "InvestorID", "ALLNT", "BBTC",
				"CENLR", "CHSJ", "CITIJ", "COHSG", "CTHSG", "CONN", "EVERB", "EXPSILVER", "GWAYBK", "GAHSG", "GINNIE",
				"HB", "INBNK", "JMBREDWD", "MTBNK", "NJHSG", "PAHSG", "PENNY", "PPLUN", "REDWD", "REGINS", "STNDR",
				"SVSOL", "SILVER", "SCHSG", "USBNK", "OCBK", "VNBNK", "FNMA", "FHLMC", "SPLSTAR", "HBFS",
				"LoanProductID", "CVCF30FX", "CVCF25FX", "CVCF20FX", "CVCF15FX", "CVCF10FX", "CVHB30FX", "CVHB25FX",
				"CVHB20FX", "CVHB15FX", "CVHB10FX", "CVCF3010/1", "CVCF307/1", "CVCF305/1", "CVCF303/1", "CVHB3010/1",
				"CVHB307/1", "CVHB305/1", "FHACF30FX", "FHACF25FX", "FHACF20FX", "FHACF15FX", "FHACF10FX", "FHAHB30FX",
				"FHAHB25FX", "FHAHB20FX", "FHAHB15FX", "FHAHB10FX", "FHACF303/1", "FHAHB305/1", "FHAHB303/1",
				"USDACF30FX", "VACF30FX", "VACF25FX", "VACF20FX", "VACF15FX", "VAHB30FX", "VAHB25FX", "VAHB20FX",
				"VAHB15FX", "VACF305/1", "VACF303/1", "VAHB305/1", "VAHB303/1", "EXP305/1", "JMB30FX", "JMB25FX",
				"JMB20FX", "JMB15FX", "JMB10FX", "JMB3015/1", "JMB3010/1", "JMB3010/1F", "JMB3010/1C", "JMB2510/1",
				"JMB2010/1", "JMB1510/1", "JMB1010/1", "JMB307/1", "JMB307/1F", "JMB307/1C", "JMB307/1IO", "JMB257/1",
				"JMB257/1IO", "JMB207/1", "JMB207/1IO", "JMB157/1", "JMB157/1IO", "JMB107/1", "JMB107/1IO", "JMB305/1",
				"JMB305/1F", "JMB305/1C", "JMB305/1IO", "JMB255/1", "JMB255/1IO", "JMB205/1", "JMB205/1IO", "JMB155/1",
				"JMB155/1IO", "JMB105/1", "JMB105/1IO", "JMB303/1", "JMB253/1", "JMB203/1", "JMB3010/5", "JMB305/5",
				"JMB303/3", "CNSTRCNF30", "CNSTRCNF25", "CNSTRCNF20", "CNSTRCNF15", "CNSTRCNF10", "CNSTRHB30",
				"CNSTRHB25", "CNSTRHB20", "CNSTRHB15", "CNSTRHB10", "HBOTCCNF30", "HBOTCCNF25", "HBOTCCNF20",
				"HBOTCCNF15", "HBOTCCNF10", "HBOTCHB30", "HBOTCHB25", "HBOTCHB20", "HBOTCHB15", "HBOTCHB10", "2ND20HE",
				"2ND15HE", "2ND10HE", "2ND20SA", "2ND15SA", "2ND10SA", "CVHB303/1", "FHACF305/1", "CVCF305/5",
				"CVHB305/5", "ELTPL30FX", "ELTPL307/1", "ELTPL305/1", "ELTPL30FIO", "ELTPL307IO", "ELTPL305IO",
				"EXP305/1IO", "SPA30FX", "SPA15FX", "SPA3010/1", "SPA307/1", "SPA305/1", "SPA30101IO", "SPA307/1IO",
				"SPA305/1IO", "LoanProgramTypeID", "203KL", "203KLDAP", "203KS", "203KSDAP", "ACCESS", "FHABND",
				"FHABNDDAP", "FXBND", "FXBNDCOM", "HMRDY", "HMSTYL", "HMSTYLCOM", "IRLCQAPPR", "IRLCQNAPPR",
				"IRLNQAPPR", "IRLNQNAPPR", "RFPLS", "RFPLSNOMI", "SRCQAPPR", "SRCQNOAPPR", "SRNQAPPR", "TXHE",
				"USDABND", "USRCQAPPR", "VABND", "E12MOBS", "E1YTR", "E24MOBS", "EAD", "EFD", "QCMOD", "NQCMOD",
				"RWDCHOICE", "RWDSELECT", "RWDCHCTX", "RWDSLCTX", "COMM", "HMRDYCOM", "HMPOSS", "HMPSSA", "HMPSSACOM",
				"SRNQNOAPPR", "HUDREO", "URCQNAPPR", "UANQNAPPR", "203HP", "203HPDAP", "203HL", "203HLDAP", "203HS",
				"203HSDAP", "EFDIO", "E24MOBSIO", "E12MOBSIO", "E1YTRIO", "EADIO", "CONVHF", "FHAHF", "VAHF", "VARENO",
				"EPFD", "EP24MOBS", "EPFDIO", "EP24MOBSIO", "EPLD", "EP12MOBS", "EPLDIO", "EP12MOBSIO", "SAFD",
				"SA24AD", "SA12AD", "SACF", "SAFDIO", "SA24ADIO", "SA12ADIO", "SACFIO", "HBHBA", "DAP",
				"LoanPurposeTypeID", "P", "R", "O", "C", "CP", "MortgageTypeID", "CONV", "FHA", "USDA", "VA", "NAGY",
				"PMI_PlanTypeID", "BP_MTH", "BP_MTHD", "BP_SP", "BP_SPR", "FHA", "LP_SP", "USDA", "VA", "PricingTierID",
				"CONF", "HB", "JMB", "EXP", "ProjClassTypeID", "EPUD", "FPUD", "1COOP", "2COOP", "PCONDO", "QCONDO",
				"RCONDO", "SCONDO", "TCONDO", "UCONDO", "VCONDO", "GNOT", "STRMR", "ESTP", "NEWP", "DETP", "UNITP",
				"RCPCRCPM", "RCPCRFHA", "RCPCRPERS", "NWCONDO", "PropertyTypeID", "SINGLE", "CONDO", "TOWNHOUSE",
				"COOP", "TWOTOFOUR", "MULTI", "MANU", "COMMERCIAL", "MIXED", "FARM", "HOMEBUS", "LAND",
				"QualifiedMortgageTypeID", "TEMPQM", "HPQM", "NONQMATRC", "NONQMATRNC", "ATREXEMPT", "QM", "TEMPQMSH",
				"TEMPQMRP", "HUDQMSH", "HUDQMRP", "GENQMSH", "GENQMRP", "NONQMSF", "NONQMRP", "RefiPurposeTypeID",
				"COHOME", "CO", "NCO", "CODEBT", "LIMITED", "RefinanceTypeID", "GOVTFULL", "VAIRRRL", "STRMAPPR",
				"STRMNOAPPR", "HFH", "PRIORFHA", "P", "Co-Borrower", "Primary Borrower" };
		String[] id = { "Application/Bluesage", "AmortizationTypeDesc", "Adjustable Rate", "Fixed Rate", "AUS_TypeDesc",
				"Desktop Underwriter", "Guaranteed Underwriting (GUS)", "Loan Prospector", "DocumentationTypeDesc",
				"Full Doc", "Streamline Refinance", "Alternative", "Reduced", "No documentation", "No Ratio",
				"Limited Documentation", "No Income", "No Income and No Assets on 1003", "No Assets on 1003",
				"No Income and No Employment on 1003", "No Income on 1003", "No Verification of Stated Income",
				"No Verification of Stated Income or assets", "No Verification of Stated Assets",
				"No Verification of Stated Income or Employment", "No Verification of Stated Income",
				"Verbal Verification of Employment", "Q = One paystub", "S = One paystub and VVOE",
				"T = One paystub and one W-2 and VVOE or one yr 1040", "InvestorName", "Alliant Credit Union",
				"BB&T Non-Conforming", "Cenlar Jumbo", "Chase Jumbo", "Citi Jumbo",
				"Colorado Housing and Finance Authority", "Connecticut Housing Finance Authority",
				"Connective Mortgage Advisory", "EverBank Residential", "Expanded", "Gateway Bank",
				"Georgia Housing DCA", "Ginnie Mae", "HomeBridge", "Investors Bank", "Jumbo", "M & T Bank",
				"New Jersey Housing and Mortgage Finance Agency", "Pennsylvania Housing Finance Agency", "Penny Mac",
				"People's United", "Redwood Residential Acquisition Corporation", "Regions", "Santander Bank",
				"ServiSolutions", "Silvergate Bank", "South Carolina State Housing Finance & Development Authority",
				"US Bank", "Oculina Bank", "Valley National Bank", "Fannie Mae", "Freddie Mac", "Simple",
				"HomeBridge Financial Services", "LoanProductDesc", "Conv Conforming 30 yr Fixed",
				"Conv Conforming 25 yr Fixed", "Conv Conforming 20 yr Fixed", "Conv Conforming 15 yr Fixed",
				"Conv Conforming 10 yr Fixed", "Conv High Balance 30 yr Fixed", "Conv High Balance 25 yr Fixed",
				"Conv High Balance 20 yr Fixed", "Conv High Balance 15 yr Fixed", "Conv High Balance 10 yr Fixed",
				"Conv Conforming 30 yr 10/1 ARM", "Conv Conforming 30 yr 7/1 ARM", "Conv Conforming 30 yr 5/1 ARM",
				"Conv Conforming 30 yr 3/1 ARM", "Conv High Balance 30 yr 10/1 ARM", "Conv High Balance 30 yr 7/1 ARM",
				"Conv High Balance 30 yr 5/1 ARM", "FHA Conforming 30 yr Fixed", "FHA Conforming 25 yr Fixed",
				"FHA Conforming 20 yr Fixed", "FHA Conforming 15 yr Fixed", "FHA Conforming 10 yr Fixed",
				"FHA High Balance 30 yr Fixed", "FHA High Balance 25 yr Fixed", "FHA High Balance 20 yr Fixed",
				"FHA High Balance 15 yr Fixed", "FHA High Balance 10 yr Fixed", "FHA Conforming 30 yr 3/1 ARM",
				"FHA High Balance 30 yr 5/1 ARM", "FHA High Balance 30 yr 3/1 ARM", "USDA Conforming 30 yr Fixed",
				"VA Conforming 30 yr Fixed", "VA Conforming 25 yr Fixed", "VA Conforming 20 yr Fixed",
				"VA Conforming 15 yr Fixed", "VA High Balance 30 yr Fixed", "VA High Balance 25 yr Fixed",
				"VA High Balance 20 yr Fixed", "VA High Balance 15 yr Fixed", "VA Conforming 30 yr 5/1 ARM",
				"VA Conforming 30 yr 3/1 ARM", "VA High Balance 30 yr 5/1 ARM", "VA High Balance 30 yr 3/1 ARM",
				"Expanded 30 yr 5/1 ARM", "Jumbo 30 yr Fixed", "Jumbo 25 yr Fixed", "Jumbo 20 yr Fixed",
				"Jumbo 15 yr Fixed", "Jumbo 10 yr Fixed", "Jumbo 30 yr 15/1 ARM", "Jumbo 30 yr 10/1 ARM",
				"Jumbo 30 yr 10/1 Foreign National ARM", "Jumbo 30 yr 10/1 Construction Perm ARM",
				"Jumbo 25 yr 10/1 ARM", "Jumbo 20 yr 10/1 ARM", "Jumbo 15 yr 10/1 ARM", "Jumbo 10 yr 10/1 ARM",
				"Jumbo 30 yr 7/1 ARM", "Jumbo 30 yr 7/1 Foreign National ARM", "Jumbo 30 yr 7/1 Construction Perm ARM",
				"Jumbo 30 yr 7/1 Interest Only ARM", "Jumbo 25 yr 7/1 ARM", "Jumbo 25 yr 7/1 Interest Only ARM",
				"Jumbo 20 yr 7/1 ARM", "Jumbo 20 yr 7/1 Interest Only ARM", "Jumbo 15 yr 7/1 ARM",
				"Jumbo 15 yr 7/1 Interest Only ARM", "Jumbo 10 yr 7/1 ARM", "Jumbo 10 yr 7/1 Interest Only ARM",
				"Jumbo 30 yr 5/1 ARM", "Jumbo 30 yr 5/1 Foreign National ARM", "Jumbo 30 yr 5/1 Construction Perm ARM",
				"Jumbo 30 yr 5/1 Interest Only ARM", "Jumbo 25 yr 5/1 ARM", "Jumbo 25 yr 5/1 Interest Only ARM",
				"Jumbo 20 yr 5/1 ARM", "Jumbo 20 yr 5/1 Interest Only ARM", "Jumbo 15 yr 5/1 ARM",
				"Jumbo 15 yr 5/1 Interest Only ARM", "Jumbo 10 yr 5/1 ARM", "Jumbo 10 yr 5/1 Interest Only ARM",
				"Jumbo 30 yr 3/1 ARM", "Jumbo 25 yr 3/1 ARM", "Jumbo 20 yr 3/1 ARM", "Jumbo 30 yr 10/5 ARM",
				"Jumbo 30 yr 5/5 ARM", "Jumbo 30 yr 3/3 ARM", "Construction Conforming 30 yr Fixed",
				"Construction Conforming 25 yr Fixed", "Construction Conforming 20 yr Fixed",
				"Construction Conforming 15 yr Fixed", "Construction Conforming 10 yr Fixed",
				"Construction High Balance 30 yr Fixed", "Construction High Balance 25 yr Fixed",
				"Construction High Balance 20 yr Fixed", "Construction High Balance 15 yr Fixed",
				"Construction High Balance 10 yr Fixed", "HB One Time Close Conforming 30 yr Fixed",
				"HB One Time Close Conforming 25 yr Fixed", "HB One Time Close Conforming 20 yr Fixed",
				"HB One Time Close Conforming 15 yr Fixed", "HB One Time Close Conforming 10 yr Fixed",
				"HB One Time Close High Balance 30 yr Fixed", "HB One Time Close High Balance 25 yr Fixed",
				"HB One Time Close High Balance 20 yr Fixed", "HB One Time Close High Balance 15 yr Fixed",
				"HB One Time Close High Balance 10 yr Fixed", "Piggy Back / HELOC 20 yr Fixed",
				"Piggy Back / HELOC 15 yr Fixed", "Piggy Back / HELOC 10 yr Fixed", "Stand Alone 2nd 20 yr Fixed",
				"Stand Alone 2nd 15 yr Fixed", "Stand Alone 2nd 10 yr Fixed", "Conv High Balance 30 yr 3/1 ARM",
				"FHA Conforming 30 yr 5/1 ARM", "Conv Conforming 30 yr 5/5 ARM", "Conv High Balance 30 yr 5/5 ARM",
				"Elite Plus 30 yr Fixed", "Elite Plus 30 yr 7/1 ARM", "Elite Plus 30 yr 5/1 ARM",
				"Elite Plus 30 yr Fixed Interest Only", "Elite Plus 30 yr 7/1  ARM Interest Only",
				"Elite Plus 30 yr 5/1  ARM Interest Only", "Expanded 30 yr 5/1  ARM Interest Only",
				"Simple Access 30 yr Fixed", "Simple Access 15 yr Fixed", "Simple Access 30 yr 10/1 ARM",
				"Simple Access 30 yr 7/1 ARM", "Simple Access 30 yr 5/1 ARM",
				"Simple Access 30 yr 10/1 ARM Interest Only", "Simple Access 30 yr 7/1 ARM Interest Only",
				"Simple Access 30 yr 5/1 ARM Interest Only", "LoanProgramTypeDesc", "FHA 203(k) Limited",
				"FHA 203(k) Limited with DAP", "FHA 203(k) Standard", "FHA 203(k)Standard with DAP", "Open Access",
				"FHA Bond", "FHA Bond With Down Payment Assistance", "Fixed Rate Bond",
				"Fixed Rate Bond With Community Second", "Home Ready", "Homestyle", "HomeStyle With Community Second",
				"IRRRL Credit Qualifying With Appraisal", "IRRRL Credit Qualifying No Appraisal",
				"IRRRL Non-Credit Qualifying With Appraisal", "IRRRL Non-Credit Qualifying No Appraisal",
				"DU Refinance Plus With MI", "DU Refinance Plus With No MI",
				"FHA Streamline Credit Qualifying With Appraisal", "FHA Streamline Credit Qualifying No Appraisal",
				"FHA Streamline Non-Credit Qualifying With Appraisal", "Texas Home Equity 50A6", "USDA Bond",
				"USDA Streamline Credit Qualifying With Appraisal", "VA Bond", "Expanded Plus 12 Month Bank Statement",
				"Expanded Plus 1 Year Tax Return", "Expanded Plus 24 Month Bank Statement",
				"Expanded Plus Asset Depletion", "Expanded Plus Full Doc", "Qualifying Construction Modification",
				"Non-Qualifying Construction Modification", "Flex", "Jumbo", "Flex Texas Home Equity 50A6",
				"Jumbo Texas Home Equity 50A6", "Community Second", "HomeReady With Community Second", "HomePossible",
				"HomePossible Advantage", "HomePossible Advantage With Community Second",
				"FHA Streamline Non-Credit Qualifying No Appraisal", "HUD REO $100 Down Payment",
				"USDA Streamline Credit Qualifying No Appraisal",
				"USDA Streamline Assist Non-Credit Qualifying No Appraisal", "FHA 203(h) Purchase",
				"FHA 203(h) Purchase with DAP", "FHA 203(h) Limited", "FHA 203(h) Limited with DAP",
				"FHA 203(h) Standard", "FHA 203(h) Standard with DAP", "Expanded Plus Full Doc Interest Only",
				"Expanded Plus 24 Month Bank Statement Interest Only",
				"Expanded Plus 12 Month Bank Statement Interest Only", "Expanded Plus 1 Year Tax Return Interest Only",
				"Expanded Plus Asset Depletion Interest Only", "Conventional Escrow Holdback", "FHA Escrow Holdback",
				"VA Escrow Holdback", "Renovation", "Elite Plus Full Doc", "Elite Plus 24 Mo Bank Statements",
				"Elite Plus Full Doc Interest Only", "Elite Plus 24 Mo Bank Statements Interest Only",
				"Elite Plus Limited Doc", "Elite Plus 12 Mo Bank Statements", "Elite Plus Limited Doc Interest Only",
				"Elite Plus 12 Mo Bank Statements Interest Only", "Simple Access Full Doc",
				"Simple Access 24 Month Alternative Doc", "Simple Access 12 Month Alternative Doc",
				"Simple Access Investor Cash Flow", "Simple Access Full Doc Interest Only",
				"Simple Access 24 Month Alt Doc Interest Only", "Simple Access 12 Month Alt Doc Interest Only",
				"Simple Access Investor Cash Flow Interest Only", "High Balance Anywhere",
				"FHA Down Payment Assistance", "LoanPurposeTypeDesc", "Purchase", "Refinance", "Other", "Construction",
				"Construction Perm", "MortgageTypeDesc", "Conventional Mortgage", "FHA", "USDA/Rural Housing Service",
				"VA", "Non Agency", "PMI_PlanTypeDesc", "Borrower Paid Monthly Premiums",
				"Borrower Paid Declining Balance Monthly Premiums", "Borrower Paid Single Premium",
				"Borrower Paid Single Premium Refundable", "FHA Insurance", "Lender Paid Single Premium",
				"USDA Guarantee", "VA Funding Fee", "PricingTierDesc", "Conforming", "High-Balance", "Jumbo",
				"Expanded", "ProjClassTypeDesc", "Lender Full Review or Refi Plus transaction - Established projects",
				"Lender Full Review - New projects", "Lender Full Review",
				"Fannie  Mae Review or Refi Plus - Project consisting of manufactured housing",
				"DU - Limited Review for new detached units only", "DU - Limited Review - Established projects",
				"DU - CPM Expedited Review/Lender Full Review - New projects",
				"DU - CPM Expedited Review/Lender Full Review - Established  projects", "DU - Fannie Review - via PERS",
				"DU - FHA and VA Appr Proj. (for conv loans w/app dated < 2/1/2010",
				"DU - Refi Plus transactions (DU or manual underwriting)", "Not in a project or development",
				"LPA - Streamlined Review", "LPA - Established Project", "LPA - New Project", "LPA - Detached Project",
				"LPA - 2- to 4-unit Project", "LPA - Reciprocal Review CPM", "LPA - Reciprocal Review FHA",
				"LPA - Reciprocal Review PERS", "Non Warrentable Condo", "PropertyTypeDesc", "Single Family",
				"Condominium", "Townhouse", "Co-operative", "Two-to-four unit property",
				"Multifamily (more than 4 units)", "Manufactured/Mobile Home", "Commercial - Non-Residential",
				"Mixed Use - Residential", "Farm", "Home and Business Combined", "Land", "QualifiedMortgageTypeDesc",
				"Temporary QM (GSE/Agency)", "Higher-Priced QM (Rebuttable Presumption)", "Non-QM - ATR Compliant",
				"Non-QM - ATR Non-Compliant", "ATR Exempt", "QM (Safe Harbor)", "Temporary QM - Safe Harbor",
				"Temporary QM - Rebuttable Presumption", "HUD QM - Safe Harbor", "HUD QM - Rebuttable Presumption",
				"General QM - Safe Harbor", "General QM - Rebuttable Presumption", "Non -QM - Safe Harbor",
				"Non -QM - Rebuttable Presumption", "RefiPurposeTypeDesc", "Cash Out Home Improvement", "Cash Out",
				"No Cash Out", "Cash Out Debt Consolidation", "Limited Cash Out", "RefinanceTypeDesc",
				"Full Documentation", "Interest Rate Reduction Refinance Loan (IRRRL)", "Streamline With Appraisal",
				"Streamline Without Appraisal", "HOPE For Homeowners", "Prior FHA Loan", "Primary Residence",
				"COBORROWER", "PRIMARY" };
		ht = new Hashtable<>();
		for (int i = 0; i < id.length; i++) {
			ht.put(id[i], value[i]);
		}

	}

	public void putNA(int rowNum, String colName) {
		xlsResults.setCellData("Results", "Actual " + colName, rowNum, "NA");
		xlsResults.setCellData("Results", "Expected " + colName, rowNum, "NA");
		xlsResults.setCellData("Results", colName + " Result", rowNum, "NA");
	}
	protected String getSaltString() {
		String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
		StringBuilder salt = new StringBuilder();
		Random rnd = new Random();
		while (salt.length() < 10) { // length of the random string.
			int index = (int) (rnd.nextFloat() * SALTCHARS.length());
			salt.append(SALTCHARS.charAt(index));
		}
		String saltStr = salt.toString();
		return saltStr;

	}
	/***************************** Reporting ********************************/

	public void reportPass(String msg) {
		test.log(LogStatus.PASS, msg);
	}

	public void reportFailure(Throwable msg) {

		test.log(LogStatus.FAIL, msg);
		takeScreenShot();
	}

	public void reportFailure(String msg) {
		try {

			if (Softassert != null) {
				Softassert.assertAll();
			}
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, e);
		}

		test.log(LogStatus.FAIL, msg);
		takeScreenShot();
	}

	public void takeScreenShot() {
		// fileName of the screenshot
		Date d = new Date();
		String screenshotFile = d.toString().replace(":", "_").replace(" ", "_") + ".png";
		// store screenshot in that file
		File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(scrFile, new File(SCREENSHOT_PATH + screenshotFile));
		} catch (IOException e) {
			e.printStackTrace();
		}
		// put screenshot file in reports
		test.log(LogStatus.INFO, "Screenshot-> " + test.addScreenCapture("./screenshots/" + screenshotFile));

	}

	/************************** Replace String ***********************/

	public String getProperty(String locator, String... macros) {
		// TODO: Change code below to ensure that 'missing from properties' is
		// clearly differentiated from 'not found on page'.
		// swap out any macros
		String property = prop.getProperty(locator);
		if (macros != null && macros.length >= 0) {
			for (int i = 0; i < macros.length; i++) {
				property = property.replaceFirst("\\$\\{[A-Za-z][A-Za-z]*\\}", macros[i]);
			}
			property = property.replaceAll("'(?<=\\p{L})'", "\\\\\"");
			property = property.replaceAll("(\\[@\\w+=)'(.*?)']", "$1\"$2\"]");
		}
		return property;
	}

	public String getPropertyApos(String locator, String... macros) {
		// TODO: Change code below to ensure that 'missing from properties' is
		// clearly differentiated from 'not found on page'.
		// swap out any macros
		String property = prop.getProperty(locator);
		if (macros != null && macros.length >= 0) {
			for (int i = 0; i < macros.length; i++) {
				property = property.replaceFirst("\\$\\{[A-Za-z][A-Za-z]*\\}", macros[i]);
			}
		}
		return property;
	}

	/** clicks using macros */
	public boolean click(String identifier, String... macros) {
		test.log(LogStatus.INFO, "clicking on using macros: " + identifier + " " + macros.toString());
		WebElement element = getElement(identifier, macros);
		if (element != null) {
			element.click();
			return true;
		}

		return false;
	}

	/** @returns webelement */
	public WebElement getElement(String namePrefix, String... macros) {
		// TODO: Change code below to ensure that 'missing from properties' is
		// clearly differentiated from 'not found on page'.
		String identifier = getFullIdentifierName(namePrefix);
		String property = getProperty(identifier, macros);
		test.log(LogStatus.INFO, "clicking on using macros: " + property);
		 System.out.println(property);
		WebElement e = null;
		try {
			if (identifier.endsWith("_id"))
				e = driver.findElement(By.id((property)));
			else if (identifier.endsWith("_xpath"))
				e = driver.findElement(By.xpath((property)));
			else if (identifier.endsWith("_name"))
				e = driver.findElement(By.name((property)));
			else if (identifier.endsWith("_classname"))
				e = driver.findElement(By.className((property)));
		} catch (Exception ex) {
			// screenshot
			ex.printStackTrace();
			Assert.fail(ex.getMessage());
		}

		return e;
	}

	public String getFullIdentifierName(String identifier) {

		String idString = String.format("%s%s", identifier, "_id");
		String nameString = String.format("%s%s", identifier, "_name");
		String xpathString = String.format("%s%s", identifier, "_xpath");

		if (prop.getProperty(idString) != null) {
			return idString.trim();
		} else if (prop.getProperty(nameString) != null) {
			return nameString.trim();
		} else if (prop.getProperty(xpathString) != null) {
			return xpathString.trim();
		} else {
			// TODO: Log no matching identifier can be found.
		}

		return null;
	}

	/****************** Application Specific ********************/

	public void switchToPopUp() {
		test.log(LogStatus.INFO, "switching to pop up");
		String parentWindowHandler = driver.getWindowHandle(); // Store your
		// parent window
		String subWindowHandler = null;

		Set<String> handles = driver.getWindowHandles(); // get all window
		// handles
		Iterator<String> iterator = handles.iterator();
		while (iterator.hasNext()) {
			subWindowHandler = iterator.next();
		}
		driver.switchTo().window(subWindowHandler); // switch to popup window

	}

	/**
	 * checks if application is submitted successfully. throws {@link Assert}
	 * exception if not successful
	 */
	public void isSuccess() {
		boolean success = isElementPresent("congrats_xpath");
		if (success) {

			test.log(LogStatus.PASS, "Submit Success");
			xlsResults.setCellData("Results", "Status", 2, "Submitted Successfuly");

		} else {
			Assert.fail("Submit Not successful");
			xlsResults.setCellData("Results", "Status", 2, "Not Submitted");
		}
	}

	/****************** Test Methods ********************/

	/**
	 * this method will run after xlsResult Method(creating new excel files) in
	 * class tests.
	 */
	@BeforeTest(dependsOnMethods = "xlsResult")
	public void initReports() {
		String sheetName = "Results";// name of sheet

		int i = 0;
		xlsResults.setCellData(sheetName, i, 1, "Test Case Name");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "PrimaryFirstName");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "PrimaryLastName");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Email");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Status");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Test Result");
		xlsResults.setCellData("Results", "Test Result", 2, "Test Fail");
	}

	@AfterMethod
	public void quit(ITestResult result) {
		if (result.getStatus() == ITestResult.FAILURE) {
			// Checking for the test if the test is failed /pass/skipped etc..
			// for reporting.
			reportFailure(result.getThrowable());

			if (xlsResults != null) {
				xlsResults.setCellData("Results", "Test Result", 2, "Test Fail");
			}

		} else if (result.getStatus() == ITestResult.SKIP) {
			test.log(LogStatus.SKIP, "Test skipped " + result.getThrowable());
		} else {
			test.log(LogStatus.PASS, "Test passed");
			if (xlsResults != null) {
				xlsResults.setCellData("Results", "Test Result", 2, "Test Passed");
			}

		}
		if (rep != null) {
			rep.endTest(test);
			rep.flush();
		}
		// quit

		if (driver != null) {
			driver.quit();
		}
	}

}
