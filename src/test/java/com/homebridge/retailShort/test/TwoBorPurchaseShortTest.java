package com.homebridge.retailShort.test;

	
	import java.text.ParseException;
	import java.util.Hashtable;

	import org.openqa.selenium.support.ui.Select;
	import org.testng.SkipException;
	import org.testng.annotations.BeforeTest;
	import org.testng.annotations.DataProvider;
	import org.testng.annotations.Test;

	import com.homebridge.retail.base.BaseTest;
	import com.homebridge.retail.util.DataUtil;
	import com.homebridge.retail.util.Xls_Reader;
	import com.relevantcodes.extentreports.LogStatus;

 public class TwoBorPurchaseShortTest extends BaseTest {
		String testName = "TwoBorPurchaseShortTest ";
		Xls_Reader xls;
	//90% application gets submitted
		@Test(dataProvider = "getData")
		public void SubmitProfile(Hashtable<String, String> data) throws ParseException, InterruptedException {
			// reporting
			test = rep.startTest(testName);
			xlsResults.setCellData("Results", "Test Case Name", 2, testName);
			// Generating the log in the report
			test.log(LogStatus.INFO, data.toString());

			// Validating the Run mode N or Y if N test will skip
			if (DataUtil.isSkip(xls, testName) || data.get("Runmode").equals("N")) {
				test.log(LogStatus.SKIP, "Skipping the test as runmode is N");
				throw new SkipException("Skipping the test as runmode is N");
			}
			test.log(LogStatus.INFO, "Starting " + testName);

			openBrowser(data.get("Browser"));
			navigate("appurl");

			click("applyNow_xpath");

			switchToPopUp();

			// signing in
			Thread.sleep(2000);
			type("email_xpath", data.get("Username"));
			type("password_xpath", data.get("Password"));
			Thread.sleep(2000);

			/** clicking on sign in button */
			click_actions("signIn_xpath");

			/** loan purpose option */
			click("generictext", data.get("SMSOptIn"));
			type_actions("phoneNumber_xpath", data.get("PhoneNumber"));
			click("loanType", data.get("LoanType"));
			// excel reporting
			String fln[] = getElement("firstLastName_xpath").getText().trim().split(" ");
			xlsResults.setCellData("Results", "PrimaryFirstName", 2, fln[0]);
			xlsResults.setCellData("Results", "PrimaryLastName", 2, fln[1]);
			click("saveContinue_xpath");
			Thread.sleep(2000);

			/** purchase loan options */
			click("generictext", data.get("PropertyLocated"));
			click("homeAddress_xpath");
			Thread.sleep(500);
			type_actions("addressAddress_xpath", data.get("AddressPurchaseLoan"));
			type_actions("cityAddress_xpath", data.get("CityPurchaseLoan"));
			type_actions("stateAddress_xpath", data.get("StatePurchaseLoan"));
			type_actions("zipcodeAddress_xpath", data.get("ZipcodePurchaseLoan"));
			click_actions("okayAddress_xpath");
			Thread.sleep(500);
			click("generictext", data.get("PropertyUsage"));
			click("propertyType_xpath");
			Thread.sleep(500);
			click("genericcontains", data.get("PropertyType"));
			Thread.sleep(500);
			type_actions("purchasePrice_xpath", data.get("PurchasePrice"));
			// type_actions("downPaymentAmount_xpath",
			// data.get("DownPaymentAmount"));

			click("saveContinue2_xpath");
			Thread.sleep(2000);

			/** filling Co appilicant details */
			click_actions("addcoapplicant_xpath");
			Thread.sleep(1000);
			click("genericcontains", data.get("CoApplicantSpose"));
			click("genericcontains", data.get("CurrentOccupancyCo"));
			click("genericcontains", data.get("MaritalStatusCo"));
			Thread.sleep(500);
			type_actions("firstName_xpath", data.get("FirstNameCo"));
			type_actions("middleInitial_xpath", data.get("MiddleNameCo"));
			type_actions("lastName_xpath", data.get("LastNameCo"));
			type_actions("birthDate_xpath", data.get("BirthDateCo"));
			type_actions("numberDependents_xpath", data.get("NoDependentsCo"));
			click("homeAddress_xpath");
			Thread.sleep(500);
			type_actions("addressAddress_xpath", data.get("AddressCo"));
			type_actions("cityAddress_xpath", data.get("CityCo"));
			type_actions("stateAddress_xpath", data.get("StateCo"));
			type_actions("zipcodeAddress_xpath", data.get("ZipcodeCo"));
			click_actions("okayAddress_xpath");
			Thread.sleep(500);
			type_actions("rentAmt_xpath", data.get("RentAmountCo"));
			type_actions("otherExpensesAmt_xpath", data.get("OtherExpensesCo"));
			type_actions("timeAtAddressYears_xpath", data.get("TimeYearsCo"));
			type_actions("timeAtAddressMonths_xpath", data.get("TimeMonthsCo"));
			type_actions("phoneNumber_xpath", data.get("PhoneNumberCo"));
			type_actions("email_xpath", data.get("EmailCo"));
			click("emailType", data.get("EmailTypeCo"));
			click("phoneType", data.get("PhoneTypeCo"));
			click_actions("save_xpath");
			Thread.sleep(1500);

			/** filling primary aplicant */
			click_actions("primaryApplicant_xpath");
			Thread.sleep(1000);

			/** reporting excel email */
			xlsResults.setCellData("Results", "Email", 2, getElement("email_xpath").getAttribute("value"));

			click("genericcontains", data.get("CurrentOccupancyPriApp"));
			click("genericcontains", data.get("MaritalStatusPriApp"));
			click("phoneType", data.get("PhoneTypePriApp"));
			click("emailType", data.get("EmailTypePriApp"));
			Thread.sleep(500);
			// TODO: need a solutuon for this
			// type_actions("firstName_xpath", data.get("FirstNamePriApp"));
			type_actions("middleInitial_xpath", data.get("MiddleNamePriApp"));
			// type_actions("lastName_xpath", data.get("LastNamePriApp"));
			type_actions("birthDate_xpath", data.get("BirthDatePriApp"));
			type_actions("numberDependents_xpath", data.get("NoDependentsPriApp"));
			Thread.sleep(500);
			click("homeAddress_xpath");
			Thread.sleep(500);
			type_actions("addressAddress_xpath", data.get("AddressPriApp"));
			type_actions("cityAddress_xpath", data.get("CityPriApp"));
			type_actions("stateAddress_xpath", data.get("StatePriApp"));
			type_actions("zipcodeAddress_xpath", data.get("ZipcodePriApp"));
			click_actions("okayAddress_xpath");
			Thread.sleep(500);
			type_actions("rentAmt_xpath", data.get("RentAmountPriApp"));
			type_actions("otherExpensesAmt_xpath", data.get("OtherExpensesPriApp"));
			type_actions("timeAtAddressYears_xpath", data.get("TimeYearsPriApp"));
			type_actions("timeAtAddressMonths_xpath", data.get("TimeMonthsPriApp"));
			// TODO:check whether PhoneNumberPriApp is needed //
			type_actions("phoneNumber_xpath", data.get("PhoneNumberPriApp"));
			// email is already filled in application
			// type_actions("email_xpath", data.get("EmailPriApp"));
			click_actions("save_xpath");
			Thread.sleep(1500);

			// saving applicants
			click("saveContinue2_xpath");
			Thread.sleep(1500);

			/** filling income primary applicant */
			type_actions("baseSalaryAnnual_xpath", data.get("BaseSalaryAnnualPriApp"));
			type_actions("overtimeAnnual_xpath", data.get("OvertimeAnnualPriApp"));
			type_actions("bonusAnnual_xpath", data.get("BonusAnnualPriApp"));
			type_actions("commissionsAnnual_xpath", data.get("CommisionsAnnualPriApp"));

			click("saveContinue2_xpath");
			Thread.sleep(500);

			/** filling income co-applicant */
			type_actions("baseSalaryAnnual_xpath", data.get("BaseSalaryAnnualCo"));
			type_actions("overtimeAnnual_xpath", data.get("OvertimeAnnualCo"));
			type_actions("bonusAnnual_xpath", data.get("BonusAnnualCo"));
			type_actions("commissionsAnnual_xpath", data.get("CommisionsAnnualCo"));

				
			click("saveContinue2_xpath");
			Thread.sleep(1500);
	// saving applicants
			click_actions("submitNowShortApp_xpath");							
			isSuccess();
			
		}
			
			@BeforeTest
			public void xlsResult() {
				initXlsResult(testName);
			}

			@DataProvider()
			public Object[][] getData() {
				super.init();
				xls = new Xls_Reader(System.getProperty("user.dir") + prop.getProperty("xlspath"));
				return DataUtil.getTestData(xls, testName);
		}

}
