package com.homebridge.retailShort.test;


	import java.text.ParseException;
	import java.util.Hashtable;
	import java.util.Iterator;
	import java.util.Set;
	import org.openqa.selenium.JavascriptExecutor;
	import org.openqa.selenium.WebElement;
	import org.openqa.selenium.interactions.Action;
	import org.openqa.selenium.interactions.Actions;
	import org.testng.SkipException;
	import org.testng.annotations.DataProvider;
	import org.testng.annotations.Test;

import com.homebridge.retail.base.BaseTest;
import com.homebridge.retail.util.DataUtil;
import com.homebridge.retail.util.Xls_Reader;
import com.relevantcodes.extentreports.LogStatus;

	public class CreateProfile  extends BaseTest {
		String testName = "ApplyNow";
		Xls_Reader xls;

		@Test(dataProvider = "getData")
		public void SubmitProfile(Hashtable<String, String> data) throws ParseException, InterruptedException {
			test = rep.startTest(testName);

			// Generating the log in the report
			test.log(LogStatus.INFO, data.toString());

			// Validating the Run mode N or Y if N test will skip
			if (DataUtil.isSkip(xls, testName) || data.get("Runmode").equals("N")) {
				test.log(LogStatus.SKIP, "Skipping the test as runmode is N");
				throw new SkipException("Skipping the test as runmode is N");
			}
			test.log(LogStatus.INFO, "Starting " + testName);

			openBrowser(data.get("Browser"));
			navigate("appurl");
			click("applyNow_xpath");

			String parentWindowHandler = driver.getWindowHandle(); // Store your
																	// parent window
			String subWindowHandler = null;

			Set<String> handles = driver.getWindowHandles(); // get all window
																// handles
			Iterator<String> iterator = handles.iterator();
			while (iterator.hasNext()) {
				subWindowHandler = iterator.next();
			}
			driver.switchTo().window(subWindowHandler); // switch to popup window

			type("email_xpath", data.get("Username"));
			type("password_xpath", data.get("Password"));
			Thread.sleep(2000);
			
			
			//clicking on sign in button
			WebElement element = getElement("signIn_xpath");
			Actions ob = new Actions(driver);
			ob.moveToElement(element);
			ob.click(element);
			Action action  = ob.build();
			action.perform();
			
			
			click("loanType_xpath", data.get("LoanType"));
			click("smsOptIn_xpath", data.get("SMSOptIn"));
			type("phoneNubme_xpath", data.get(""));
			click("saveContinue_xpath");
			
			
		}

		// @AfterMethod
		// public void quit() {
		// rep.endTest(test);
		// rep.flush();
		// }

		@DataProvider()
		public Object[][] getData() {
			super.init();
			xls = new Xls_Reader(System.getProperty("user.dir") + prop.getProperty("xlspath"));
			return DataUtil.getTestData(xls, testName);

		}

	}



