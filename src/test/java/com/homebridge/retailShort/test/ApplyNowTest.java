package com.homebridge.retailShort.test;

import java.text.ParseException;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.homebridge.retail.base.BaseTest;
import com.homebridge.retail.util.DataUtil;
import com.homebridge.retail.util.Xls_Reader;
import com.relevantcodes.extentreports.LogStatus;

public class ApplyNowTest extends BaseTest {
	String testName = "ApplyNowTest";
	Xls_Reader xls;

	@Test(dataProvider = "getData")
	public void SubmitProfile(Hashtable<String, String> data) throws ParseException, InterruptedException {
		// reporting
		test = rep.startTest(testName);

		// Generating the log in the report
		test.log(LogStatus.INFO, data.toString());

		// Validating the Run mode N or Y if N test will skip
		if (DataUtil.isSkip(xls, testName) || data.get("Runmode").equals("N")) {
			test.log(LogStatus.SKIP, "Skipping the test as runmode is N");
			throw new SkipException("Skipping the test as runmode is N");
		}
		test.log(LogStatus.INFO, "Starting " + testName);

		openBrowser(data.get("Browser"));
		navigate("appurl");
		click("applyNow_xpath");

		switchToPopUp();

		type("email_xpath", data.get("Username"));
		type("password_xpath", data.get("Password"));
		Thread.sleep(2000);

		// clicking on sign in button
		click_actions("signIn_xpath");

		// loan purpose option
		click("loanType", data.get("LoanType"));
		click("generictext", data.get("SMSOptIn"));

		type_actions("phoneNumber_xpath", data.get("PhoneNumber"));
		click("saveContinue_xpath");

		// purchase loan options
		Thread.sleep(2000);
		click("generictext", data.get("PropertyLocated"));

		click("generictext", data.get("PropertyUsage"));
		click("propertyType_xpath");
		Thread.sleep(500);
		click("genericcontains", data.get("PropertyType"));
		Thread.sleep(500);
		type_actions("searchZipcode_xpath", data.get("ZipCode"));
		type_actions("purchasePrice_xpath", data.get("PurchasePrice"));
		type_actions("downPaymentAmount_xpath", data.get("DownPaymentAmount"));
		click("saveContinue2_xpath");
		Thread.sleep(2000);

		// filling primary aplicant click_actions("primaryApplicant_xpath");
		Thread.sleep(1000);
		click("genericcontains", data.get("CurrentOccupancy"));
		click("genericcontains", data.get("MaritalStatus"));
		click("genericcontains", data.get("PhoneType"));
		click("emailType", data.get("EmailType"));
		Thread.sleep(500);
		type_actions("birthDate_xpath", data.get("BirthDateCo"));
		type_actions("numberDependents_xpath", data.get("NoDependentsCo"));
		Thread.sleep(500);
		type_actions("dependentAges_xpath", data.get("DependentAges"));
		click("homeAddress_xpath");
		Thread.sleep(500);
		type_actions("addressAddress_xpath", data.get("AddressCo"));
		type_actions("cityAddress_xpath", data.get("CityCo"));
		type_actions("stateAddress_xpath", data.get("StateCo"));
		type_actions("zipcodeAddress_xpath", data.get("ZipcodeCo"));
		click_actions("okayAddress_xpath");
		Thread.sleep(500);
		type_actions("timeAtAddressYears_xpath", data.get("TimeYearsCo"));
		type_actions("timeAtAddressMonths_xpath", data.get("TimeMonthsCo"));
		// TODO:check whether PhoneNumberCo is needed //
		type_actions("phoneNumber_xpath", data.get("PhoneNumberCo"));
		click_actions("save_xpath");
		Thread.sleep(1500);

		/** filling Co appilicant details */
		click_actions("addcoapplicant_xpath");
		Thread.sleep(1000);
		click("genericcontains", data.get("CoApplicantSpose"));
		click("genericcontains", data.get("CurrentOccupancy"));
		click("genericcontains", data.get("MaritalStatus"));
		click("genericcontains", data.get("PhoneType"));
		click("emailType", data.get("EmailType"));
		Thread.sleep(500);
		type_actions("firstName_xpath", data.get("FirstNameCo"));
		type_actions("middleInitial_xpath", data.get("MiddleNameCo"));
		type_actions("lastName_xpath", data.get("LastNameCo"));
		type_actions("birthDate_xpath", data.get("BirthDateCo"));
		type_actions("numberDependents_xpath", data.get("NoDependentsCo"));
		Thread.sleep(500);
		type_actions("dependentAges_xpath", data.get("DependentAges"));
		click("homeAddress_xpath");
		Thread.sleep(500);
		type_actions("addressAddress_xpath", data.get("AddressCo"));
		type_actions("cityAddress_xpath", data.get("CityCo"));
		type_actions("stateAddress_xpath", data.get("StateCo"));
		type_actions("zipcodeAddress_xpath", data.get("ZipcodeCo"));
		click_actions("okayAddress_xpath");
		Thread.sleep(500);
		type_actions("timeAtAddressYears_xpath", data.get("TimeYearsCo"));
		type_actions("timeAtAddressMonths_xpath", data.get("TimeMonthsCo"));
		type_actions("phoneNumber_xpath", data.get("PhoneNumberCo"));
		type_actions("email_xpath", data.get("EmailCo"));
		click_actions("save_xpath");

		// saving applicants
		Thread.sleep(1500);
		click("saveContinue2_xpath");
		// filling income Thread.sleep(1500);
		type_actions("baseSalaryAnnual_xpath", data.get("BaseSalaryAnnual"));
		type_actions("overtimeAnnual_xpath", data.get("OvertimeAnnual"));
		type_actions("bonusAnnual_xpath", data.get("BonusAnnual"));
		type_actions("commissionsAnnual_xpath", data.get("CommisionsAnnual"));

		Thread.sleep(1500);
		click("saveContinue2_xpath");
		Thread.sleep(500);

		type_actions("baseSalaryAnnual_xpath", data.get("BaseSalaryAnnual"));
		type_actions("overtimeAnnual_xpath", data.get("OvertimeAnnual"));
		type_actions("bonusAnnual_xpath", data.get("BonusAnnual"));
		type_actions("commissionsAnnual_xpath", data.get("CommisionsAnnual"));
		Thread.sleep(1500);
		click("saveContinue2_xpath");

		// Employment

		// full application
		// TODO:check this code for working on a new account
		Thread.sleep(2000);
		click_actions("okayAddress_xpath");
		Thread.sleep(500);
		click_actions("continueFullApplication_xapth");

		Thread.sleep(2000);
		click_actions("addemployment_xpath");
		Thread.sleep(500);
		click_actions("employmentType_xpath");
		Thread.sleep(500);
		click("genericcontains", data.get("EmploymentType"));
		Thread.sleep(500);

		type_actions("employerName_xpath", data.get("EmployerName"));
		click_actions("employmentAddress_xpath");
		Thread.sleep(500);
		type_actions("addressEmpAddress_xpath", data.get("AddressEmpPri"));
		type_actions("cityEmpAddress_xpath", data.get("CityEmpPri"));
		type_actions("stateEmpAddress_xpath", data.get("StateEmpPri"));
		type_actions("zipcodeEmpAddress_xpath", data.get("ZipcodeEmpPri"));
		click_actions("okayEmpAddress_xpath");
		Thread.sleep(500);
		click("genericcontains", data.get("SelfEmployed"));
		click("stillEmployedHere", data.get("StillEmployedHere"));
		type_actions("positionEmployment_xpath", data.get("PositionHeld"));
		type_actions("yearsWorked_xpath", data.get("YearsWorked"));
		type_actions("monthsWorked_xpath", data.get("MonthsWorked"));
		click_actions("save_xpath");

		// Assets
		click_actions("addcoapplicant_xpath");
		click("AssetAppliesT0_xpath");
		click("AssetAppliesT0Co_xpath");
		type_actions("AssetAccountType_xpath", data.get("AccountType"));
		type_actions("AssetInstitutionName_xpath", data.get("InstitutionName"));
		type_actions("Assetaddress_xpath", data.get("AssetAddress"));
		type_actions("AseetcityAddress_xpath", data.get("AssetCity"));
		type_actions("AssetstateAddress_xpath", data.get("AssetStateCo"));
		click_actions("AssetokayAddress_xpath");
		type_actions("AssetaccountNumber_xpath", data.get("AssetaccountNumber"));
		type_actions("AssetBalanceCurrentValue_xpath", data.get("AssetBalanceCurrentValue"));
		click_actions("save_xpath");
		click("saveContinue2_xpath");

		Thread.sleep(500);

	}
	// @AfterMethod
	// public void quit() {
	// rep.endTest(test);
	// rep.flush();
	// }

	@DataProvider()
	public Object[][] getData() {
		super.init();
		xls = new Xls_Reader(System.getProperty("user.dir") + prop.getProperty("xlspath"));
		return DataUtil.getTestData(xls, testName);

	}

}
