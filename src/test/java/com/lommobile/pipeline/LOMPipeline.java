package com.lommobile.pipeline;

import java.text.ParseException;
import java.util.Hashtable;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.homebridge.retail.base.BaseTest;
import com.homebridge.retail.util.DataUtil;
import com.homebridge.retail.util.Xls_Reader;
import com.relevantcodes.extentreports.LogStatus;

public class LOMPipeline extends BaseTest {
	String testName = "LOMPipeline";
	Xls_Reader xls;

	@Test(dataProvider = "getData")

	public void SubmitProfile(Hashtable<String, String> data) throws ParseException, InterruptedException {

		/**
		 * reporting
		 */
		test = rep.startTest(data.get("TestCase"));

		/**
		 * Generating the log in the report
		 */
		test.log(LogStatus.INFO, data.toString());

		/**
		 * Validating the Run mode N or Y if N test will skip
		 */
		if (DataUtil.isSkip(xls, testName) || data.get("Runmode").equals("N")) {
			test.log(LogStatus.SKIP, "Skipping the test as runmode is N");
			throw new SkipException("Skipping the test as runmode is N");
		}

		test.log(LogStatus.INFO, "Starting " + testName);
		xlsResults.setCellData("Results", "Test Case Name", DataUtil.getFilledRows(xlsResults, "Results"),
				data.get("TestCase"));
		int filledRows = DataUtil.getFilledRows(xlsResults, "Results") - 1;
		xlsResults.setCellData("Results", "Test Result LOM", filledRows, "Fail");
		xlsResults.setCellData("Results", "loUserName", filledRows, data.get("loUserName"));
		openBrowser(data.get("Browser"));
		navigate("URL_lompROD");
		/**
		 * signing in
		 */
		Thread.sleep(2000);
		type("loUserName_xpath", data.get("loUserName").trim());
		type("loPassword_xpath", data.get("loPassword"));
		click("losubmit_xpath");
		Thread.sleep(2000);

		click("menuLo_id");
		Thread.sleep(3000);
		wait("myLoansMenuLo_xpath", 5);
		click("myLoansMenuLo_xpath");
		Thread.sleep(1000);
		// wait("searchTabLo_xpath", 5);
		// click("searchTabLo_xpath"

		/**
		 * Searching the loan number
		 */
		type("searchTextbox_xpath", data.get("Loan_Number"));
		Thread.sleep(3000);
		click_actions("searchedLoanNumber_xpath");
		Thread.sleep(5000);
		wait("getLoanNumberSelectedLoan_xpath", 5);
		/*
		 * // TODO: there is bug, cannot find anything
		 * xlsResults.setCellData("Results", "Borr1_DOB",
		 * DataUtil.getFilledRows(xlsResults, "Results") - 1,
		 * getElement("OccupancyLOM_xpath").getText());
		 * xlsResults.setCellData("Results", "Borr1_Email_Address",
		 * DataUtil.getFilledRows(xlsResults, "Results") - 1,
		 * getElement("PurchasePriceLOM_xpath").getText());
		 * 
		 * // where is pull card // my contacts
		 * xlsResults.setCellData("Results", "Loan_Officer_UserID",
		 * DataUtil.getFilledRows(xlsResults, "Results") - 1,
		 * getElement("NameLOM_xpath").getText());
		 */

		// TODO: in conditions page
		// xlsResults.setCellData("Results", "Conditions__Title",
		// DataUtil.getFilledRows(xlsResults, "Results") - 1,
		// getElement("NameLOM_xpath").getText());
		// xlsResults.setCellData("Results", "Conditions__Description",
		// DataUtil.getFilledRows(xlsResults, "Results") - 1,
		// getElement("NameLOM_xpath").getText());

		xlsResults.setCellData("Results", "Actual LoanNumberLOM", filledRows,
				getElement("getloanNumber_xpath").getText());
		xlsResults.setCellData("Results", "Expected LoanNumberLOM", filledRows, data.get("Loan_Number"));
		try {
			Assert.assertEquals(getElement("getloanNumber_xpath").getText(), data.get("Loan_Number"));
			xlsResults.setCellData("Results", "LoanNumberLOM Result", filledRows, "Pass");
		} catch (AssertionError e) {
			xlsResults.setCellData("Results", "LoanNumberLOM Result", filledRows, "Fail");
		}

		// first name and last name
		String[] fullName = getElement("NameLOM_xpath").getText().split(" ");
		xlsResults.setCellData("Results", "Actual Borr1_First_Name", filledRows, fullName[0]);
		xlsResults.setCellData("Results", "Expected Borr1_First_Name", filledRows, data.get("Borr1_First_Name"));
		// validating and reporting in excel
		try {
			Assert.assertEquals(fullName[0], data.get("Borr1_First_Name"));
			xlsResults.setCellData("Results", "Borr1_First_Name Result", filledRows, "Pass");
		} catch (AssertionError e) {
			xlsResults.setCellData("Results", "Borr1_First_Name Result", filledRows, "Fail");
		}
		xlsResults.setCellData("Results", "Actual Borr1_Last_Name", filledRows, fullName[1]);
		xlsResults.setCellData("Results", "Expected Borr1_Last_Name", filledRows, data.get("Borr1_Last_Name"));
		try {
			Assert.assertEquals(fullName[1], data.get("Borr1_Last_Name"));
			xlsResults.setCellData("Results", "Borr1_Last_Name Result", filledRows, "Pass");
		} catch (AssertionError e) {
			xlsResults.setCellData("Results", "Borr1_Last_Name Result", filledRows, "Fail");
		}

		// Borr1_Total_Income Borr2_Total_Income
		if (isElementPresent("borr2NameLOM_xpath")) {

			// borr2 first name and last name
			String[] fullNameorr2 = getElement("borr2NameLOM_xpath").getText().split(" ");
			xlsResults.setCellData("Results", "Actual Borr2_First_Name", filledRows, fullNameorr2[0]);
			xlsResults.setCellData("Results", "Expected Borr2_First_Name", filledRows, data.get("Borr2_First_Name"));
			// validating and reporting in excel
			try {
				Assert.assertEquals(fullNameorr2[0], data.get("Borr2_First_Name"));
				xlsResults.setCellData("Results", "Borr2_First_Name Result", filledRows, "Pass");
			} catch (AssertionError e) {
				xlsResults.setCellData("Results", "Borr2_First_Name Result", filledRows, "Fail");
			}
			xlsResults.setCellData("Results", "Expected Borr2_Last_Name", filledRows, data.get("Borr2_Last_Name"));
			xlsResults.setCellData("Results", "Actual Borr2_Last_Name", filledRows, fullNameorr2[1]);
			try {
				Assert.assertEquals(fullNameorr2[1], data.get("Borr2_Last_Name"));
				xlsResults.setCellData("Results", "Borr2_Last_Name Result", filledRows, "Pass");
			} catch (AssertionError e) {
				xlsResults.setCellData("Results", "Borr2_Last_Name Result", filledRows, "Fail");
			}
		} else {
			// TODO: handle exception
			xlsResults.setCellData("Results", "Actual Borr2_First_Name", filledRows, "NA");
			xlsResults.setCellData("Results", "Expected Borr2_First_Name", filledRows, "NA");
			xlsResults.setCellData("Results", "Borr2_First_Name Result", filledRows, "NA");
			xlsResults.setCellData("Results", "Actual Borr2_Last_Name", filledRows, "NA");
			xlsResults.setCellData("Results", "Expected Borr2_Last_Name", filledRows, "NA");
			xlsResults.setCellData("Results", "Borr2_Last_Name Result", filledRows, "NA");

		}

		// borr3 first name and last name
		if (isElementPresent("borr3NameLOM_xpath")) {
			String[] fullNameorr3 = getElement("borr3NameLOM_xpath").getText().split(" ");
			xlsResults.setCellData("Results", "Actual Borr3_First_Name", filledRows, fullNameorr3[0]);
			xlsResults.setCellData("Results", "Expected Borr3_First_Name", filledRows, data.get("Borr3_First_Name"));
			// validating and reporting in excel
			try {
				Assert.assertEquals(fullNameorr3[0], data.get("Borr3_First_Name"));
				xlsResults.setCellData("Results", "Borr3_First_Name Result", filledRows, "Pass");
			} catch (AssertionError e) {
				xlsResults.setCellData("Results", "Borr3_First_Name Result", filledRows, "Fail");
			}
			xlsResults.setCellData("Results", "Actual Borr3_Last_Name", filledRows, fullNameorr3[1]);
			xlsResults.setCellData("Results", "Expected Borr3_Last_Name", filledRows, data.get("Borr3_Last_Name"));
			try {
				Assert.assertEquals(fullNameorr3[1], data.get("Borr3_Last_Name"));
				xlsResults.setCellData("Results", "Borr3_Last_Name Result", filledRows, "Pass");
			} catch (AssertionError e) {
				xlsResults.setCellData("Results", "Borr3_Last_Name Result", filledRows, "Fail");
			}
		} else {
			xlsResults.setCellData("Results", "Actual Borr3_First_Name", filledRows, "NA");
			xlsResults.setCellData("Results", "Expected Borr3_First_Name", filledRows, "NA");
			xlsResults.setCellData("Results", "Borr3_First_Name Result", filledRows, "NA");
			xlsResults.setCellData("Results", "Actual Borr3_Last_Name", filledRows, "NA");
			xlsResults.setCellData("Results", "Expected Borr3_Last_Name", filledRows, "NA");
			xlsResults.setCellData("Results", "Borr3_Last_Name Result", filledRows, "NA");

		}

		if (isElementPresent("borr4NameLOM_xpath")) {
			// borr4 first name and last name
			String[] fullNameorr4 = getElement("borr4NameLOM_xpath").getText().split(" ");
			xlsResults.setCellData("Results", "Actual Borr4_First_Name", filledRows, fullNameorr4[0]);
			xlsResults.setCellData("Results", "Expected Borr4_First_Name", filledRows, data.get("Borr4_First_Name"));
			// validating and reporting in excel
			try {
				Assert.assertEquals(fullNameorr4[0], data.get("Borr4_First_Name"));
				xlsResults.setCellData("Results", "Borr4_First_Name Result", filledRows, "Pass");
			} catch (AssertionError e) {
				xlsResults.setCellData("Results", "Borr4_First_Name Result", filledRows, "Fail");
			}
			xlsResults.setCellData("Results", "Actual Borr4_Last_Name", filledRows, fullNameorr4[1]);
			xlsResults.setCellData("Results", "Expected Borr4_Last_Name", filledRows, data.get("Borr4_Last_Name"));
			try {
				Assert.assertEquals(fullNameorr4[1], data.get("Borr4_Last_Name"));
				xlsResults.setCellData("Results", "Borr4_Last_Name Result", filledRows, "Pass");
			} catch (AssertionError e) {
				xlsResults.setCellData("Results", "Borr4_Last_Name Result", filledRows, "Fail");
			}
		} else {
			xlsResults.setCellData("Results", "Actual Borr4_First_Name", filledRows, "NA");
			xlsResults.setCellData("Results", "Expected Borr4_First_Name", filledRows, "NA");
			xlsResults.setCellData("Results", "Borr4_First_Name Result", filledRows, "NA");
			xlsResults.setCellData("Results", "Actual Borr4_Last_Name", filledRows, "NA");
			xlsResults.setCellData("Results", "Expected Borr4_Last_Name", filledRows, "NA");
			xlsResults.setCellData("Results", "Borr4_Last_Name Result", filledRows, "NA");

		}

		xlsResults.setCellData("Results", "Actual Borr1_Total_Income", filledRows,

				getElement("borr1IncomeLOM_xpath").getText());
		xlsResults.setCellData("Results", "Expected Borr1_Total_Income", filledRows, data.get("Borr1_Total_Income"));
		try {

			String putchaprice = Double.toString(currecytoInt(getElement("borr1IncomeLOM_xpath").getText()));
			int actInt = (int) currecytoInt(getElement("borr1IncomeLOM_xpath").getText());
			// TODO: round of
			if ((putchaprice).equals(data.get("Borr1_Total_Income"))
					|| (actInt) == ((int) (Math.round(Double.parseDouble(data.get("Borr1_Total_Income")))))) {
				xlsResults.setCellData("Results", "Borr1_Total_Income Result", filledRows, "Pass");
			} else {
				xlsResults.setCellData("Results", "Borr1_Total_Income Result", filledRows, "Fail");
			}

		} catch (AssertionError e) {
		}
		if (isElementPresent("borr2IncomeLOM_xpath")) {
			xlsResults.setCellData("Results", "Actual Borr2_Total_Income", filledRows,
					getElement("borr2IncomeLOM_xpath").getText());
			xlsResults.setCellData("Results", "Expected Borr2_Total_Income", filledRows,
					data.get("Borr2_Total_Income"));

			try {

				String putchaprice = currecytoInt2(getElement("borr2IncomeLOM_xpath").getText());
				int actInt = (int) currecytoInt(getElement("borr2IncomeLOM_xpath").getText());

				if ((putchaprice).equals(data.get("Borr2_Total_Income"))
						|| (actInt) == ((int) (Math.round(Double.parseDouble(data.get("Borr2_Total_Income")))))) {
					xlsResults.setCellData("Results", "Borr2_Total_Income Result", filledRows, "Pass");
				} else {
					xlsResults.setCellData("Results", "Borr2_Total_Income Result", filledRows, "Fail");
				}

			} catch (AssertionError e) {
			}
			if (isElementPresent("borr3IncomeLOM_xpath")) {
				xlsResults.setCellData("Results", "Actual Borr3_Total_Income", filledRows,
						getElement("borr3IncomeLOM_xpath").getText());
				xlsResults.setCellData("Results", "Expected Borr3_Total_Income", filledRows,
						data.get("Borr3_Total_Income"));

				try {

					String putchaprice = currecytoInt2(getElement("borr3IncomeLOM_xpath").getText());
					int actInt = (int) currecytoInt(getElement("borr3IncomeLOM_xpath").getText());
					if ((putchaprice).equals(data.get("Borr3_Total_Income"))
							|| (actInt) == ((int) (Math.round(Double.parseDouble(data.get("Borr3_Total_Income")))))) {
						xlsResults.setCellData("Results", "Borr3_Total_Income Result", filledRows, "Pass");
					} else {
						xlsResults.setCellData("Results", "Borr3_Total_Income Result", filledRows, "Fail");
					}

				} catch (AssertionError e) {
				}
				if (isElementPresent("borr4IncomeLOM_xpath")) {
					xlsResults.setCellData("Results", "Actual Borr4_Total_Income", filledRows,
							getElement("borr4IncomeLOM_xpath").getText());
					xlsResults.setCellData("Results", "Expected Borr4_Total_Income", filledRows,
							data.get("Borr4_Total_Income"));

					try {

						String putchaprice = currecytoInt2(getElement("borr4IncomeLOM_xpath").getText());

						int actInt = (int) currecytoInt(getElement("borr4IncomeLOM_xpath").getText());
						if ((putchaprice).equals(data.get("Borr4_Total_Income")) || (actInt) == ((int) (Math
								.round(Double.parseDouble(data.get("Borr4_Total_Income")))))) {
							xlsResults.setCellData("Results", "Borr4_Total_Income Result", filledRows, "Pass");
						} else {
							xlsResults.setCellData("Results", "Borr4_Total_Income Result", filledRows, "Fail");
						}

					} catch (AssertionError e) {
					}
				} else {
					xlsResults.setCellData("Results", "Actual Borr4_Total_Income", filledRows, "NA");
					xlsResults.setCellData("Results", "Expected Borr4_Total_Income", filledRows, "NA");
					xlsResults.setCellData("Results", "Borr4_Total_Income Result", filledRows, "NA");
				}
			} else {
				xlsResults.setCellData("Results", "Actual Borr4_Total_Income", filledRows, "NA");
				xlsResults.setCellData("Results", "Expected Borr4_Total_Income", filledRows, "NA");
				xlsResults.setCellData("Results", "Borr4_Total_Income Result", filledRows, "NA");
				xlsResults.setCellData("Results", "Actual Borr3_Total_Income", filledRows, "NA");
				xlsResults.setCellData("Results", "Expected Borr3_Total_Income", filledRows, "NA");
				xlsResults.setCellData("Results", "Borr3_Total_Income Result", filledRows, "NA");
			}
		} else {
			xlsResults.setCellData("Results", "Actual Borr2_Total_Income", filledRows, "NA");
			xlsResults.setCellData("Results", "Expected Borr2_Total_Income", filledRows, "NA");
			xlsResults.setCellData("Results", "Borr2_Total_Income Result", filledRows, "NA");

			xlsResults.setCellData("Results", "Actual Borr3_Total_Income", filledRows, "NA");
			xlsResults.setCellData("Results", "Expected Borr3_Total_Income", filledRows, "NA");
			xlsResults.setCellData("Results", "Borr3_Total_Income Result", filledRows, "NA");

			xlsResults.setCellData("Results", "Actual Borr4_Total_Income", filledRows, "NA");
			xlsResults.setCellData("Results", "Expected Borr4_Total_Income", filledRows, "NA");
			xlsResults.setCellData("Results", "Borr4_Total_Income Result", filledRows, "NA");

		}
		xlsResults.setCellData("Results", "Expected Property_Zip", filledRows, data.get("Property_Zip"));
		xlsResults.setCellData("Results", "Expected Property_State", filledRows, data.get("Property_State"));

		xlsResults.setCellData("Results", "Expected Property_City", filledRows, data.get("Property_City"));

		String propertyFull = getElement("PropertyAddress_xpath").getText();
		String[] property = null;
		String[] propertyZipState = null;
		try {
			property = propertyFull.split(",");

			xlsResults.setCellData("Results", "Actual Property_City", filledRows, propertyFull);

			propertyZipState = property[property.length - 1].split(" ");
			xlsResults.setCellData("Results", "Actual Property_Zip", filledRows, propertyFull);
			xlsResults.setCellData("Results", "Actual Property_State", filledRows, propertyFull);
		} catch (java.lang.ArrayIndexOutOfBoundsException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		if (data.get("Property_Address").equals("")) {
			xlsResults.setCellData("Results", "Actual Property_Address", filledRows, "NA");
			xlsResults.setCellData("Results", "Expected Property_Address", filledRows, "NA");
			xlsResults.setCellData("Results", "Property_Address Result", filledRows, "NA");
		} else {
			xlsResults.setCellData("Results", "Actual Property_Address", filledRows,
					getElement("PropertyAddress_xpath").getText());
			xlsResults.setCellData("Results", "Expected Property_Address", filledRows,
					data.get("Property_Address"));
			try {
				Assert.assertEquals(property[0].trim(), data.get("Property_Address"));
				xlsResults.setCellData("Results", "Property_Address Result", filledRows, "Pass");
			} catch (AssertionError e) {
				xlsResults.setCellData("Results", "Property_Address Result", filledRows, "Fail");
			}
		}
		if (propertyFull.equals("null null")) {

			if (data.get("Property_Address").equalsIgnoreCase("null")) {
				xlsResults.setCellData("Results", "Property_Address Result", filledRows, "Pass");
			} else {
				xlsResults.setCellData("Results", "Property_Address Result", filledRows, "Fail");
			}

			if (data.get("Property_City").equalsIgnoreCase("null")) {
				xlsResults.setCellData("Results", "Property_City Result", filledRows, "Pass");
			} else {
				xlsResults.setCellData("Results", "Property_City Result", filledRows, "Fail");
			}

			if (data.get("Property_Zip").equalsIgnoreCase("null")) {
				xlsResults.setCellData("Results", "Property_Zip Result", filledRows, "Pass");
			} else {
				xlsResults.setCellData("Results", "Property_Zip Result", filledRows, "Fail");
			}

			if (data.get("Property_State").equalsIgnoreCase("null")) {
				xlsResults.setCellData("Results", "Property_State Result", filledRows, "Pass");
			} else {
				xlsResults.setCellData("Results", "Property_State Result", filledRows, "Fail");
			}

		} else {

			try {
				if (propertyFull.startsWith(data.get("Property_City"))
						|| property[1].trim().equals(data.get("Property_City"))) {

					xlsResults.setCellData("Results", "Property_City Result", filledRows, "Pass");
				} else {
					xlsResults.setCellData("Results", "Property_City Result", filledRows, "Fail");
				}
				if (propertyFull.endsWith(data.get("Property_Zip"))) {
					xlsResults.setCellData("Results", "Property_Zip Result", filledRows, "Pass");
				} else {
					xlsResults.setCellData("Results", "Property_Zip Result", filledRows, "Fail");
				}

				if (propertyZipState[1].trim().equals(data.get("Property_State"))) {
					xlsResults.setCellData("Results", "Property_State Result", filledRows, "Pass");
				} else if (propertyFull.equals("null null") && data.get("Property_State").equals("")) {
					xlsResults.setCellData("Results", "Property_State Result", filledRows, "Pass");
				} else {
					xlsResults.setCellData("Results", "Property_State Result", filledRows, "Fail");
				}

			} catch (java.lang.ArrayIndexOutOfBoundsException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

		}

		xlsResults.setCellData("Results", "Actual Occupancy", filledRows, getElement("OccupancyLOM_xpath").getText());
		xlsResults.setCellData("Results", "Expected Occupancy", filledRows, data.get("Occupancy"));
		xlsResults.setCellData("Results", "Actual Property_Type", filledRows,
				getElement("PropertyTypeLOM_xpath").getText());
		xlsResults.setCellData("Results", "Actual Loan_Product", filledRows,
				getElement("LoanProductLOM_xpath").getText());
		xlsResults.setCellData("Results", "Actual Loan_Program", filledRows,
				getElement("LoanProgramLOM_xpath").getText());
		xlsResults.setCellData("Results", "Actual Loan_Purpose", filledRows,
				getElement("LoanPurposeLOM_xpath").getText());
		xlsResults.setCellData("Results", "Actual Purchase_Price", filledRows,
				getElement("PurchasePriceLOM_xpath").getText());
		xlsResults.setCellData("Results", "Actual Total_Loan_Amt", filledRows,
				getElement("LoanAmountrLOM_xpath").getText());

		xlsResults.setCellData("Results", "Expected Property_Type", filledRows, data.get("Property_Type"));
		xlsResults.setCellData("Results", "Expected Loan_Product", filledRows, data.get("Loan_Product"));
		xlsResults.setCellData("Results", "Expected Loan_Program", filledRows, data.get("Loan_Program"));
		xlsResults.setCellData("Results", "Expected Loan_Purpose", filledRows, data.get("Loan_Purpose"));
		xlsResults.setCellData("Results", "Expected Purchase_Price", filledRows, data.get("Purchase_Price"));
		xlsResults.setCellData("Results", "Expected Total_Loan_Amt", filledRows, data.get("Total_Loan_Amt"));

		try {
			String actual = getElement("OccupancyLOM_xpath").getText();

			if ((actual.equals("-")
					&& (data.get("Occupancy").equals("") || data.get("Occupancy").equalsIgnoreCase("Null")))
					|| actual.equals(data.get("Occupancy"))) {
				xlsResults.setCellData("Results", "Occupancy Result", filledRows, "Pass");
			}

			if (actual.startsWith(data.get("Occupancy"))) {
				xlsResults.setCellData("Results", "Occupancy Result", filledRows, "Pass");
			} else {
				xlsResults.setCellData("Results", "Occupancy Result", filledRows, "Fail");
			}

		} catch (AssertionError e) {
		}

		try {
			String actual1 = getElement("PropertyTypeLOM_xpath").getText();
			if ((actual1.equals("-")
					&& (data.get("Property_Type").equals("") || data.get("Property_Type").equalsIgnoreCase("Null")))
					|| actual1.equals(data.get("Property_Type"))) {
				xlsResults.setCellData("Results", "Property_Type Result", filledRows, "Pass");
			} else if (actual1.equalsIgnoreCase(data.get("Property_Type"))) {
				xlsResults.setCellData("Results", "Property_Type Result", filledRows, "Pass");
			} else {
				xlsResults.setCellData("Results", "Property_Type Result", filledRows, "Fail");
			}
		} catch (AssertionError e) {
		}

		try {
			String actual1 = getElement("LoanProductLOM_xpath").getText();

			if ((actual1.equals("-")
					&& (data.get("Loan_Product").equals("") || data.get("Loan_Product").equalsIgnoreCase("Null")))
					|| actual1.equals(data.get("Loan_Product"))) {
				xlsResults.setCellData("Results", "Loan_Product Result", filledRows, "Pass");
			} else if (actual1.equals("-") && !data.get("Loan_Product").equals("")) {
				xlsResults.setCellData("Results", "Loan_Product Result", filledRows, "Fail");
			}

			else if (ht.get(actual1).equals(data.get("Loan_Product"))) {

				xlsResults.setCellData("Results", "Loan_Product Result", filledRows, "Pass");
			} else if (ht.get(data.get("Loan_Product")).equals(actual1)) {
				xlsResults.setCellData("Results", "Loan_Product Result", filledRows, "Pass");
			} else {
				xlsResults.setCellData("Results", "Loan_Product Result", filledRows, "Fail");
			}

		} catch (

		AssertionError e) {
		}

		{
			String actual = getElement("LoanProgramLOM_xpath").getText();
			if ((actual.equals("-")
					&& (data.get("Loan_Program").equals("") || data.get("Loan_Program").equalsIgnoreCase("Null")))
					|| actual.equals(data.get("Loan_Program"))) {
				xlsResults.setCellData("Results", "Loan_Program Result", filledRows, "Pass");
			} else if (actual.equals("-") && !data.get("Loan_Program").equals("")) {
				xlsResults.setCellData("Results", "Loan_Program Result", filledRows, "Fail");
			}

			else if (ht.get(actual).equals(data.get("Loan_Program"))) {
				xlsResults.setCellData("Results", "Loan_Program Result", filledRows, "Pass");
			} else if (ht.get(data.get("Loan_Program")).equals(actual)) {
				xlsResults.setCellData("Results", "Loan_Program Result", filledRows, "Pass");
			} else {
				xlsResults.setCellData("Results", "Loan_Program Result", filledRows, "Fail");
			}
		}

		try {

			String actual = getElement("LoanPurposeLOM_xpath").getText();
			if (actual.startsWith(data.get("Loan_Purpose"))) {
				xlsResults.setCellData("Results", "Loan_Purpose Result", filledRows, "Pass");
			} else if (ht.get(actual).equals(data.get("Loan_Purpose"))) {
				xlsResults.setCellData("Results", "Loan_Purpose Result", filledRows, "Pass");
			} else if (ht.get(data.get("Loan_Purpose")).equals(actual)) {
				xlsResults.setCellData("Results", "Loan_Purpose Result", filledRows, "Pass");
			} else {
				xlsResults.setCellData("Results", "Loan_Purpose Result", filledRows, "Fail");
			}
		} catch (AssertionError e) {
		}

		try {
			String actual = getElement("PurchasePriceLOM_xpath").getText();
			String putchaprice = currecytoInt2(actual);
			System.out.println(actual + "    " + putchaprice);
			System.out.print(actual.equals("$0.00") && data.get("Purchase_Price").equals(""));
			System.out.print(actual.equals("$0.00") + "    ");
			System.out.print(data.get("Purchase_Price").equals("") + "    ");

			if (actual.equals("$0.00") && data.get("Purchase_Price").equals("")) {
				xlsResults.setCellData("Results", "Purchase_Price Result", filledRows, "Pass");
			} else if (actual.equals("-")) {
				xlsResults.setCellData("Results", "Purchase_Price Result", filledRows, "Fail");
			} else if (putchaprice.equals("0") && data.get("Purchase_Price").equals("")) {
				xlsResults.setCellData("Results", "Purchase_Price Result", filledRows, "Pass");
			} else if (putchaprice.equals(data.get("Purchase_Price"))) {
				xlsResults.setCellData("Results", "Purchase_Price Result", filledRows, "Pass");
			} else {
				xlsResults.setCellData("Results", "Purchase_Price Result", filledRows, "Fail");
			}

		} catch (AssertionError e) {
		}
		try {
			String actual = getElement("LoanAmountrLOM_xpath").getText();
			String putchaprice = currecytoInt2(getElement("LoanAmountrLOM_xpath").getText());

			System.out.println(actual + "    " + putchaprice);
			System.out.print(actual.equals("$0.00") && data.get("Total_Loan_Amt").equals(""));
			System.out.print(actual.equals("$0.00") + "    ");
			System.out.print(data.get("Total_Loan_Amt").equals("") + "    ");

			if (getElement("LoanAmountrLOM_xpath").getText().equals("$0.00") && data.get("Total_Loan_Amt").equals("")) {
				xlsResults.setCellData("Results", "Total_Loan_Amt Result", filledRows, "Pass");
			} else if (putchaprice.equals("0") && data.get("Total_Loan_Amt").equals("")) {
				xlsResults.setCellData("Results", "Total_Loan_Amt Result", filledRows, "Pass");
			} else if (putchaprice.equals(data.get("Total_Loan_Amt"))) {
				xlsResults.setCellData("Results", "Total_Loan_Amt Result", filledRows, "Pass");
			} else {
				xlsResults.setCellData("Results", "Total_Loan_Amt Result", filledRows, "Fail");

			}

			Assert.assertEquals((putchaprice), data.get("Total_Loan_Amt"));
			xlsResults.setCellData("Results", "Total_Loan_Amt Result", filledRows, "Pass");
		} catch (AssertionError e) {
			xlsResults.setCellData("Results", "Total_Loan_Amt Result", filledRows, "Fail");
		}

		String[] LTV_CLTV = getElement("LTV_CLTV_LOM_xpath").getText().split("/");
		xlsResults.setCellData("Results", "Actual LTV", filledRows, LTV_CLTV[0]);
		xlsResults.setCellData("Results", "Actual Combined_LTV", filledRows, LTV_CLTV[1]);
		xlsResults.setCellData("Results", "Actual Interest_Rate", filledRows,

				getElement("LoanIntrestLOM_xpath").getText());
		xlsResults.setCellData("Results", "Actual Status_410_In_Processing",
				DataUtil.getFilledRows(xlsResults, "Results") - 1, getElement("StatusLOM_xpath").getText());

		xlsResults.setCellData("Results", "Expected LTV", filledRows, data.get("LTV"));
		xlsResults.setCellData("Results", "Expected Combined_LTV", filledRows, data.get("Combined_LTV"));
		xlsResults.setCellData("Results", "Expected Interest_Rate", filledRows, data.get("Interest_Rate"));
		xlsResults.setCellData("Results", "Expected Status_410_In_Processing", filledRows,
				data.get("Status_410_In_Processing"));

		try {
			String actual = LTV_CLTV[0];
			if ((actual.equals("-") && data.get("LTV").equals("")) || actual.startsWith(data.get("LTV"))) {
				xlsResults.setCellData("Results", "LTV Result", filledRows, "Pass");
			} else {
				// NumberFormat defaultFormat =
				// NumberFormat.getPercentInstance(Locale.US);
				// int percnt = defaultFormat.parse(actual).intValue();
				// Assert.assertEquals(Integer.toString(percnt),
				// data.get("LTV"));
				xlsResults.setCellData("Results", "LTV Result", filledRows, "Pass");
			}
		} catch (AssertionError e) {
			xlsResults.setCellData("Results", "LTV Result", filledRows, "Fail");
		}
		try {
			String actual = LTV_CLTV[1].trim();
			if ((actual.equals("-") && data.get("Combined_LTV").equals(""))
					|| actual.startsWith(data.get("Combined_LTV"))) {
				xlsResults.setCellData("Results", "Combined_LTV Result", filledRows, "Pass");
			} else {
				// NumberFormat defaultFormat =
				// NumberFormat.getPercentInstance(Locale.US);
				// int percnt = defaultFormat.parse(actual).intValue();
				// Assert.assertEquals(Integer.toString(percnt),
				// data.get("Combined_LTV"));
				xlsResults.setCellData("Results", "Combined_LTV Result", filledRows, "Pass");
			}
		} catch (AssertionError e) {
			xlsResults.setCellData("Results", "Combined_LTV Result", filledRows, "Fail");
		}

		try {

			// changing string to currency to int to string again for comparing
			// with excel data
			String actual = getElement("LoanIntrestLOM_xpath").getText();
			if ((actual.equals("-") && data.get("Loan_Program").equals(""))
					|| actual.startsWith(data.get("Loan_Program"))) {
				xlsResults.setCellData("Results", "Interest_Rate Result", filledRows, "Pass");
			} else {

				// NumberFormat defaultFormat =
				// NumberFormat.getPercentInstance(Locale.US);
				// int percnt = defaultFormat.parse(actual).intValue();
				// Assert.assertEquals(Integer.toString(percnt),
				// data.get("Interest_Rate"));
				xlsResults.setCellData("Results", "Interest_Rate Result", filledRows, "Pass");
			}
		} catch (AssertionError e) {
			xlsResults.setCellData("Results", "Interest_Rate Result", filledRows, "Fail");
		}
		try {
			Assert.assertEquals(getElement("StatusLOM_xpath").getText(), data.get("Status_410_In_Processing"));
			xlsResults.setCellData("Results", "Status_410_In_Processing Result", filledRows, "Pass");
		} catch (AssertionError e) {
			xlsResults.setCellData("Results", "Status_410_In_Processing Result", filledRows, "Fail");
		}

		// click("pricingPageLOM_id");
		// // pricing
		// xlsResults.setCellData("Results", "Actual Status_130_Prospect",
		// DataUtil.getFilledRows(xlsResults, "Results") - 1,
		// getElement("RateLockDateLOM_xpath").getText());
		// // in pricing
		//
		// xlsResults.setCellData("Results", "Expected Status_130_Prospect",
		// filledRows, data.get("Status_130_Prospect"));
		//
		// String actual = getElement("RateLockDateLOM_xpath").getText();
		// String expected = data.get("Status_130_Prospect");
		// SimpleDateFormat sdf[] = new SimpleDateFormat[] { new
		// SimpleDateFormat("MM/dd/yyyy HH:mm a"),
		// new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss") };
		// Date dateStart = sdf[0].parse(actual);
		// Date dateEnd = sdf[1].parse(expected);
		// if (dateStart.equals(dateEnd)) {
		// xlsResults.setCellData("Results", "Status_130_Prospect Result",
		// filledRows, "Pass");
		//
		// } else {
		// xlsResults.setCellData("Results", "Status_130_Prospect Result",
		// filledRows, "Fail");
		// }

	}

	// Reading the data from inputfile
	@DataProvider
	public Object[][] getData() {

		xls = new Xls_Reader(System.getProperty("user.dir") + prop.getProperty("lompipeline_xls_path"));
		return DataUtil.getTestData(xls, testName);
	}

	@Override
	@BeforeTest
	public void initReports() {
		initCodes();
		super.init();
		initXlsResult(testName);
		String sheetName = "Results";// name of sheet
		int i = 0;
		xlsResults.setCellData(sheetName, i, 1, "Test Case Name");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "loUserName");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Actual LoanNumberLOM");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Expected LoanNumberLOM");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "LoanNumberLOM Result");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Actual Borr1_First_Name");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Expected Borr1_First_Name");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Borr1_First_Name Result");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Actual Borr1_Last_Name");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Expected Borr1_Last_Name");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Borr1_Last_Name Result");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Actual Borr1_Total_Income");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Expected Borr1_Total_Income");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Borr1_Total_Income Result");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Actual Borr2_First_Name");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Expected Borr2_First_Name");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Borr2_First_Name Result");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Actual Borr2_Last_Name");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Expected Borr2_Last_Name");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Borr2_Last_Name Result");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Actual Borr2_Total_Income");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Expected Borr2_Total_Income");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Borr2_Total_Income Result");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Actual Borr3_First_Name");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Expected Borr3_First_Name");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Borr3_First_Name Result");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Actual Borr3_Last_Name");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Expected Borr3_Last_Name");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Borr3_Last_Name Result");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Actual Borr3_Total_Income");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Expected Borr3_Total_Income");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Borr3_Total_Income Result");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Actual Borr4_First_Name");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Expected Borr4_First_Name");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Borr4_First_Name Result");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Actual Borr4_Last_Name");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Expected Borr4_Last_Name");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Borr4_Last_Name Result");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Actual Borr4_Total_Income");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Expected Borr4_Total_Income");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Borr4_Total_Income Result");
		i++;
		// xlsResults.setCellData(sheetName, i, 1, "Actual
		// Status_130_Prospect");
		// i++;
		// xlsResults.setCellData(sheetName, i, 1, "Expected
		// Status_130_Prospect");
		// i++;
		// xlsResults.setCellData(sheetName, i, 1, "Status_130_Prospect
		// Result");
		// i++;
		xlsResults.setCellData(sheetName, i, 1, "Actual Property_Address");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Expected Property_Address");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Property_Address Result");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Actual Property_Zip");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Expected Property_Zip");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Property_Zip Result");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Actual Property_City");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Expected Property_City");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Property_City Result");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Actual Property_State");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Expected Property_State");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Property_State Result");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Actual Occupancy");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Expected Occupancy");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Occupancy Result");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Actual Property_Type");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Expected Property_Type");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Property_Type Result");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Actual Loan_Product");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Expected Loan_Product");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Loan_Product Result");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Actual Loan_Program");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Expected Loan_Program");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Loan_Program Result");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Actual Loan_Purpose");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Expected Loan_Purpose");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Loan_Purpose Result");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Actual Purchase_Price");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Expected Purchase_Price");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Purchase_Price Result");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Actual Total_Loan_Amt");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Expected Total_Loan_Amt");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Total_Loan_Amt Result");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Actual Combined_LTV");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Expected Combined_LTV");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Combined_LTV Result");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Actual LTV");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Expected LTV");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "LTV Result");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Actual Interest_Rate");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Expected Interest_Rate");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Interest_Rate Result");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Actual Status_410_In_Processing");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Expected Status_410_In_Processing");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Status_410_In_Processing Result");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Actual loUserName");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Expected  loUserName ");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "loUserName Result");
		i++;
		// xlsResults.setCellData(sheetName, i, 1, "Actual
		// Loan_Officer_UserID");
		// i++;
		// xlsResults.setCellData(sheetName, i, 1, "Expected
		// Loan_Officer_UserID");
		// i++;
		// xlsResults.setCellData(sheetName, i, 1, "Loan_Officer_UserID
		// Result");
		// i++;
		// xlsResults.setCellData(sheetName, i, 1, "Actual Conditions__Title");
		// i++;
		// xlsResults.setCellData(sheetName, i, 1, "Expected
		// Conditions__Title");
		// i++;
		// xlsResults.setCellData(sheetName, i, 1, "Conditions__Title Result");
		// i++;
		// xlsResults.setCellData(sheetName, i, 1, "Actual
		// Conditions__Description");
		// i++;
		// xlsResults.setCellData(sheetName, i, 1, "Expected
		// Conditions__Description");
		// i++;
		// xlsResults.setCellData(sheetName, i, 1, "Conditions__Description
		// Result");

	}

	@Override
	@AfterMethod
	public void quit(ITestResult result) {
		if (result.getStatus() == ITestResult.FAILURE) {
			// Checking for the test if the test is failed /pass/skipped etc..
			// for reporting.
			reportFailure(result.getThrowable());

		} else if (result.getStatus() == ITestResult.SKIP) {
			test.log(LogStatus.SKIP, "Test skipped " + result.getThrowable());
		} else {
			test.log(LogStatus.PASS, "Test passed");
			xlsResults.setCellData("Results", "Test Result LOM", DataUtil.getFilledRows(xlsResults, "Results") - 1,
					"Pass");
		}
		if (rep != null) {
			rep.endTest(test);
			rep.flush();
		}
		// quit

		if (driver != null) {
			driver.quit();
		}
	}
}
