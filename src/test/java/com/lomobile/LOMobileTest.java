
package com.lomobile;

import java.text.ParseException;
import java.util.Hashtable;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.homebridge.retail.base.BaseTest;
import com.homebridge.retail.util.DataUtil;
import com.homebridge.retail.util.Xls_Reader;
import com.relevantcodes.extentreports.LogStatus;

public class LOMobileTest extends BaseTest {
	String testName = "LOMobileTest";
	Xls_Reader xls;

	@Test(dataProvider = "getData")
	public void SubmitProfile(Hashtable<String, String> data) throws ParseException, InterruptedException {
		/**
		 * reporting
		 */
		test = rep.startTest(data.get("TestCase"));
		xlsResults.setCellData("Results", "Test Case Name", DataUtil.getFilledRows(xlsResults, "Results"),
				data.get("TestCase"));
		// xlsResults.setCellData("Results", "Test Result LOM",
		// DataUtil.getFilledRows(xlsResults, "Results") - 1, "Fail");
		/**
		 * Generating the log in the report
		 */
		test.log(LogStatus.INFO, data.toString());

		/**
		 * Validating the Run mode N or Y if N test will skip
		 */
		if (DataUtil.isSkip(xls, testName) || data.get("Runmode").equals("N")) {
			test.log(LogStatus.SKIP, "Skipping the test as runmode is N");
			throw new SkipException("Skipping the test as runmode is N");
		}
		test.log(LogStatus.INFO, "Starting " + testName);

		openBrowser(data.get("Browser"));
		navigate("URL_lompROD");
		/**
		 * signing in
		 */
		Thread.sleep(2000);
		type("loUserName_xpath", data.get("loUserName"));
		type("loPassword_xpath", data.get("loPassword"));
		click("losubmit_xpath");
		Thread.sleep(2000);

		click("menuLo_id");
		Thread.sleep(3000);
		wait("myLoansMenuLo_xpath", 5);
		click("myLoansMenuLo_xpath");
		Thread.sleep(1000);
		// wait("searchTabLo_xpath", 5);
		// click("searchTabLo_xpath");
		// Thread.sleep(1000);

		/**
		 * Searching the loan number
		 */
		type("searchTextbox_xpath", data.get("Name"));
		Thread.sleep(1000);
		click_actions("searchedLoanNumber_xpath");
		Thread.sleep(5000);
		wait("getLoanNumberSelectedLoan_xpath", 5);
		int rowsFilled = DataUtil.getFilledRows(xlsResults, "Results") - 1;
		xlsResults.setCellData("Results", "LoanNumberLOM", rowsFilled, getElement("getloanNumber_xpath").getText());

		xlsResults.setCellData("Results", "Actual LoanAmount", rowsFilled,
				getElement("LoanAmountrLOM_xpath").getText());
		xlsResults.setCellData("Results", "Actual LoanIntrest", rowsFilled,
				getElement("LoanIntrestLOM_xpath").getText());
		xlsResults.setCellData("Results", "Actual Occupancy", rowsFilled, getElement("OccupancyLOM_xpath").getText());
		xlsResults.setCellData("Results", "PurchasePrice", rowsFilled, getElement("PurchasePriceLOM_xpath").getText());
		xlsResults.setCellData("Results", "Actual FullName", rowsFilled, getElement("NameLOM_xpath").getText());
		xlsResults.setCellData("Results", "Actual Employer", rowsFilled, getElement("EmployerLOM_xpath").getText());
		xlsResults.setCellData("Results", "Actual Contact", rowsFilled, getElement("ContactLOM_xpath").getText());
		xlsResults.setCellData("Results", "Actual Income", rowsFilled, getElement("borr1IncomeLOM_xpath").getText());
		xlsResults.setCellData("Results", "Actual Assets", rowsFilled, getElement("AssetsLOM_xpath").getText());
		xlsResults.setCellData("Results", "Actual Liabilities", rowsFilled,
				getElement("LiabilitiesLOM_xpath").getText());

		xlsResults.setCellData("Results", "Expected LoanAmount", rowsFilled, data.get("LoanAmount"));
		xlsResults.setCellData("Results", "Expected LoanIntrest", rowsFilled, data.get("LoanIntrest"));
		xlsResults.setCellData("Results", "Expected Occupancy", rowsFilled, data.get("Occupancy"));
		xlsResults.setCellData("Results", "PurchasePrice", rowsFilled, data.get("PurchasePrice"));
		xlsResults.setCellData("Results", "Expected FullName", rowsFilled, data.get("FullName"));
		xlsResults.setCellData("Results", "Expected Employer", rowsFilled, data.get("Employer"));
		xlsResults.setCellData("Results", "Expected Contact", rowsFilled, data.get("Contact"));
		xlsResults.setCellData("Results", "Expected Income", rowsFilled, data.get("Income"));
		xlsResults.setCellData("Results", "Expected Assets", rowsFilled, data.get("Assets"));
		xlsResults.setCellData("Results", "Expected Liabilities", rowsFilled, data.get("Liabilities"));

		// validating and reporting in excel
		try {
			Assert.assertEquals(getElement("LoanAmountrLOM_xpath").getText(), data.get("LoanAmount"));
			xlsResults.setCellData("Results", "LoanAmount Result", rowsFilled, "Pass");
		} catch (AssertionError e) {
			xlsResults.setCellData("Results", "LoanAmount Result", rowsFilled, "Fail");
		}

		try {

			Assert.assertEquals(getElement("LoanIntrestLOM_xpath").getText(), data.get("LoanIntrest"));
			xlsResults.setCellData("Results", "LoanIntrest Result", rowsFilled, "Pass");
		} catch (AssertionError e) {
			xlsResults.setCellData("Results", "LoanIntrest Result", rowsFilled, "Fail");
		}

		try {
			Assert.assertEquals(getElement("OccupancyLOM_xpath").getText(), data.get("Occupancy"));
			xlsResults.setCellData("Results", "Occupancy Result", rowsFilled, "Pass");
		} catch (AssertionError e) {
			xlsResults.setCellData("Results", "Occupancy Result", rowsFilled, "Fail");
		}

		try {

			Assert.assertEquals(getElement("PurchasePriceLOM_xpath").getText(), data.get("PurchasePrice"));
			xlsResults.setCellData("Results", "PurchasePrice Result", rowsFilled, "Pass");
		} catch (AssertionError e) {
			xlsResults.setCellData("Results", "PurchasePrice Result", rowsFilled, "Fail");
		}

		try {

			Assert.assertEquals(getElement("NameLOM_xpath").getText(), data.get("FullName"));
			xlsResults.setCellData("Results", "FullName Result", rowsFilled, "Pass");
		} catch (AssertionError e) {
			xlsResults.setCellData("Results", "FullName Result", rowsFilled, "Fail");
		}

		try {

			Assert.assertEquals(getElement("EmployerLOM_xpath").getText(), data.get("Employer"));
			xlsResults.setCellData("Results", "Employer Result", rowsFilled, "Pass");
		} catch (AssertionError e) {
			xlsResults.setCellData("Results", "Employer Result", rowsFilled, "Fail");
		}

		try {
			Assert.assertEquals(getElement("ContactLOM_xpath").getText(), data.get("Contact"));
			xlsResults.setCellData("Results", "Contact Result", rowsFilled, "Pass");
		} catch (AssertionError e) {
			xlsResults.setCellData("Results", "Contact Result", rowsFilled, "Fail");
		}

		try {

			Assert.assertEquals(getElement("borr1IncomeLOM_xpath").getText(), data.get("Income"));
			xlsResults.setCellData("Results", "Income Result", rowsFilled, "Pass");
		} catch (AssertionError e) {
			xlsResults.setCellData("Results", "Income Result", rowsFilled, "Fail");
		}

		try {

			Assert.assertEquals(getElement("AssetsLOM_xpath").getText(), data.get("Assets"));
			xlsResults.setCellData("Results", "Assets Result", rowsFilled, "Pass");
		} catch (AssertionError e) {
			xlsResults.setCellData("Results", "Assets Result", rowsFilled, "Fail");
		}

		try {
			Assert.assertEquals(getElement("LiabilitiesLOM_xpath").getText(), data.get("Liabilities"));
			xlsResults.setCellData("Results", "Liabilities Result", rowsFilled, "Pass");
		} catch (AssertionError e) {
			xlsResults.setCellData("Results", "Liabilities Result", rowsFilled, "Fail");
		}

		click("creditPageLOM_id");
		wait("ordrCreditLOM_xpath", 10);
		click("ordrCreditLOM_xpath");
		Thread.sleep(3000);
		// wait("checkBoxNameLOM_xpath", 10);

		click_actions("checkBoxNameLOM_xpath");
		wait("submitOrderLOM_xpath", 10);

		click("submitOrderLOM_xpath");
	}

	// Reading the data from inputfile
	@DataProvider()
	public Object[][] getData() {
		super.init();
		xls = new Xls_Reader(System.getProperty("user.dir") + prop.getProperty("lom_xlspath"));

		return DataUtil.getTestData(xls, testName);
	}

	@Override
	@BeforeTest
	public void initReports() {

		initXlsResult(testName);

		String sheetName = "Results";// name of sheet

		int i = 0;
		xlsResults.setCellData(sheetName, i, 1, "Test Case Name");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "LoanNumberLOM");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Expected LoanAmount");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Actual LoanAmount");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "LoanAmount Result");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Expected LoanIntrest");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Actual LoanIntrest");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "LoanIntrest Result");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Expected Occupancy");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Actual Occupancy");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Occupancy Result");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Expected PurchasePrice");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Actual PurchasePrice");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "PurchasePrice Result");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Expected FullName");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Actual FullName");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "FullName Result");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Expected Employer");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Actual Employer");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Employer Result");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Expected Contact");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Actual Contact");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Contact Result");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Expected Income");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Actual Income");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Income Result");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Expected Assets");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Actual Assets");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Assets Result");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Expected Liabilities");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Actual Liabilities");
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Liabilities Result");
		// i++;
		// xlsResults.setCellData(sheetName, i, 1, "Test Result LOM");
	}

	@Override
	@AfterMethod
	public void quit(ITestResult result) {
		if (result.getStatus() == ITestResult.FAILURE) {
			// Checking for the test if the test is failed /pass/skipped etc..
			// for reporting.
			reportFailure(result.getThrowable());

		} else if (result.getStatus() == ITestResult.SKIP) {
			test.log(LogStatus.SKIP, "Test skipped " + result.getThrowable());
		} else {
			test.log(LogStatus.PASS, "Test passed");
			xlsResults.setCellData("Results", "Test Result LOM", DataUtil.getFilledRows(xlsResults, "Results") - 1,
					"Pass");
		}
		if (rep != null) {
			rep.endTest(test);
			rep.flush();
		}
		// quit

		if (driver != null) {
			driver.quit();
		}
	}
}
