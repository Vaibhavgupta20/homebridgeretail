
package com.lopPipeline;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Hashtable;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.testng.ITestResult;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.homebridge.retail.base.BaseTest;
import com.homebridge.retail.util.DataUtil;
import com.homebridge.retail.util.Xls_Reader;
import com.relevantcodes.extentreports.LogStatus;

public class LOPPipelineTest extends BaseTest {
	String testName = "LOPPipleineTest";
	Xls_Reader xls;

	@Test(dataProvider = "getData")
	public void SubmitProfile(Hashtable<String, String> data) throws ParseException, InterruptedException {
		/**
		 * reporting excel and extent
		 */
		test = rep.startTest(data.get("TestCase"));

		/**
		 * Validating the Run mode N or Y if N test will skip
		 */
		if (DataUtil.isSkip(xls, testName) || data.get("Runmode").equals("N")) {
			test.log(LogStatus.SKIP, "Skipping the test as runmode is N");
			throw new SkipException("Skipping the test as runmode is N");
		}
		xlsResults.setCellData("Results", "Test Case Name", DataUtil.getFilledRows(xlsResults, "Results"),
				data.get("TestCase"));
		int filledRows = DataUtil.getFilledRows(xlsResults, "Results") - 1;
		xlsResults.setCellData("Results", "Loan_Number", filledRows, data.get("Loan_Number"));

		xlsResults.setCellData("Results", "Test Result LOM", DataUtil.getFilledRows(xlsResults, "Results") - 1, "Fail");
		/**
		 * Generating the log in the report
		 */
		test.log(LogStatus.INFO, data.toString());

		openBrowser(data.get("Browser"));
		navigate("URL_loppROD");
		/**
		 * signing in
		 */
		Thread.sleep(2000);
		type("lopUserName_xpath", data.get("loUserName"));
		type("lopPassword_xpath", data.get("loPassword"));
		click("lopsubmit_xpath");
		wait("loanPage_lop_xpath", 10);
		click("loanPage_lop_xpath");

		/**
		 * Searching the loan number
		 */
		wait("loanTxtbox_lop_xpath", 5);
		System.out.println(data.get("Loan_Number"));
		type("loanTxtbox_lop_xpath", data.get("Loan_Number"));
		Thread.sleep(500);
		getElement("loanTxtbox_lop_xpath").sendKeys(Keys.RETURN);
		Thread.sleep(2000);
		wait(10, "searchedLoanNumber_lop", data.get("Loan_Number"));
		click("searchedLoanNumber_lop", data.get("Loan_Number"));
		Thread.sleep(5000);
		if (isElementPresent("summary_lop_xpath")) {
			try {
				driver.findElements(By.xpath(getProperty("searchedLoanNumber_lop", data.get("Loan_Number")))).get(1)
						.click();
			} catch (Exception e1) {
			}

		}

		wait("summary_lop_xpath", 5);
		click("summary_lop_xpath");
		wait("loanPurpose_lop_name", 5);
		verifyValueText("loanProduct_lop", data.get("Loan_Product"), filledRows, "Loan_Product");
		verifyValueText("loanProgram_lop", data.get("Loan_Program"), filledRows, "Loan_Program");
		verifyValueText("loanPurpose_lop", data.get("Loan_Purpose"), filledRows, "Loan_Purpose");
		
		//check row 3 of data
		verifyValueText("purchasePrice_lop", data.get("Purchase_Price"), filledRows, "Purchase_Price");
		verifyValueText("appraisedValue_lop", data.get("Appraised_Value"), filledRows, "Appraised_Value");
		verifyValueText("baseLoanAmount_lop", data.get("Base_Loan_Amt"), filledRows, "Base_Loan_Amt");
		verifyValueText("totalLoanAmt_lop", data.get("Total_Loan_Amt"), filledRows, "Total_Loan_Amt");
		verifyValueText("LTV_lop", data.get("LTV"), filledRows, "LTV");
		verifyValueText("CLTV_lop", data.get("Combined_LTV"), filledRows, "Combined_LTV");
		verifyValueText("HCLTV_lop", data.get("HCLTV"), filledRows, "HCLTV");
		verifyValueText("loanType_lop", data.get("Mortgage_Type"), filledRows, "Mortgage_Type");
		verifyValueText("mortInsFinanced_lop", data.get("MI_Financed"), filledRows, "MI_Financed");
		verifyValueText("occupancyType_lop", data.get("Occupancy"), filledRows, "Occupancy");
		verifyValueText("interestRate_lop", data.get("Interest_Rate"), filledRows, "Interest_Rate");

		verifyText("borr1AppType_lop", data.get("Borr1_Applicant_Type"), filledRows, "Borr1_Applicant_Type");
		verifyText("borr1Name_lop", data.get("Borr1_First_Name") + " " + data.get("Borr1_Last_Name"), filledRows,
				"Borr1_First_Last_Name");
		verifyText("borr1Income_lop", data.get("Borr1_Total_Income"), filledRows, "Borr1_Total_Income");
		boolean borr2Avail = isElementPresent("borr2Name_lop_xpath");
		boolean borr3Avail = isElementPresent("borr3Name_lop_xpath");
		boolean borr4Avail = isElementPresent("borr4Name_lop_xpath");

		if (borr2Avail) {
			verifyText("borr2Income_lop", data.get("Borr2_Total_Income"), filledRows, "Borr2_Total_Income");
			verifyText("borr2AppType_lop", data.get("Borr2_Applicant_Type"), filledRows, "Borr2_Applicant_Type");

			verifyText("borr2Name_lop", data.get("Borr2_First_Name") + " " + data.get("Borr2_Last_Name"), filledRows,
					"Borr2_First_Last_Name");
		} else {
			putNA(filledRows, "Borr2_Applicant_Type");
			putNA(filledRows, "Borr2_First_Last_Name");

			putNA(filledRows, "Borr2_Total_Income");
		}

		if (borr3Avail) {
			verifyText("borr3Income_lop", data.get("Borr3_Total_Income"), filledRows, "Borr3_Total_Income");

			verifyText("borr3Name_lop", data.get("Borr3_First_Name") + " " + data.get("Borr3_Last_Name"), filledRows,
					"Borr3_First_Last_Name");
			verifyText("borr3AppType_lop", data.get("Borr3_Applicant_Type"), filledRows, "Borr3_Applicant_Type");
		} else {
			putNA(filledRows, "Borr3_Applicant_Type");
			putNA(filledRows, "Borr3_First_Last_Name");

			putNA(filledRows, "Borr3_Total_Income");
		}

		if (borr4Avail) {
			verifyText("borr4AppType_lop", data.get("Borr4_Applicant_Type"), filledRows, "Borr4_Applicant_Type");
			verifyText("borr4Name_lop", data.get("Borr4_First_Name") + " " + data.get("Borr4_Last_Name"), filledRows,
					"Borr4_First_Last_Name");

			verifyText("borr4Income_lop", data.get("Borr4_Total_Income"), filledRows, "Borr4_Total_Income");
		} else {
			putNA(filledRows, "Borr4_Applicant_Type");
			putNA(filledRows, "Borr4_First_Last_Name");

			putNA(filledRows, "Borr4_Total_Income");

		}

		click("1003_lop_xpath");

		// click("borrower_lop_xpath");

		wait("taxIdNumberBorr1_lop_name", 15);

		verifyValueText("taxIdNumberBorr1_lop", data.get("Borr1_SSN"), filledRows, "Borr1_SSN");
		verifyValueText("birthDateBorr1_lop", data.get("Borr1_DOB"), filledRows, "Borr1_DOB");
		verifyValueText("emailAddressBorr1_lop", data.get("Borr1_Email_Address"), filledRows, "Borr1_Email_Address");

		verifyValueText("addressLineOneBorr1_lop", data.get("Borr1_PresentAddressStreet"), filledRows,
				"Borr1_PresentAddressStreet");
		verifyValueText("cityBorr1_lop", data.get("Borr1_PresentAddressCity"), filledRows, "Borr1_PresentAddressCity");
		verifyValueText("stateIdBorr1_lop", data.get("Borr1_PresentAddressState"), filledRows,
				"Borr1_PresentAddressState");
		verifyValueText("postalCodeIdBorr1_lop", data.get("Borr1_PresentAddressZip"), filledRows,
				"Borr1_PresentAddressZip");

		// TODO: employee loan
		// System.out.println(getElement("employeeLenderBorr1_lop_xpath").isSelected());
		// System.out.println(getElement("employeeLenderBorr1_lop_xpath").isEnabled());

		xlsResults.setCellData("Results", "Actual " + "EmployeeLoan", filledRows,
				Boolean.toString(getElement("employeeLenderBorr1_lop_xpath").isSelected()));
		xlsResults.setCellData("Results", "Expected " + "EmployeeLoan", filledRows, data.get("EmployeeLoan"));

		if ((getElement("employeeLenderBorr1_lop_xpath").isSelected() && data.get("EmployeeLoan").equals("1"))
				|| (!getElement("employeeLenderBorr1_lop_xpath").isSelected()
						&& data.get("EmployeeLoan").equals("0"))) {
			xlsResults.setCellData("Results", "EmployeeLoan" + " Result", filledRows, "Pass");
		} else {
			xlsResults.setCellData("Results", "EmployeeLoan" + " Result", filledRows, "Fail");
		}

		verifyValueText("Borr1_Home_Phone_lop", data.get("Borr1_Home_Phone"), filledRows, "Borr1_Home_Phone");
		verifyValueText("Borr1_Cell_Phone_lop", data.get("Borr1_Cell_Phone"), filledRows, "Borr1_Cell_Phone");
		verifyValueText("Borr1_Work_Phone_lop", data.get("Borr1_Work_Phone"), filledRows, "Borr1_Work_Phone");

		if (borr2Avail) {
			verifyValueText("taxIdNumberBorr2_lop", data.get("Borr2_SSN"), filledRows, "Borr2_SSN");
			verifyValueText("birthDateBorr2_lop", data.get("Borr2_DOB"), filledRows, "Borr2_DOB");
			verifyValueText("emailAddressBorr2_lop", data.get("Borr2_Email_Address"), filledRows,
					"Borr2_Email_Address");
			verifyValueText("addressLineOneBorr2_lop", data.get("Borr2_PresentAddressStreet"), filledRows,
					"Borr2_PresentAddressStreet");
			verifyValueText("cityBorr2_lop", data.get("Borr2_PresentAddressCity"), filledRows,
					"Borr2_PresentAddressCity");
			verifyValueText("stateIdBorr2_lop", data.get("Borr2_PresentAddressState"), filledRows,
					"Borr2_PresentAddressState");
			verifyValueText("postalCodeIdBorr2_lop", data.get("Borr2_PresentAddressZip"), filledRows,
					"Borr2_PresentAddressZip");
			verifyValueText("Borr2_Home_Phone_lop", data.get("Borr2_Home_Phone"), filledRows, "Borr2_Home_Phone");
			verifyValueText("Borr2_Cell_Phone_lop", data.get("Borr2_Cell_Phone"), filledRows, "Borr2_Cell_Phone");
			verifyValueText("Borr2_Work_Phone_lop", data.get("Borr2_Work_Phone"), filledRows, "Borr2_Work_Phone");

		} else {
			putNA(filledRows, "Borr2_Home_Phone");
			putNA(filledRows, "Borr2_Cell_Phone");
			putNA(filledRows, "Borr2_Work_Phone");

			putNA(filledRows, "Borr2_SSN");
			putNA(filledRows, "Borr2_DOB");
			putNA(filledRows, "Borr2_Email_Address");

			putNA(filledRows, "Borr2_PresentAddressStreet");
			putNA(filledRows, "Borr2_PresentAddressCity");
			putNA(filledRows, "Borr2_PresentAddressState");
			putNA(filledRows, "Borr2_PresentAddressZip");
		}
		if (borr3Avail) {

			verifyValueText("taxIdNumberBorr3_lop", data.get("Borr3_SSN"), filledRows, "Borr3_SSN");
			verifyValueText("birthDateBorr3_lop", data.get("Borr3_DOB"), filledRows, "Borr3_DOB");
			verifyValueText("emailAddressBorr3_lop", data.get("Borr3_Email_Address"), filledRows,
					"Borr3_Email_Address");
			verifyValueText("addressLineOneBorr3_lop", data.get("Borr3_PresentAddressStreet"), filledRows,
					"Borr3_PresentAddressStreet");
			verifyValueText("cityBorr3_lop", data.get("Borr3_PresentAddressCity"), filledRows,
					"Borr3_PresentAddressCity");
			verifyValueText("stateIdBorr3_lop", data.get("Borr3_PresentAddressState"), filledRows,
					"Borr3_PresentAddressState");
			verifyValueText("postalCodeIdBorr3_lop", data.get("Borr3_PresentAddressZip"), filledRows,
					"Borr3_PresentAddressZip");

			verifyValueText("Borr3_Home_Phone_lop", data.get("Borr3_Home_Phone"), filledRows, "Borr3_Home_Phone");
			verifyValueText("Borr3_Cell_Phone_lop", data.get("Borr3_Cell_Phone"), filledRows, "Borr3_Cell_Phone");
			verifyValueText("Borr3_Work_Phone_lop", data.get("Borr3_Work_Phone"), filledRows, "Borr3_Work_Phone");

		} else {
			putNA(filledRows, "Borr3_Home_Phone");
			putNA(filledRows, "Borr3_Cell_Phone");
			putNA(filledRows, "Borr3_Work_Phone");
			putNA(filledRows, "Borr3_SSN");
			putNA(filledRows, "Borr3_DOB");
			putNA(filledRows, "Borr3_Email_Address");
			putNA(filledRows, "Borr3_PresentAddressStreet");
			putNA(filledRows, "Borr3_PresentAddressCity");
			putNA(filledRows, "Borr3_PresentAddressState");
			putNA(filledRows, "Borr3_PresentAddressZip");
		}
		if (borr4Avail) {
			verifyValueText("taxIdNumberBorr4_lop", data.get("Borr4_SSN"), filledRows, "Borr4_SSN");
			verifyValueText("birthDateBorr4_lop", data.get("Borr4_DOB"), filledRows, "Borr4_DOB");
			verifyValueText("emailAddressBorr4_lop", data.get("Borr4_Email_Address"), filledRows,
					"Borr4_Email_Address");

			verifyValueText("addressLineOneBorr4_lop", data.get("Borr4_PresentAddressStreet"), filledRows,
					"Borr4_PresentAddressStreet");
			verifyValueText("cityBorr4_lop", data.get("Borr4_PresentAddressCity"), filledRows,
					"Borr4_PresentAddressCity");
			verifyValueText("stateIdBorr4_lop", data.get("Borr4_PresentAddressState"), filledRows,
					"Borr4_PresentAddressState");
			verifyValueText("postalCodeIdBorr4_lop", data.get("Borr4_PresentAddressZip"), filledRows,
					"Borr4_PresentAddressZip");

			verifyValueText("Borr4_Home_Phone_lop", data.get("Borr4_Home_Phone"), filledRows, "Borr4_Home_Phone");
			verifyValueText("Borr4_Cell_Phone_lop", data.get("Borr4_Cell_Phone"), filledRows, "Borr4_Cell_Phone");
			verifyValueText("Borr4_Work_Phone_lop", data.get("Borr4_Work_Phone"), filledRows, "Borr4_Work_Phone");

		} else {
			putNA(filledRows, "Borr4_Home_Phone");
			putNA(filledRows, "Borr4_Cell_Phone");
			putNA(filledRows, "Borr4_Work_Phone");
			putNA(filledRows, "Borr4_SSN");
			putNA(filledRows, "Borr4_DOB");
			putNA(filledRows, "Borr4_Email_Address");
			putNA(filledRows, "Borr4_PresentAddressStreet");
			putNA(filledRows, "Borr4_PresentAddressCity");
			putNA(filledRows, "Borr4_PresentAddressState");
			putNA(filledRows, "Borr4_PresentAddressZip");
		}
		if (isElementPresent("mailingAddressBorr1_lop_xpath")) {
			verifyText("mailingAddressBorr1_lop",
					data.get("Borr1_MailingAddressStreet") + ", " + data.get("Borr1_MailingAddressCity") + ", "
							+ data.get("Borr1_MailingAddressState") + ", " + data.get("Borr1_MailingAddressZip"),
					filledRows, "Borr1_MailingAddressStreet");
		} else {
			putNA(filledRows, "Borr1_MailingAddressStreet");
		}

		if (isElementPresent("mailingAddressBorr2_lop_xpath")) {
			verifyText("mailingAddressBorr2_lop",
					data.get("Borr2_MailingAddressStreet") + ", " + data.get("Borr2_MailingAddressCity") + ", "
							+ data.get("Borr2_MailingAddressState") + ", " + data.get("Borr2_MailingAddressZip"),
					filledRows, "Borr2_MailingAddressStreet");
		} else {
			putNA(filledRows, "Borr2_MailingAddressStreet");
		}
		if (isElementPresent("mailingAddressBorr3_lop_xpath")) {
			verifyText("mailingAddressBorr3_lop",
					data.get("Borr3_MailingAddressStreet") + ", " + data.get("Borr3_MailingAddressCity") + ", "
							+ data.get("Borr3_MailingAddressState") + ", " + data.get("Borr3_MailingAddressZip"),
					filledRows, "Borr3_MailingAddressStreet");
		} else {
			putNA(filledRows, "Borr3_MailingAddressStreet");
		}
		if (isElementPresent("mailingAddressBorr4_lop_xpath")) {
			verifyText("mailingAddressBorr4_lop",
					data.get("Borr4_MailingAddressStreet") + ", " + data.get("Borr4_MailingAddressCity") + ", "
							+ data.get("Borr4_MailingAddressState") + ", " + data.get("Borr4_MailingAddressZip"),
					filledRows, "Borr4_MailingAddressStreet");
		} else {
			putNA(filledRows, "Borr4_MailingAddressStreet");
		}

		// 1003 employer
		// Borr1_SelfEmployed

		click("employment_lop_xpath");
		wait("Borr1_EmployerName_lop_xpath", 15);

		verifyValueText("Borr1_EmployerName_lop", data.get("Borr1_EmployerName"), filledRows, "Borr1_EmployerName");
		verifyValueText("Borr1_EmployerAddressStreet_lop", data.get("Borr1_EmployerAddressStreet"), filledRows,
				"Borr1_EmployerAddressStreet");
		verifyValueText("Borr1_EmployerAddressZip_lop", data.get("Borr1_EmployerAddressZip"), filledRows,
				"Borr1_EmployerAddressZip");
		verifyValueText("Borr1_EmployerAddressCity_lop", data.get("Borr1_EmployerAddressCity"), filledRows,
				"Borr1_EmployerAddressCity");
		verifyValueText("Borr1_EmployerAddressState_lop", data.get("Borr1_EmployerAddressState"), filledRows,
				"Borr1_EmployerAddressState");
		verifyValueText("Borr1_JobPosition_lop", data.get("Borr1_JobPosition"), filledRows, "Borr1_JobPosition");

		// TODO: can't find the checked button self employed
		// System.out.println(getElement("Borr1_SelfEmployed_yes_lop_xpath").isSelected());
		// System.out.println(getElement("Borr1_SelfEmployed_yes_lop_xpath").isEnabled());
		// System.out.println(getElement("Borr2_SelfEmployed_yes_lop_xpath").isSelected());
		// System.out.println(getElement("Borr2_SelfEmployed_yes_lop_xpath").isEnabled());
		// System.out.println(getElement("Borr3_SelfEmployed_yes_lop_xpath").isSelected());
		// System.out.println(getElement("Borr3_SelfEmployed_yes_lop_xpath").isEnabled());
		// System.out.println(getElement("Borr4_SelfEmployed_yes_lop_xpath").isSelected());
		// System.out.println(getElement("Borr4_SelfEmployed_yes_lop_xpath").isEnabled());
		//
		// System.out.println(
		// driver.findElement(By.xpath("(//*[@data-selenium-id=\"selfEmployed\"]//input)[1]")).isEnabled());
		// System.out.println(
		// driver.findElement(By.xpath("(//*[@data-selenium-id=\"selfEmployed\"]//input)[1]")).isSelected());
		// System.out.println(
		// driver.findElement(By.xpath("(//*[@data-selenium-id=\"selfEmployed\"]//input)[3]")).isEnabled());
		// System.out.println(
		// driver.findElement(By.xpath("(//*[@data-selenium-id=\"selfEmployed\"]//input)[3]")).isSelected());
		// System.out.println(
		// driver.findElement(By.xpath("(//*[@data-selenium-id=\"selfEmployed\"]//input)[5]")).isEnabled());
		// System.out.println(
		// driver.findElement(By.xpath("(//*[@data-selenium-id=\"selfEmployed\"]//input)[5]")).isSelected());
		// System.out.println(
		// driver.findElement(By.xpath("(//*[@data-selenium-id=\"selfEmployed\"]//input)[7]")).isEnabled());
		// System.out.println(
		// driver.findElement(By.xpath("(//*[@data-selenium-id=\"selfEmployed\"]//input)[7]")).isSelected());

		// String str =
		// getElement("Borr1_SelfEmployed_yes_lop_xpath").getAttribute("checked");
		// if (str.equalsIgnoreCase("true")) {
		// System.out.println("Checkbox selected");
		// }
		//
		// xlsResults.setCellData("Results", "Actual " + "Borr1_SelfEmployed",
		// filledRows,
		// Boolean.toString(getElement("Borr1_SelfEmployed_yes_lop_xpath").isSelected()));
		// xlsResults.setCellData("Results", "Expected " + "Borr1_SelfEmployed",
		// filledRows,
		// data.get("Borr1_SelfEmployed"));
		//
		// if (getElement("Borr1_SelfEmployed_yes_lop_xpath").isSelected() &&
		// data.get("Borr1_SelfEmployed").equals("1")) {
		// xlsResults.setCellData("Results", "Borr1_SelfEmployed" + " Result",
		// filledRows, "Pass");
		// } else {
		// xlsResults.setCellData("Results", "Borr1_SelfEmployed" + " Result",
		// filledRows, "Fail");
		// }

		if (borr2Avail) {
			verifyValueText("Borr2_EmployerName_lop", data.get("Borr2_EmployerName"), filledRows, "Borr2_EmployerName");
			verifyValueText("Borr2_EmployerAddressStreet_lop", data.get("Borr2_EmployerAddressStreet"), filledRows,
					"Borr2_EmployerAddressStreet");
			verifyValueText("Borr2_EmployerAddressZip_lop", data.get("Borr2_EmployerAddressZip"), filledRows,
					"Borr2_EmployerAddressZip");
			verifyValueText("Borr2_EmployerAddressCity_lop", data.get("Borr2_EmployerAddressCity"), filledRows,
					"Borr2_EmployerAddressCity");
			verifyValueText("Borr2_EmployerAddressState_lop", data.get("Borr2_EmployerAddressState"), filledRows,
					"Borr2_EmployerAddressState");
			verifyValueText("Borr2_JobPosition_lop", data.get("Borr2_JobPosition"), filledRows, "Borr2_JobPosition");

		} else {
			putNA(filledRows, "Borr2_EmployerName");
			putNA(filledRows, "Borr2_EmployerAddressStreet");
			putNA(filledRows, "Borr2_EmployerAddressZip");
			putNA(filledRows, "Borr2_EmployerAddressCity");
			putNA(filledRows, "Borr2_EmployerAddressState");
			putNA(filledRows, "Borr2_JobPosition");
		}
		if (borr3Avail) {

			verifyValueText("Borr3_EmployerName_lop", data.get("Borr3_EmployerName"), filledRows, "Borr3_EmployerName");
			verifyValueText("Borr3_EmployerAddressStreet_lop", data.get("Borr3_EmployerAddressStreet"), filledRows,
					"Borr3_EmployerAddressStreet");
			verifyValueText("Borr3_EmployerAddressZip_lop", data.get("Borr3_EmployerAddressZip"), filledRows,
					"Borr3_EmployerAddressZip");
			verifyValueText("Borr3_EmployerAddressCity_lop", data.get("Borr3_EmployerAddressCity"), filledRows,
					"Borr3_EmployerAddressCity");
			verifyValueText("Borr3_EmployerAddressState_lop", data.get("Borr3_EmployerAddressState"), filledRows,
					"Borr3_EmployerAddressState");
			verifyValueText("Borr3_JobPosition_lop", data.get("Borr3_JobPosition"), filledRows, "Borr3_JobPosition");

		} else {
			putNA(filledRows, "Borr3_EmployerName");
			putNA(filledRows, "Borr3_EmployerAddressStreet");
			putNA(filledRows, "Borr3_EmployerAddressZip");
			putNA(filledRows, "Borr3_EmployerAddressCity");
			putNA(filledRows, "Borr3_EmployerAddressState");
			putNA(filledRows, "Borr3_JobPosition");
		}
		if (borr4Avail) {
			verifyValueText("Borr4_EmployerName_lop", data.get("Borr4_EmployerName"), filledRows, "Borr4_EmployerName");
			verifyValueText("Borr4_EmployerAddressStreet_lop", data.get("Borr4_EmployerAddressStreet"), filledRows,
					"Borr4_EmployerAddressStreet");
			verifyValueText("Borr4_EmployerAddressZip_lop", data.get("Borr4_EmployerAddressZip"), filledRows,
					"Borr4_EmployerAddressZip");
			verifyValueText("Borr4_EmployerAddressCity_lop", data.get("Borr4_EmployerAddressCity"), filledRows,
					"Borr4_EmployerAddressCity");
			verifyValueText("Borr4_EmployerAddressState_lop", data.get("Borr4_EmployerAddressState"), filledRows,
					"Borr4_EmployerAddressState");
			verifyValueText("Borr4_JobPosition_lop", data.get("Borr4_JobPosition"), filledRows, "Borr4_JobPosition");

		} else {
			putNA(filledRows, "Borr4_EmployerName");
			putNA(filledRows, "Borr4_EmployerAddressStreet");
			putNA(filledRows, "Borr4_EmployerAddressZip");
			putNA(filledRows, "Borr4_EmployerAddressCity");
			putNA(filledRows, "Borr4_EmployerAddressState");
			putNA(filledRows, "Borr4_JobPosition");
		}

		// 1003 income
		click("incomePgae_lop_xpath");
		wait("baseEmploymentIncomeBorr1_lop_name", 15);

		verifyValueText("baseEmploymentIncomeBorr1_lop", data.get("Borr1_Base_Employment"), filledRows,
				"Borr1_Base_Employment");
		verifyValueText("overtimePayBorr1_lop", data.get("Borr1_Overtime"), filledRows, "Borr1_Overtime");
		verifyValueText("bonusesBorr1_lop", data.get("Borr1_Bonus"), filledRows, "Borr1_Bonus");
		verifyValueText("commissionsBorr1_lop", data.get("Borr1_Commission"), filledRows, "Borr1_Commission");
		verifyValueText("dividendsBorr1_lop", data.get("Borr1_Dividends_Interest"), filledRows,
				"Borr1_Dividends_Interest");
		verifyValueText("netRentalIncomeBorr1_lop", data.get("Borr1_Net_Rental_Income"), filledRows,
				"Borr1_Net_Rental_Income");
		verifyValueText("totalOtherIncomeBorr1_lop", data.get("Borr1_Total_Other_Income"), filledRows,
				"Borr1_Total_Other_Income");
		verifyValueText("totalIncomeBorr1_lop", data.get("Borr1_Total_Income"), filledRows, "Borr1_Total_Income");

		if (borr2Avail) {
			verifyValueText("baseEmploymentIncomeBorr2_lop", data.get("Borr2_Base_Employment"), filledRows,
					"Borr2_Base_Employment");
			verifyValueText("overtimePayBorr2_lop", data.get("Borr2_Overtime"), filledRows, "Borr2_Overtime");
			verifyValueText("bonusesBorr2_lop", data.get("Borr2_Bonus"), filledRows, "Borr2_Bonus");
			verifyValueText("commissionsBorr2_lop", data.get("Borr2_Commission"), filledRows, "Borr2_Commission");
			verifyValueText("dividendsBorr2_lop", data.get("Borr2_Dividends_Interest"), filledRows,
					"Borr2_Dividends_Interest");
			verifyValueText("netRentalIncomeBorr2_lop", data.get("Borr2_Net_Rental_Income"), filledRows,
					"Borr2_Net_Rental_Income");
			verifyValueText("totalOtherIncomeBorr2_lop", data.get("Borr2_Total_Other_Income"), filledRows,
					"Borr2_Total_Other_Income");
			verifyValueText("totalIncomeBorr2_lop", data.get("Borr2_Total_Income"), filledRows, "Borr2_Total_Income");
		} else {
			putNA(filledRows, "Borr2_Base_Employment");
			putNA(filledRows, "Borr2_Overtime");
			putNA(filledRows, "Borr2_Bonus");
			putNA(filledRows, "Borr2_Commission");
			putNA(filledRows, "Borr2_Dividends_Interest");
			putNA(filledRows, "Borr2_Net_Rental_Income");
			putNA(filledRows, "Borr2_Total_Other_Income");
			putNA(filledRows, "Borr2_Total_Income");

		}
		if (borr3Avail) {
			verifyValueText("baseEmploymentIncomeBorr3_lop", data.get("Borr3_Base_Employment"), filledRows,
					"Borr3_Base_Employment");
			verifyValueText("overtimePayBorr3_lop", data.get("Borr3_Overtime"), filledRows, "Borr3_Overtime");
			verifyValueText("bonusesBorr3_lop", data.get("Borr3_Bonus"), filledRows, "Borr3_Bonus");
			verifyValueText("commissionsBorr3_lop", data.get("Borr3_Commission"), filledRows, "Borr3_Commission");
			verifyValueText("dividendsBorr3_lop", data.get("Borr3_Dividends_Interest"), filledRows,
					"Borr3_Dividends_Interest");
			verifyValueText("netRentalIncomeBorr3_lop", data.get("Borr3_Net_Rental_Income"), filledRows,
					"Borr3_Net_Rental_Income");
			verifyValueText("totalOtherIncomeBorr3_lop", data.get("Borr3_Total_Other_Income"), filledRows,
					"Borr3_Total_Other_Income");
			verifyValueText("totalIncomeBorr3_lop", data.get("Borr3_Total_Income"), filledRows, "Borr3_Total_Income");
		} else {

			putNA(filledRows, "Borr3_Base_Employment");
			putNA(filledRows, "Borr3_Overtime");
			putNA(filledRows, "Borr3_Bonus");
			putNA(filledRows, "Borr3_Commission");
			putNA(filledRows, "Borr3_Dividends_Interest");
			putNA(filledRows, "Borr3_Net_Rental_Income");
			putNA(filledRows, "Borr3_Total_Other_Income");
			putNA(filledRows, "Borr3_Total_Income");

		}
		if (borr4Avail) {
			verifyValueText("baseEmploymentIncomeBorr4_lop", data.get("Borr4_Base_Employment"), filledRows,
					"Borr4_Base_Employment");
			verifyValueText("overtimePayBorr4_lop", data.get("Borr4_Overtime"), filledRows, "Borr4_Overtime");
			verifyValueText("bonusesBorr4_lop", data.get("Borr4_Bonus"), filledRows, "Borr4_Bonus");
			verifyValueText("commissionsBorr4_lop", data.get("Borr4_Commission"), filledRows, "Borr4_Commission");
			verifyValueText("dividendsBorr4_lop", data.get("Borr4_Dividends_Interest"), filledRows,
					"Borr4_Dividends_Interest");
			verifyValueText("netRentalIncomeBorr4_lop", data.get("Borr4_Net_Rental_Income"), filledRows,
					"Borr4_Net_Rental_Income");
			verifyValueText("totalOtherIncomeBorr4_lop", data.get("Borr4_Total_Other_Income"), filledRows,
					"Borr4_Total_Other_Income");
			verifyValueText("totalIncomeBorr4_lop", data.get("Borr4_Total_Income"), filledRows, "Borr4_Total_Income");

		} else {

			putNA(filledRows, "Borr4_Base_Employment");
			putNA(filledRows, "Borr4_Overtime");
			putNA(filledRows, "Borr4_Bonus");
			putNA(filledRows, "Borr4_Commission");
			putNA(filledRows, "Borr4_Dividends_Interest");
			putNA(filledRows, "Borr4_Net_Rental_Income");
			putNA(filledRows, "Borr4_Total_Other_Income");
			putNA(filledRows, "Borr4_Total_Income");
		}

		// 1003 property
		click("property_lop_xpath");
		Thread.sleep(7500);
		wait("Property_Zip_lop_name", 15);

		verifyValueText("Property_City_lop", data.get("Property_City"), filledRows, "Property_City");
		verifyValueText("Property_State_lop", data.get("Property_State"), filledRows, "Property_State");
		verifyValueText("Property_Address_lop", data.get("Property_Address"), filledRows, "Property_Address");
		verifyValueText("Property_Zip_lop", data.get("Property_Zip"), filledRows, "Property_Zip");
		verifyValueText("Occupancy_lop", data.get("Occupancy"), filledRows, "Occupancy");
		verifyValueText("Property_Type_lop", data.get("Property_Type"), filledRows, "Property_Type");
		verifyValueText("Project_Type_lop", data.get("Project_Type"), filledRows, "Project_Type");
		verifyValueText("No_Units_lop", data.get("No_Units"), filledRows, "No_Units");

		// 1003 consent
		click("consent_lop_xpath");
		// TODO:
		wait("Status_130_Prospect_xpath", 15);
		verifyText("Status_130_Prospect", data.get("Status_130_Prospect"), filledRows, "Status_130_Prospect");
		verifyText("Borr1_pullCreditConsentInd", data.get("Borr1_pullCreditConsentInd"), filledRows,
				"Borr1_pullCreditConsentInd");
		verifyText("Borr1_pullCreditConsentDateTime", data.get("Borr1_pullCreditConsentDateTime"), filledRows,
				"Borr1_pullCreditConsentDateTime");
		verifyText("Borr1_PullCreditConsentMethodType", data.get("Borr1_PullCreditConsentMethodType"), filledRows,
				"Borr1_PullCreditConsentMethodType");

		if (borr2Avail) {

			verifyText("Borr2_pullCreditConsentInd", data.get("Borr2_pullCreditConsentInd"), filledRows,
					"Borr2_pullCreditConsentInd");
			verifyText("Borr2_pullCreditConsentDateTime", data.get("Borr2_pullCreditConsentDateTime"), filledRows,
					"Borr2_pullCreditConsentDateTime");
			verifyText("Borr2_PullCreditConsentMethodType", data.get("Borr2_PullCreditConsentMethodType"), filledRows,
					"Borr2_PullCreditConsentMethodType");

		} else {
			putNA(filledRows, "Borr2_pullCreditConsentInd");
			putNA(filledRows, "Borr2_pullCreditConsentDateTime");
			putNA(filledRows, "Borr2_PullCreditConsentMethodType");
		}
		if (borr3Avail) {

			verifyText("Borr3_pullCreditConsentInd", data.get("Borr3_pullCreditConsentInd"), filledRows,
					"Borr3_pullCreditConsentInd");
			verifyText("Borr3_pullCreditConsentDateTime", data.get("Borr3_pullCreditConsentDateTime"), filledRows,
					"Borr3_pullCreditConsentDateTime");
			verifyText("Borr3_PullCreditConsentMethodType", data.get("Borr3_PullCreditConsentMethodType"), filledRows,
					"Borr3_PullCreditConsentMethodType");

		} else {
			putNA(filledRows, "Borr3_pullCreditConsentInd");
			putNA(filledRows, "Borr3_pullCreditConsentDateTime");
			putNA(filledRows, "Borr3_PullCreditConsentMethodType");
		}
		if (borr4Avail) {

			verifyText("Borr4_pullCreditConsentInd", data.get("Borr4_pullCreditConsentInd"), filledRows,
					"Borr4_pullCreditConsentInd");
			verifyText("Borr4_pullCreditConsentDateTime", data.get("Borr4_pullCreditConsentDateTime"), filledRows,
					"Borr4_pullCreditConsentDateTime");
			verifyText("Borr4_PullCreditConsentMethodType", data.get("Borr4_PullCreditConsentMethodType"), filledRows,
					"Borr4_PullCreditConsentMethodType");

		} else {
			putNA(filledRows, "Borr4_pullCreditConsentInd");
			putNA(filledRows, "Borr4_pullCreditConsentDateTime");
			putNA(filledRows, "Borr4_PullCreditConsentMethodType");
		}

		// 1003 credit

		click("creditPage_lop_xpath");
		// TODO: Borr1_CreditReportDocID
		try {
			wait("creditAgency_lop_xpath", 15);
		} catch (org.openqa.selenium.NoSuchElementException e) {
			e.printStackTrace();
		}
		verifyText("creditAgency_lop", data.get("Borr1_CreditAgency"), filledRows, "Borr1_CreditAgency");

		verifyText("creditReferenceNum_lop", data.get("Borr1_CreditReferenceNumber"), filledRows,
				"Borr1_CreditReferenceNumber ");

		// 1003 demographic

		click("demographicInfo_lop_xpath");
		wait("borr1Demo_lop_id", 15);
		click("borr1Demo_lop_id");
		Thread.sleep(500);
		xlsResults.setCellData("Results", "Expected " + "Borr1_Gender", filledRows, data.get("Borr1_Gender"));

		if ((getElement("genderMaleBorr_xpath").isEnabled() && data.get("Borr1_Gender").equals("M"))) {
			xlsResults.setCellData("Results", "Actual " + "Borr1_Gender", filledRows, "Male");
			xlsResults.setCellData("Results", "Borr1_Gender" + " Result", filledRows, "Pass");

		} else if (getElement("genderFemaleBorr_xpath").isEnabled() && data.get("Borr1_Gender").equals("F")) {
			xlsResults.setCellData("Results", "Actual " + "Borr1_Gender", filledRows, "Female");
			xlsResults.setCellData("Results", "Borr1_Gender" + " Result", filledRows, "Pass");

		} else if (getElement("genderNeigtherBorr_xpath").isEnabled() && data.get("Borr1_Gender").equals("")) {
			xlsResults.setCellData("Results", "Actual " + "Borr1_Gender", filledRows, "x");
			xlsResults.setCellData("Results", "Borr1_Gender" + " Result", filledRows, "Pass");

		} else {
			xlsResults.setCellData("Results", "EmployeeLoan" + " Result", filledRows, "Fail");
		}

		if (borr2Avail) {
			click("borr2Demo_lop_id");
			Thread.sleep(500);
			xlsResults.setCellData("Results", "Expected " + "Borr2_Gender", filledRows, data.get("Borr2_Gender"));
			if ((getElement("genderMaleBorr_xpath").isEnabled() && data.get("Borr2_Gender").equals("M"))) {
				xlsResults.setCellData("Results", "Actual " + "Borr2_Gender", filledRows, "Male");
				xlsResults.setCellData("Results", "Borr2_Gender" + " Result", filledRows, "Pass");

			} else if (getElement("genderFemaleBorr_xpath").isEnabled() && data.get("Borr2_Gender").equals("F")) {
				xlsResults.setCellData("Results", "Actual " + "Borr2_Gender", filledRows, "Female");
				xlsResults.setCellData("Results", "Borr2_Gender" + " Result", filledRows, "Pass");

			} else if (getElement("genderNeigtherBorr_xpath").isEnabled() && data.get("Borr2_Gender").equals("")) {
				xlsResults.setCellData("Results", "Actual " + "Borr2_Gender", filledRows, "x");
				xlsResults.setCellData("Results", "Borr2_Gender" + " Result", filledRows, "Pass");

			} else {
				xlsResults.setCellData("Results", "Borr2_Gender" + " Result", filledRows, "Fail");
			}
		} else {
			putNA(filledRows, "Borr2_Gender");
		}
		if (borr3Avail) {
			click("borr3Demo_lop_id");
			Thread.sleep(500);
			xlsResults.setCellData("Results", "Expected " + "Borr3_Gender", filledRows, data.get("Borr3_Gender"));
			if ((getElement("genderMaleBorr_xpath").isEnabled() && data.get("Borr3_Gender").equals("M"))) {
				xlsResults.setCellData("Results", "Actual " + "Borr3_Gender", filledRows, "Male");
				xlsResults.setCellData("Results", "Borr3_Gender" + " Result", filledRows, "Pass");

			} else if (getElement("genderFemaleBorr_xpath").isEnabled() && data.get("Borr3_Gender").equals("F")) {
				xlsResults.setCellData("Results", "Actual " + "Borr3_Gender", filledRows, "Female");
				xlsResults.setCellData("Results", "Borr3_Gender" + " Result", filledRows, "Pass");

			} else if (getElement("genderNeigtherBorr_xpath").isEnabled() && data.get("Borr3_Gender").equals("")) {
				xlsResults.setCellData("Results", "Actual " + "Borr3_Gender", filledRows, "x");
				xlsResults.setCellData("Results", "Borr3_Gender" + " Result", filledRows, "Pass");

			} else {
				xlsResults.setCellData("Results", "Borr3_Gender" + " Result", filledRows, "Fail");
			}
		}
		if (borr4Avail) {
			click("borr4Demo_lop_id");
			Thread.sleep(500);
			xlsResults.setCellData("Results", "Expected " + "Borr4_Gender", filledRows, data.get("Borr4_Gender"));
			if ((getElement("genderMaleBorr_xpath").isEnabled() && data.get("Borr4_Gender").equals("M"))) {
				xlsResults.setCellData("Results", "Actual " + "Borr4_Gender", filledRows, "Male");
				xlsResults.setCellData("Results", "Borr4_Gender" + " Result", filledRows, "Pass");

			} else if (getElement("genderFemaleBorr_xpath").isEnabled() && data.get("Borr4_Gender").equals("F")) {
				xlsResults.setCellData("Results", "Actual " + "Borr4_Gender", filledRows, "Female");
				xlsResults.setCellData("Results", "Borr4_Gender" + " Result", filledRows, "Pass");

			} else if (getElement("genderNeigtherBorr_xpath").isEnabled() && data.get("Borr4_Gender").equals("")) {
				xlsResults.setCellData("Results", "Actual " + "Borr4_Gender", filledRows, "x");
				xlsResults.setCellData("Results", "Borr4_Gender" + " Result", filledRows, "Pass");

			} else {
				xlsResults.setCellData("Results", "Borr4_Gender" + " Result", filledRows, "Fail");
			}
		}
		// 1003 pricing
		click("pricing_lop_xpath");
		Thread.sleep(3000);
		// wait("applicationDateTime_lop_name", 10);
		verifyValueText("applicationDateTime_lop", data.get("Appl_Date"), filledRows, "Appl_Date");

	}

	// Reading the data from input file
	@DataProvider()
	public Object[][] getData() {
		super.init();
		xls = new Xls_Reader(System.getProperty("user.dir") + prop.getProperty("lop_xlspath"));
		return DataUtil.getTestData(xls, testName);
	}

	// creating column in excel result

	private void initExcelResultCol(String keys) {
		String sheetName = "Results";// name of sheet

		int i = DataUtil.getFilledCol(xlsResults, sheetName);
		xlsResults.setCellData(sheetName, i, 1, "Actual " + keys);
		i++;
		xlsResults.setCellData(sheetName, i, 1, "Expected " + keys);
		i++;
		xlsResults.setCellData(sheetName, i, 1, keys + " Result");
	}

	private void initReports2() {

		if (xlsResults != null) {
			return;
		}
		initXlsResult(testName);
		Object[] keys = { "Appl_Date", "Borr1_Applicant_Type", "Borr1_SSN", "Borr1_DOB", "Borr1_Email_Address",
				"Borr1_Base_Employment", "Borr1_Overtime", "Borr1_Bonus", "Borr1_Commission",
				"Borr1_Dividends_Interest", "Borr1_Net_Rental_Income", "Borr1_Total_Other_Income", "Borr1_Total_Income",
				"Borr1_pullCreditConsentInd", "Borr1_pullCreditConsentDateTime", "Borr1_PullCreditConsentMethodType",
				"Borr1_CreditReferenceNumber", "Borr1_CreditAgency", "EmployeeLoan", "Borr1_Gender", "Borr1_Home_Phone",
				"Borr1_Cell_Phone", "Borr1_Work_Phone", "Borr1_EmployerName", "Borr1_EmployerAddressStreet",
				"Borr1_EmployerAddressCity", "Borr1_EmployerAddressState", "Borr1_EmployerAddressZip",
				"Borr1_JobPosition", "Borr1_SelfEmployed", "Borr1_CreditReportDocID", "Borr1_PresentAddressStreet",
				"Borr1_PresentAddressCity", "Borr1_PresentAddressState", "Borr1_PresentAddressZip",
				"Borr1_MailingAddressStreet", "Borr2_Applicant_Type", "Borr2_SSN", "Borr2_DOB", "Borr2_Email_Address",
				"Borr2_Base_Employment", "Borr2_Overtime", "Borr2_Bonus", "Borr2_Commission",
				"Borr2_Dividends_Interest", "Borr2_Net_Rental_Income", "Borr2_Total_Other_Income", "Borr2_Total_Income",
				"Borr2_pullCreditConsentInd", "Borr2_pullCreditConsentDateTime", "Borr2_PullCreditConsentMethodType",
				"Borr2_Gender", "Borr2_Home_Phone", "Borr2_Cell_Phone", "Borr2_Work_Phone", "Borr2_EmployerName",
				"Borr2_EmployerAddressStreet", "Borr2_EmployerAddressCity", "Borr2_EmployerAddressState",
				"Borr2_EmployerAddressZip", "Borr2_JobPosition", "Borr2_SelfEmployed", "Borr2_CreditReportDocID",
				"Borr2_PresentAddressStreet", "Borr2_PresentAddressCity", "Borr2_PresentAddressState",
				"Borr2_PresentAddressZip", "Borr2_MailingAddressStreet", "Borr3_Applicant_Type", "Borr3_SSN",
				"Borr3_DOB", "Borr3_Email_Address", "Borr3_Base_Employment", "Borr3_Overtime", "Borr3_Bonus",
				"Borr3_Commission", "Borr3_Dividends_Interest", "Borr3_Net_Rental_Income", "Borr3_Total_Other_Income",
				"Borr3_Total_Income", "Borr3_pullCreditConsentInd", "Borr3_pullCreditConsentDateTime",
				"Borr3_PullCreditConsentMethodType", "Borr3_Gender", "Borr3_Home_Phone", "Borr3_Cell_Phone",
				"Borr3_Work_Phone", "Borr3_EmployerName", "Borr3_EmployerAddressStreet", "Borr3_EmployerAddressCity",
				"Borr3_EmployerAddressState", "Borr3_EmployerAddressZip", "Borr3_JobPosition", "Borr3_SelfEmployed",
				"Borr3_CreditReportDocID", "Borr3_PresentAddressStreet", "Borr3_PresentAddressCity",
				"Borr3_PresentAddressState", "Borr3_PresentAddressZip", "Borr3_MailingAddressStreet",
				"Borr4_Applicant_Type", "Borr4_SSN", "Borr4_DOB", "Borr4_Email_Address", "Borr4_Base_Employment",
				"Borr4_Overtime", "Borr4_Bonus", "Borr4_Commission", "Borr4_Dividends_Interest",
				"Borr4_Net_Rental_Income", "Borr4_Total_Other_Income", "Borr4_Total_Income",
				"Borr4_pullCreditConsentInd", "Borr4_pullCreditConsentDateTime", "Borr4_Gender", "Borr4_Home_Phone",
				"Borr4_Cell_Phone", "Borr4_Work_Phone", "Borr4_EmployerName", "Borr4_EmployerAddressStreet",
				"Borr4_EmployerAddressCity", "Borr4_EmployerAddressState", "Borr4_EmployerAddressZip",
				"Borr4_JobPosition", "Borr4_SelfEmployed", "Borr4_PresentAddressStreet", "Borr4_PresentAddressCity",
				"Borr4_PresentAddressState", "Borr4_PresentAddressZip", "Borr4_MailingAddressStreet",
				"Property_Address", "Property_Zip", "Property_City", "Property_State", "Occupancy", "Property_Type",
				"Project_Type", "No_Units", "Mortgage_Type", "Loan_Product", "Loan_Program", "Loan_Purpose",
				"Purchase_Price", "Appraised_Value", "Base_Loan_Amt", "MI_Financed", "Total_Loan_Amt", "LTV",
				"Combined_LTV", "HCLTV", "Interest_Rate", "Status_130_Prospect"

		};
		String sheetName = "Results";// name of sheet
		xlsResults.setCellData(sheetName, 0, 1, "Test Case Name");
		xlsResults.setCellData(sheetName, 1, 1, "Loan_Number");

		String[] notCol = { "Borr1_SelfEmployed", "Borr2_SelfEmployed", "Borr3_SelfEmployed", "Borr4_SelfEmployed",
				"Borr2_CreditReferenceNumber", "Borr2_CreditAgency", "Borr3_CreditReferenceNumber",
				"Borr3_CreditAgency", "Borr4_CreditAgency", "Borr2_Dividends_Interest", "Borr4_CreditReferenceNumber",
				"Other_Financing_Ind", "TestCase", "Browser", "loUserName", "loPassword", "Loan_Number", "Runmode",
				"Borr1_First_Name", "Borr1_Middle_Name", "Borr1_Last_Name", "Borr2_First_Name", "Borr2_Middle_Name",
				"Borr2_Last_Name", "Borr3_First_Name", "Borr3_Middle_Name", "Borr3_Last_Name", "Borr4_First_Name",
				"Borr4_Middle_Name", "Borr4_Last_Name", "TransactionId", "BS_InputPayLoad_RowID", "TransactionId",
				"BS_Loan_Status", "HB_Status_Msg", "BS_Status_Msg", "Lender_ID", "Channel_ID", "Profile_ID",
				"ENC_Loan_GUID", "Linked_Enc_Loan_GUID", "Est_Closing_Date", "Campaign_ID",
				"Invite_Borrowers_To_Portal_Ind", "CreditScoreUsedForDecision", "No_Borrowers", "Borr1_BorrowerID",
				"Borr1_Suffix", "Borr1_Vesting_Type", "Borr1_Borr2_Rel", "Borr1_CreditReportDocID",
				"Borr1_ExperianScore", "Borr1_TransunionScore", "Borr1_EquifaxScore", "Borr2_BorrowerID",
				"Borr2_Suffix", "Borr2_Vesting_Type", "Borr2_Borr3_Rel", "Borr2_CreditReportDocID",
				"Borr2_ExperianScore", "Borr2_TransunionScore", "Borr2_EquifaxScore", "Borr3_BorrowerID",
				"Borr3_Suffix", "Borr3_Vesting_Type", "Borr3_Borr4_Rel", "Borr3_CreditReportDocID",
				"Borr3_ExperianScore", "Borr3_TransunionScore", "Borr3_EquifaxScore", "Borr4_BorrowerID",
				"Borr4_Suffix", "Borr4_Email_Address", "Borr4_Vesting_Type", "Borr4_CreditReportDocID",
				"Borr4_ExperianScore", "Borr4_TransunionScore", "Borr4_EquifaxScore", "NBO1_BorrowerID",
				"NBO1_Email_Address", "NBO1_First_Name", "NBO1_Middle_Name", "NBO1_Last_Name", "NBO1_Suffix",
				"NBO1_Vesting_Type", "NBO1_DOB", "NBO2_BorrowerID", "NBO2_Email_Address", "NBO2_First_Name",
				"NBO2_Middle_Name", "NBO2_Last_Name", "NBO2_Suffix", "NBO2_Vesting_Type", "NBO2_DOB", "Frontend_DTI",
				"Backend_DTI", "Property_County", "Property_County_FIPS_Code", "Lien_Type", "Pricing_Tier",
				"Amortization_Type", "Optimal_Blue_Product_ID", "Encompass_Loan_Program_Name", "Refi_purpose",
				"Refi_Type", "Loan_Term", "Doc_Type", "InvestorID", "Qualifying_Rate", "Arm_Margin", "Arm_Plan",
				"Arm_Index", "Waive_Escrow_Ind", "Credit_Score", "MI_Coverage_Pct", "Concurrent_Liens", "PMI_Plan",
				"PMI_Company", "PMI_Single_Prem_Pct", "PMI_Renewal_Rate_1", "PMI_Renewal_Rate_2",
				"PMI_Renewal_1_Start_Mos", "PMI_Renewal_1_End_Mos", "PMI_Renewal_2_Start_Mos", "PMI_Renewal_2_End_Mos",
				"Upfront_Premium_Amt", "Amount_Financed", "MI_Monthly_Pmt", "VA_Military_Type", "VA_Entitlement_Ind",
				"VA_Funding_Fee_Exempt_Ind", "VA_Funding_Fee_Pct", "VA_Funding_Fee_Due_Upfront",
				"MIP_Upfront_Premium_Pct", "MIP_Renewal_Rate_1", "Mip_Renewal_Term_Mos", "USDA_Upfront_Guarantee_Pct",
				"USDA_Annual_Guarantee_Pct", "Approval_Expires_Date", "Credit_Expires_Date", "Income_Expires_Date",
				"Asset_Expires_Date", "Appraisal_Expires_Date", "Rate_Lock_Expires_Date", "Submit_To_UW_Date",
				"Approval_Date", "Lock_Days", "Manual_UW_Ind", "AUS_Type", "Status_210_App_in_Process",
				"Status_310_in_Setup", "Status_410_In_Processing", "Status_510_UW", "Status_610_Approved",
				"Status_710_Clear_to_Close", "Status_810_Ready_for_Docs", "Status_820_Docs_Out", "Status_905_Docs_Back",
				"Status_910_Funding_in_Process", "Status_990_Funded", "Status_1110_Docs_Back", "Status_1210_Shipped",
				"Status_1290_Delivery_Completed", "Status_1390_Withdrawn", "Status_1490_Cancelled",
				"Status_1590_Denied", "Loan_Officer_UserID", "LOA_UserID", "Loan_Processor_UserID",
				"Underwriter_UserID", "CEMA_Ind", "Closing_Date", "Interest_Credit", "Vesting", "Settlement_Agent_Name",
				"Attorney_Name", "Settlement_Agent_Contact_Name", "Settlement_Agent_Address_1",
				"Settlement_Agent_Address_2", "Settlement_Agent_Zip", "Settlement_Agent_City", "Settlement_Agent_State",
				"Settlement_Agent_Phone", "Settlement_Agent_Fax", "Settlement_Agent_Email", "Borr1_MailingAddressCity",
				"Borr1_MailingAddressState", "Borr1_MailingAddressZip"

		};

		initExcelResultCol("Borr1_First_Last_Name");
		initExcelResultCol("Borr2_First_Last_Name");
		initExcelResultCol("Borr3_First_Last_Name");
		initExcelResultCol("Borr4_First_Last_Name");

		for (int j = 0; j < keys.length; j++) {
			if (Arrays.stream(notCol).anyMatch(keys[j].toString()::equals)) {
				continue;
			}
			initExcelResultCol(keys[j].toString());
		}
	}

	@Override
	@BeforeTest
	public void initReports() {
		initReports2();
		initCodes();
	}

	@Override
	@AfterMethod
	public void quit(ITestResult result) {
		if (result.getStatus() == ITestResult.FAILURE) {
			// Checking for the test if the test is failed /pass/skipped etc..
			// for reporting.
			reportFailure(result.getThrowable());
		} else if (result.getStatus() == ITestResult.SKIP) {
			test.log(LogStatus.SKIP, "Test skipped " + result.getThrowable());
		} else {
			test.log(LogStatus.PASS, "Test passed");

		}
		if (rep != null) {
			rep.endTest(test);
			rep.flush();
		}
		// quit

		if (driver != null) {
			driver.quit();
		}

	}
}
