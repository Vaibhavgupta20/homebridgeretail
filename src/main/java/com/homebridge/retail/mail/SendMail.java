package com.homebridge.retail.mail;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.*;

import com.homebridge.retail.util.Constants;

import java.io.File;
import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.util.*;

public class SendMail

{
	public static void main(String[] args) throws Exception

	{

		// report folder - extent reports
		// date
		String reportFolder = Constants.REPORT_PATH;

		String emaillist = "jfarrier@homebridge.com,sanjay.verma@homebridge.com";
		SendMail.sendMail("no-reply@homebridge.com", "qwertyHe", "email.homebridge.com", "", emaillist,
				"Automation Loan and Lock Exceptions", "This is BlueSage WholeSale Portal UAT Build",
				"C:\\BlueSagePortalCombinedLoan_and_LockExceptions\\report\\report.zip");

	}

	public static boolean sendMail(final String userName, final String passWord, String host, String port,
			String recipients, String subject, String text, String filename) {

		Properties props = new Properties();
		// props.put("mail.smtp.auth", "true");
		// props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", host);
		// props.put("mail.smtp.port", port);

		// Get the Session object.
		Session session = Session.getInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(userName, passWord);
			}
		});

		try {
			// Create a default MimeMessage object.
			Message message = new MimeMessage(session);

			// Set From: header field of the header.
			message.setFrom(new InternetAddress(userName));

			String[] recipientList = recipients.split(",");

			InternetAddress[] recipientAddress = new InternetAddress[recipientList.length];
			int counter = 0;
			for (String recipient : recipientList) {
				recipientAddress[counter] = new InternetAddress(recipient.trim());
				counter++;
			}
			message.setRecipients(Message.RecipientType.TO, recipientAddress);

			// Set To: header field of the header.
//			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to[0]));

			// Set Subject: header field
			message.setSubject(subject);

			// Create the message part
			BodyPart messageBodyPart = new MimeBodyPart();

			// Now set the actual message
			messageBodyPart.setText(text);

			// Create a multipar message
			Multipart multipart = new MimeMultipart();

			// Set text message part
			multipart.addBodyPart(messageBodyPart);

			// Part two is attachment
			messageBodyPart = new MimeBodyPart();

			DataSource source = new FileDataSource(filename);
			messageBodyPart.setDataHandler(new DataHandler(source));
			messageBodyPart.setFileName(filename);
			multipart.addBodyPart(messageBodyPart);

			// Send the complete message parts
			message.setContent(multipart);

			// Send message
			Transport.send(message);

			System.out.println("Sent message successfully....");

		} catch (MessagingException e) {
			e.printStackTrace();
			return false;

		}
		return true;
	}

}