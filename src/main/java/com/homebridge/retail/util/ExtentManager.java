package com.homebridge.retail.util;

//http://relevantcodes.com/Tools/ExtentReports2/javadoc/index.html?com/relevantcodes/extentreports/ExtentReports.html

import java.io.File;
import java.util.Date;

import com.homebridge.retail.base.BaseTest;
import com.relevantcodes.extentreports.DisplayOrder;
import com.relevantcodes.extentreports.ExtentReports;

public class ExtentManager {
	private static ExtentReports extent;

	
	
	public static ExtentReports getInstance() {
		if (extent == null) {
			extent = new ExtentReports( BaseTest.REPORT_PATH+ "/ExtentReport.html", true,
					DisplayOrder.NEWEST_FIRST);

			extent.loadConfig(new File(System.getProperty("user.dir") + "//ReportsConfig.xml"));
			// optional
			extent.addSystemInfo("Selenium Version", "3.07.0").addSystemInfo("Environment", "QA");
		}
		return extent;
	}
}
