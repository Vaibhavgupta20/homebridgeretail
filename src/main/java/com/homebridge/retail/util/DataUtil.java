package com.homebridge.retail.util;

import java.util.Hashtable;

import org.testng.Assert;

public class DataUtil {

	public static Object[][] getTestData(Xls_Reader xls, String testCaseName) {
		String sheetName = "Data";
		// reads data for only testCaseName

		int testStartRowNum = 1;
		while (!xls.getCellData(sheetName, 0, testStartRowNum).equals(testCaseName)) {
			testStartRowNum++;
		}
		System.out.println("Test starts from row - " + testStartRowNum);
		int colStartRowNum = testStartRowNum + 1;
		int dataStartRowNum = testStartRowNum + 2;

		// calculate rows of data
		int rows = 0;
		while (!xls.getCellData(sheetName, 0, dataStartRowNum + rows).equals("")) {
			rows++;
		}
		System.out.println("Total rows are  - " + rows);

		// calculate total cols
		int cols = 0;
		while (!xls.getCellData(sheetName, cols, colStartRowNum).equals("")) {
			cols++;
		}
		System.out.println("Total cols are  - " + cols);
		Object[][] data = new Object[rows][1];
		// read the data
		int dataRow = 0;
		Hashtable<String, String> table = null;
		for (int rNum = dataStartRowNum; rNum < dataStartRowNum + rows; rNum++) {
			table = new Hashtable<String, String>();
			for (int cNum = 0; cNum < cols; cNum++) {
				String key = xls.getCellData(sheetName, cNum, colStartRowNum).trim();
				String value = xls.getCellData(sheetName, cNum, rNum).trim();
				table.put(key, value);
				// 0,0 0,1 0,2
				// 1,0 1,1
			}
			data[dataRow][0] = table;
			dataRow++;
		}
		return data;
	}

	/**
	 * this function sets the data at the specific coloumnName you provide and
	 * rows based on uniqueColName and UniqueData
	 * 
	 * @param xls
	 * @param testCaseName
	 * @param uniqueCol
	 *            uniqueColName
	 * @param uniqueData
	 *            UniqueData
	 * @param testColdata
	 *            coloumnName
	 * @param testdata
	 *            data you want to input
	 * @return
	 */
	public static boolean setTestData(Xls_Reader xls, String testCaseName, String uniqueCol, String uniqueData,
			String testColdata, String testdata) {
		String sheetName = "Data";
		// reads data for only testCaseName

		int testStartRowNum = 1;
		while (!xls.getCellData(sheetName, 0, testStartRowNum).equals(testCaseName)) {
			testStartRowNum++;
		}
		System.out.println("Test starts from row - " + testStartRowNum);
		int colStartRowNum = testStartRowNum + 1;
		int dataStartRowNum = testStartRowNum + 2;

		// calculate rows of data
		int totalRows = 0;
		while (!xls.getCellData(sheetName, 0, dataStartRowNum + totalRows).equals("")) {
			totalRows++;
		}
		System.out.println("Total rows are  - " + totalRows);

		// calculate test cols
		int testcols = 0;
		while (!xls.getCellData(sheetName, testcols, colStartRowNum).equals(testColdata)) {
			testcols++;
		}
		System.out.println("test cols are  - " + testcols);

		// calculate idetificationCol
		int identificationcols = 0;
		while (!xls.getCellData(sheetName, identificationcols, colStartRowNum).equals(uniqueCol)) {
			identificationcols++;
		}
		System.out.println("identification cols are  - " + identificationcols);

		for (int i = dataStartRowNum; i < totalRows + testStartRowNum + dataStartRowNum; i++) {
			if (xls.getCellData("Data", identificationcols, i).equals(uniqueData)) {
				xls.setCellData("Data", testcols, i, testdata);
				break;
			}
			Assert.fail();
		}

		return true;
	}

	public static boolean isRunnable(String testName, Xls_Reader xls) {
		String sheet = "TestCases";
		int rows = xls.getRowCount(sheet);
		for (int r = 2; r <= rows; r++) {
			String tName = xls.getCellData(sheet, "TCID", r);
			if (tName.equals(testName)) {
				String runmode = xls.getCellData(sheet, "Runmode", r);
				if (runmode.equals("Y"))
					return true;
				else
					return false;

			}
		}
		return false;
	}

	public static boolean isSkip(Xls_Reader xls, String testName) {
		int rows = xls.getRowCount(Constants.TESTCASES_SHEET);

		for (int rNum = 2; rNum <= rows; rNum++) {
			String tcid = xls.getCellData(Constants.TESTCASES_SHEET, Constants.TCID_COL, rNum);
			if (tcid.equals(testName)) {
				String runmode = xls.getCellData(Constants.TESTCASES_SHEET, Constants.RUNMODE_COL, rNum);
				if (runmode.equals("Y"))
					return false;
				else
					return true;
			}
		}

		return true;// default

	}

	public static int getFilledRows(Xls_Reader xls, String sheetName) {

		int rows = 1;
		while (!xls.getCellData(sheetName, 0, rows).equals("")) {
			rows++;
		}
		return rows;
	}

	public static int getFilledCol(Xls_Reader xls, String sheetName) {
		int cols = 0;
		while (!xls.getCellData(sheetName, cols, 1).equals("")) {
			cols++;
		}
		return cols;
	}
}
