package com.homebridge.retail.util;

import java.util.Date;

public class Constants {


	public static final String TCID_COL = "TCID";
	public static final String KEYWORD_COL = "Keyword";
	public static final String OBJECT_COL = "Object";
	public static final String DATA_COL = "Data";
	public static final String TESTCASES_SHEET = "TestCases";
	public static final String RUNMODE_COL = "Runmode";
	static Date d = new Date();
	public static final String DATE_PATH = d.toString().replace(":", "_").replace(" ", "_");
	public static final String REPORT_PATH = "C:\\BlueSagereports\\" + DATE_PATH;
	// public static final String REPORT_XLSX_PATH = REPORT_PATH + ".xlsx";
	public static final String SCREENSHOT_PATH = REPORT_PATH + "\\screenshots\\";
	public static final String ZIP_PATH = "C:\\BlueSagereports\\report.zip";
	public static final String PASS = "PASS";
	public static final String FAIL = "FAIL";
	
		
}
